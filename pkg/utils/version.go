package utils

import (
	"fmt"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
)

const (
	versionMajor = "0.2"
	versionMinor = "6"
)

// Function used to display answer to the help command (or flag)
func DisplayVersion() {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug("Display the version message")
	versionMessage := `sxcollector v` + versionMajor + "." + versionMinor
	fmt.Println(versionMessage)
}
