package utils

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	sxKCli "gitlab.com/startx1/k8s/go-libs/pkg/k8sclient"
	sxKCfg "gitlab.com/startx1/k8s/go-libs/pkg/kubeconfig"
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	sxCollector "gitlab.com/startx1/k8s/sxcollector/pkg/collector"
	sxPlugins "gitlab.com/startx1/k8s/sxcollector/pkg/plugin"
	sxPluginCore "gitlab.com/startx1/k8s/sxcollector/pkg/plugincore"
)

const (
	// GroupNameSXCollector is the group name use in this package
	GroupNameSXCollector                      = "utils"
	dbgSXQAPPrepare                           = "Prepare the sxCollectorArgParser"
	dbgSXQAPPrepareAction                     = "Found the action %s"
	dbgSXQAPPrepareNoAction                   = "No action found"
	dbgSXQAPPrepareSubAction                  = "Found the sub-action %s"
	dbgSXQAPPrepareNoSubAction                = "No sub-action found"
	dbgSXQAPPrepareHasNS                      = "sxCollectorArgParser has namespace  : %t (%s)"
	dbgSXQAPPrepareNeedK8sClient              = "action %s need k8sclient : %t"
	dbgSXQAPPrepareNoKubeconfigFile           = "Please provide a kubeconfig file for argument --kubeconfig"
	dbgSXCLHelp                               = "Display help message"
	dbgSXQAPStart                             = "Start display the sxCollectorArgParser"
	errSXQAPPrepareCheck                      = "could not check arguments because : %v"
	dbgSXQAPPrepareActionFoundPlugin          = "Prepare : Plugin %s found"
	dbgSXQAPExec                              = "Exec the sxCollectorArgParser"
	errExec                                   = "error executing the plugin : %v"
	dbgSXQAPExecPluginAction                  = "Executing the %s plugin Exec action"
	dbgSXQAPExecPluginNotLoaded               = "The plugin %s is not loaded"
	dbgGetNameBySubcommandNotFound            = "could not find plugin for subcommand %s"
	ddSeqStart                                = "Executing %s subcommand"
	dbgSXQAPCheckHasCSVSep                    = "Found argument --output-csv-separator set to %s"
	dbgSXQAPCheckHasNoHeaders                 = "Found argument --output-noheaders set to true"
	dbgSXQAPCheckHasNoUnits                   = "Found argument --output-nounits set to true"
	dbgSXQAPCheckHasFormat                    = "Found argument --output-format set to %s"
	dbgSXQAPCheckHasFile                      = "Found argument --output-file set to %s"
	dbgSXQAPCheckHasFrom                      = "Found argument --from set to %s"
	dbgSXQAPCheckHasTo                        = "Found argument --to set to %s"
	dbgSXQAPCheckHasDuration                  = "Found argument --duration set to %s"
	dbgSXQAPCheckHasInsecure                  = "Found argument --insecure set to true"
	dbgSXQAPCheckHasPrecision                 = "Found argument --precision set to %s"
	dbgSXQAPCheckHasSmtpInsecure              = "Found argument --smtp-insecure set to true"
	dbgSXQAPCheckHasMailTo                    = "Found argument --mail-to set to %s"
	dbgSXQAPCheckHasMailFrom                  = "Found argument --mail-from set to %s"
	dbgSXQAPCheckHasMailSubject               = "Found argument --mail-subject set to %s"
	dbgSXQAPCheckHasSmtpHost                  = "Found argument --smtp-host set to %s"
	dbgSXQAPCheckHasSmtpPort                  = "Found argument --smtp-port set to %s"
	dbgSXQAPCheckHasSmtpUsername              = "Found argument --smtp-username set to %s"
	dbgSXQAPCheckHasSmtpPassword              = "Found argument --smtp-password set to %s"
	dbgSXQAPCheckHasSmtpHostEnv               = "Define argument --smtp-host with env SMTP_HOST set to %s"
	dbgSXQAPCheckHasSmtpPortEnv               = "Define argument --smtp-port with env SMTP_PORT set to %s"
	dbgSXQAPCheckHasSmtpUsernameEnv           = "Define argument --smtp-username with env SMTP_USERNAME set to %s"
	dbgSXQAPCheckHasSmtpPasswordEnv           = "Define argument --smtp-password with env SMTP_PASSWORD set to %s"
	wrnCheckConvertPrecision                  = "could not convert %s because %v. Omit precision and use default one"
	errSXQAPCheckHasFormat                    = "format %s is not valid and must be either csv, json or tab"
	dbgSXQAPCheckHasToEnv                     = "Define argument --to with env SXCOLLECTOR_TO set to %s"
	dbgSXQAPCheckHasDurationEnv               = "Define argument --duration with env SXCOLLECTOR_DURATION set to %s"
	dbgSXQAPCheckHasPrecisionEnv              = "Define argument --precision with env SXCOLLECTOR_PRECISION set to %s"
	dbgSXQAPCheckHasInsecureEnv               = "Define argument --insecure with env SXCOLLECTOR_INSECURE set to true"
	dbgSXQAPCheckHasSmtpInsecureEnv           = "Define argument --smtp-insecure with env SMTP_INSECURE set to true"
	dbgSXQAPCheckHasNoHeadersEnv              = "Define argument --output-noheaders with env SXCOLLECTOR_NOHEADERS set to true"
	dbgSXQAPCheckHasNoUnitsEnv                = "Define argument --output-nounits with env SXCOLLECTOR_NOUNITS set to true"
	dbgSXQAPCheckHasCSVSepEnv                 = "Define argument --output-csv-separator with env SXCOLLECTOR_SEP set to %s"
	dbgSXQAPCheckHasMailToEnv                 = "Define argument --mail-to with env MAIL_TO set to %s"
	dbgSXQAPCheckHasMailFromEnv               = "Define argument --mail-from with env MAIL_FROM set to %s"
	dbgSXQAPCheckHasMailSubjectEnv            = "Define argument --mail-subject with env MAIL_SUBJECT set to %s"
	dbgSXQAPCheckHasOutputFormatEnv           = "Define argument --output-format with env SXCOLLECTOR_FORMAT set to %s"
	dbgSXQAPPrepareHasKubeconfigArg           = "Loading kubeconfig from argument --kubeconfig: %s"
	errPrepareNoKubeEnv                       = "could not load kubeconfig from environment: %v"
	errPrepareNoKubeHome                      = "could not load kubeconfig from home directory: %v"
	dbgSXQAPCheckHasConfluenceUrlEnv          = "Define argument --confluence-url with env CONFLUENCE_BASE_URL set to %s"
	dbgSXQAPCheckHasConfluenceUsernameEnv     = "Define argument --confluence-username with env CONFLUENCE_USERNAME set to %s"
	dbgSXQAPCheckHasConfluenceApiTokenEnv     = "Define argument --confluence-apitoken with env CONFLUENCE_API_TOKEN set to %s"
	dbgSXQAPCheckHasConfluenceSpaceKeyEnv     = "Define argument --confluence-spacekey with env CONFLUENCE_SPACE_KEY set to %s"
	dbgSXQAPCheckHasConfluencePageTitleEnv    = "Define argument --confluence-title with env CONFLUENCE_PAGE_TITLE set to %s"
	dbgSXQAPCheckHasConfluencePageContentEnv  = "Define argument --confluence-content with env CONFLUENCE_PAGE_HTML_CONTENT set to %s"
	dbgSXQAPCheckHasConfluencePageIDEnv       = "Define argument --confluence-pageid with env CONFLUENCE_PAGE_ID set to %s"
	dbgSXQAPCheckHasConfluencePageParentIDEnv = "Define argument --confluence-parentid with env CONFLUENCE_PARENT_ID set to %s"
	dbgSXQAPCheckHasConfluenceMode            = "Found argument --confluence-mode set to %s"
	dbgSXQAPCheckHasConfluenceUrl             = "Found argument --confluence-url set to %s"
	dbgSXQAPCheckHasConfluenceUsername        = "Found argument --confluence-username set to %s"
	dbgSXQAPCheckHasConfluenceApiToken        = "Found argument --confluence-apitoken set to %s"
	dbgSXQAPCheckHasConfluenceSpaceKey        = "Found argument --confluence-spacekey set to %s"
	dbgSXQAPCheckHasConfluencePageTitle       = "Found argument --confluence-title set to %s"
	dbgSXQAPCheckHasConfluencePageContent     = "Found argument --confluence-content set to %s"
	dbgSXQAPCheckHasConfluencePageID          = "Found argument --confluence-pageid set to %s"
	dbgSXQAPCheckHasConfluencePageParentID    = "Found argument --confluence-parentid set to %s"
	dbgSXQAPCheckHasGSheetMode                = "Found argument --gsheet-mode set to %s"
	dbgSXQAPCheckHasGSheetSpreadsheetID       = "Found argument --gsheet-spreadsheet-id set to %s"
	dbgSXQAPCheckHasGSheetSpreadsheetIDEnv    = "Define argument --gsheet-spreadsheet-id with env GSPREADSHEET_ID set to %s"
	dbgSXQAPCheckHasGSheetSpreadsheetTitle    = "Found argument --gsheet-spreadsheet-title set to %s"
	dbgSXQAPCheckHasGSheetSpreadsheetTitleEnv = "Define argument --gsheet-spreadsheet-title with env GSPREADSHEET_TITLE set to %s"
	dbgSXQAPCheckHasGSheetSheetTitle          = "Found argument --gsheet-sheet-title set to %s"
	dbgSXQAPCheckHasGSheetSheetTitleEnv       = "Define argument --gsheet-sheet-title with env GSPREADSHEET_SHEET_TITLE set to %s"
	dbgSXQAPCheckHasGDriveCredFilename        = "Found argument --gdrive-creds-filename set to %s"
	dbgSXQAPCheckHasGDriveCredFilenameEnv     = "Define argument --gdrive-creds-filename with env GDRIVE_CREDENTIAL_FILENAME set to %s"
	dbgSXQAPCheckHasGDriveCredPath            = "Found argument --gdrive-creds-path set to %s"
	dbgSXQAPCheckHasGDriveCredPathEnv         = "Define argument --gdrive-creds-path with env GDRIVE_CREDENTIAL_PATH set to %s"
	dbgSXQAPCheckHasGDriveCredDomain          = "Found argument --gdrive-creds-domain set to %s"
	dbgSXQAPCheckHasGDriveCredDomainEnv       = "Define argument --gdrive-creds-domain with env GDRIVE_CREDENTIAL_DOMAIN set to %s"
)

// ArgParser is a definition for displaying message for a command line
type sxCollectorArgParser struct {
	Args               *sxUtils.ArgParser
	HasNamespace       bool
	Namespace          string
	PluginGenericsArgs map[string]string
	PluginLoaded       bool
	Plugin             sxPluginCore.PluginInterface
}

// Initialize a ArgParser object
// ex:
//
//	display := NewArgParser("main",true)
func NewSXCollectorArgParser(sourceArgs []string) *sxCollectorArgParser {
	var args = sxUtils.NewArgParser(sourceArgs)
	sxPluginCore.InitializePlugins()
	plugin, _ := sxPluginCore.LoadPlugin(sxPlugins.DefaultPlugin)
	argParser := &sxCollectorArgParser{
		Args:               args,
		PluginGenericsArgs: map[string]string{},
		HasNamespace:       false,
		Namespace:          "",
		PluginLoaded:       false,
		Plugin:             plugin,
	}
	return argParser
}

// Display the content of the ArgParser
func (argParser *sxCollectorArgParser) Debug() *sxCollectorArgParser {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(dbgSXQAPStart)
	argParser.Args.Debug()
	display.Debug(fmt.Sprintf(dbgSXQAPPrepareHasNS, argParser.HasNamespace, argParser.Namespace))
	return argParser
}

// used to scan arguments remove the globals one and return a new arguments
// list without them
func (argParser *sxCollectorArgParser) Check() error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	// generic subcommand flags
	genericArgs := map[string]string{}
	genericArgs["output-csv-separator"] = ","
	genericArgs["insecure"] = "false"
	genericArgs["smtp-insecure"] = "false"
	genericArgs["output-noheaders"] = "false"
	genericArgs["output-nounits"] = "false"
	genericArgs["output-type"] = "stdout"
	genericArgs["output-format"] = "tab"
	genericArgs["output-file"] = "./sxcollector.csv"
	genericArgs["confluence-parentid"] = ""
	genericArgs["confluence-pageid"] = ""
	// generics options
	if argParser.Args.HasFlag("--insecure") {
		genericArgs["insecure"] = "true"
		nsPos := argParser.Args.GetFlagPos("--insecure")
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(dbgSXQAPCheckHasInsecure)
	} else if os.Getenv("SXCOLLECTOR_INSECURE") == "true" {
		genericArgs["insecure"] = "true"
		display.Debug(dbgSXQAPCheckHasInsecureEnv)
	}
	// prometheus options
	if argParser.Args.HasFlag("--from") {
		genericArgs["from"] = argParser.Args.GetFlagNextVal("--from")
		nsPos := argParser.Args.GetFlagPos("--from")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasFrom, genericArgs["from"]))
	}
	if argParser.Args.HasFlag("--to") {
		genericArgs["to"] = argParser.Args.GetFlagNextVal("--to")
		nsPos := argParser.Args.GetFlagPos("--to")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasTo, genericArgs["to"]))
	}
	if argParser.Args.HasFlag("--duration") {
		genericArgs["duration"] = argParser.Args.GetFlagNextVal("--duration")
		nsPos := argParser.Args.GetFlagPos("--duration")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasDuration, genericArgs["duration"]))
	} else if os.Getenv("SXCOLLECTOR_DURATION") != "" {
		genericArgs["duration"] = os.Getenv("SXCOLLECTOR_DURATION")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasDurationEnv, genericArgs["duration"]))
	}
	if argParser.Args.HasFlag("--precision") {
		genericArgs["precision"] = argParser.Args.GetFlagNextVal("--precision")
		nsPos := argParser.Args.GetFlagPos("--precision")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		_, errConv := strconv.Atoi(genericArgs["precision"])
		if errConv != nil {
			display.Warning(fmt.Sprintf(wrnCheckConvertPrecision, genericArgs["precision"], errConv))
		}
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasPrecision, genericArgs["precision"]))
	} else if os.Getenv("SXCOLLECTOR_PRECISION") != "" {
		genericArgs["precision"] = os.Getenv("SXCOLLECTOR_PRECISION")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasPrecisionEnv, genericArgs["precision"]))
	}
	// output options
	if argParser.Args.HasFlag("--output-noheaders") {
		genericArgs["output-noheaders"] = "true"
		nsPos := argParser.Args.GetFlagPos("--output-noheaders")
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(dbgSXQAPCheckHasNoHeaders)
	} else if os.Getenv("SXCOLLECTOR_NOHEADERS") == "true" {
		genericArgs["output-noheaders"] = "true"
		display.Debug(dbgSXQAPCheckHasNoHeadersEnv)
	}
	if argParser.Args.HasFlag("--output-nounits") {
		genericArgs["output-nounits"] = "true"
		nsPos := argParser.Args.GetFlagPos("--output-nounits")
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(dbgSXQAPCheckHasNoUnits)
	} else if os.Getenv("SXCOLLECTOR_NOUNITS") == "true" {
		genericArgs["output-nounits"] = "true"
		display.Debug(dbgSXQAPCheckHasNoUnitsEnv)
	}
	if argParser.Args.HasFlag("--output-format") {
		outputFormat := argParser.Args.GetFlagNextVal("--output-format")
		if outputFormat != "tab" && outputFormat != "json" && outputFormat != "csv" {
			return fmt.Errorf(errSXQAPCheckHasFormat, outputFormat)
		}
		genericArgs["output-format"] = outputFormat
		nsPos := argParser.Args.GetFlagPos("--output-format")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasFormat, genericArgs["output-format"]))
	} else if os.Getenv("SXCOLLECTOR_FORMAT") != "" {
		genericArgs["output-format"] = os.Getenv("SXCOLLECTOR_FORMAT")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasOutputFormatEnv, genericArgs["output-format"]))
	}
	if argParser.Args.HasFlag("--output-csv-separator") {
		genericArgs["output-csv-separator"] = argParser.Args.GetFlagNextVal("--output-csv-separator")
		nsPos := argParser.Args.GetFlagPos("--output-csv-separator")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasCSVSep, genericArgs["output-csv-separator"]))
	} else if os.Getenv("SXCOLLECTOR_SEP") != "" {
		genericArgs["output-csv-separator"] = os.Getenv("SXCOLLECTOR_SEP")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasCSVSepEnv, genericArgs["output-csv-separator"]))
	}
	if argParser.Args.HasFlag("--output-file") {
		genericArgs["output-type"] = "file"
		genericArgs["output-file"] = argParser.Args.GetFlagNextVal("--output-file")
		nsPos := argParser.Args.GetFlagPos("--output-file")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasFile, genericArgs["output-file"]))
	}
	// confluence options
	if argParser.Args.HasFlag("--confluence-mode") {
		genericArgs["output-type"] = "confluence"
		genericArgs["confluence-mode"] = argParser.Args.GetFlagNextVal("--confluence-mode")
		nsPos := argParser.Args.GetFlagPos("--confluence-mode")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasConfluenceMode, genericArgs["confluence-mode"]))
	}
	if argParser.Args.HasFlag("--confluence-url") {
		genericArgs["confluence-url"] = argParser.Args.GetFlagNextVal("--confluence-url")
		nsPos := argParser.Args.GetFlagPos("--confluence-url")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasConfluenceUrl, genericArgs["confluence-url"]))
	} else if os.Getenv("CONFLUENCE_BASE_URL") != "" {
		genericArgs["confluence-url"] = os.Getenv("CONFLUENCE_BASE_URL")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasConfluenceUrlEnv, genericArgs["confluence-url"]))
	}
	if argParser.Args.HasFlag("--confluence-username") {
		genericArgs["confluence-username"] = argParser.Args.GetFlagNextVal("--confluence-username")
		nsPos := argParser.Args.GetFlagPos("--confluence-username")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasConfluenceUsername, genericArgs["confluence-username"]))
	} else if os.Getenv("CONFLUENCE_USERNAME") != "" {
		genericArgs["confluence-username"] = os.Getenv("CONFLUENCE_USERNAME")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasConfluenceUsernameEnv, genericArgs["confluence-username"]))
	}
	if argParser.Args.HasFlag("--confluence-apitoken") {
		genericArgs["confluence-apitoken"] = argParser.Args.GetFlagNextVal("--confluence-apitoken")
		nsPos := argParser.Args.GetFlagPos("--confluence-apitoken")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasConfluenceApiToken, genericArgs["confluence-apitoken"]))
	} else if os.Getenv("CONFLUENCE_API_TOKEN") != "" {
		genericArgs["confluence-apitoken"] = os.Getenv("CONFLUENCE_API_TOKEN")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasConfluenceApiTokenEnv, genericArgs["confluence-apitoken"]))
	}
	if argParser.Args.HasFlag("--confluence-spacekey") {
		genericArgs["confluence-spacekey"] = argParser.Args.GetFlagNextVal("--confluence-spacekey")
		nsPos := argParser.Args.GetFlagPos("--confluence-spacekey")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasConfluenceSpaceKey, genericArgs["confluence-spacekey"]))
	} else if os.Getenv("CONFLUENCE_SPACE_KEY") != "" {
		genericArgs["confluence-spacekey"] = os.Getenv("CONFLUENCE_SPACE_KEY")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasConfluenceSpaceKeyEnv, genericArgs["confluence-spacekey"]))
	}
	if argParser.Args.HasFlag("--confluence-title") {
		genericArgs["confluence-title"] = argParser.Args.GetFlagNextVal("--confluence-title")
		nsPos := argParser.Args.GetFlagPos("--confluence-title")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasConfluencePageTitle, genericArgs["confluence-title"]))
	} else if os.Getenv("CONFLUENCE_PAGE_TITLE") != "" {
		genericArgs["confluence-title"] = os.Getenv("CONFLUENCE_PAGE_TITLE")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasConfluencePageTitleEnv, genericArgs["confluence-title"]))
	}
	if argParser.Args.HasFlag("--confluence-content") {
		genericArgs["confluence-content"] = argParser.Args.GetFlagNextVal("--confluence-content")
		nsPos := argParser.Args.GetFlagPos("--confluence-content")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasConfluencePageContent, genericArgs["confluence-content"]))
	} else if os.Getenv("CONFLUENCE_PAGE_HTML_CONTENT") != "" {
		genericArgs["confluence-content"] = os.Getenv("CONFLUENCE_PAGE_HTML_CONTENT")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasConfluencePageContentEnv, genericArgs["confluence-content"]))
	}
	if argParser.Args.HasFlag("--confluence-pageid") {
		genericArgs["confluence-pageid"] = argParser.Args.GetFlagNextVal("--confluence-pageid")
		nsPos := argParser.Args.GetFlagPos("--confluence-pageid")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasConfluencePageID, genericArgs["confluence-pageid"]))
	} else if os.Getenv("CONFLUENCE_PAGE_ID") != "" {
		genericArgs["confluence-pageid"] = os.Getenv("CONFLUENCE_PAGE_ID")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasConfluencePageIDEnv, genericArgs["confluence-pageid"]))
	}
	if argParser.Args.HasFlag("--confluence-parentid") {
		genericArgs["confluence-parentid"] = argParser.Args.GetFlagNextVal("--confluence-parentid")
		nsPos := argParser.Args.GetFlagPos("--confluence-parentid")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasConfluencePageParentID, genericArgs["confluence-parentid"]))
	} else if os.Getenv("CONFLUENCE_PARENT_ID") != "" {
		genericArgs["confluence-parentid"] = os.Getenv("CONFLUENCE_PARENT_ID")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasConfluencePageParentIDEnv, genericArgs["confluence-parentid"]))
	}

	// gsheet options
	if argParser.Args.HasFlag("--gsheet-spreadsheet-id") {
		genericArgs["gsheet-spreadsheet-id"] = argParser.Args.GetFlagNextVal("--gsheet-spreadsheet-id")
		genericArgs["gsheet-mode"] = "update"
		nsPos := argParser.Args.GetFlagPos("--gsheet-spreadsheet-id")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasGSheetSpreadsheetID, genericArgs["gsheet-spreadsheet-id"]))
	} else if os.Getenv("GSPREADSHEET_ID") != "" {
		genericArgs["gsheet-spreadsheet-id"] = os.Getenv("GSPREADSHEET_ID")
		genericArgs["gsheet-mode"] = "update"
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasGSheetSpreadsheetIDEnv, genericArgs["gsheet-spreadsheet-id"]))
	}
	if argParser.Args.HasFlag("--gsheet-spreadsheet-title") {
		genericArgs["gsheet-spreadsheet-title"] = argParser.Args.GetFlagNextVal("--gsheet-spreadsheet-title")
		nsPos := argParser.Args.GetFlagPos("--gsheet-spreadsheet-title")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasGSheetSpreadsheetTitle, genericArgs["gsheet-spreadsheet-title"]))
	} else if os.Getenv("GSPREADSHEET_TITLE") != "" {
		genericArgs["gsheet-spreadsheet-title"] = os.Getenv("GSPREADSHEET_TITLE")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasGSheetSpreadsheetTitleEnv, genericArgs["gsheet-spreadsheet-title"]))
	}
	if argParser.Args.HasFlag("--gsheet-sheet-title") {
		genericArgs["gsheet-sheet-title"] = argParser.Args.GetFlagNextVal("--gsheet-sheet-title")
		nsPos := argParser.Args.GetFlagPos("--gsheet-sheet-title")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasGSheetSheetTitle, genericArgs["gsheet-sheet-title"]))
	} else if os.Getenv("GSPREADSHEET_SHEET_TITLE") != "" {
		genericArgs["gsheet-sheet-title"] = os.Getenv("GSPREADSHEET_SHEET_TITLE")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasGSheetSheetTitleEnv, genericArgs["gsheet-sheet-title"]))
	}
	if argParser.Args.HasFlag("--gdrive-creds-filename") {
		genericArgs["gdrive-creds-filename"] = argParser.Args.GetFlagNextVal("--gdrive-creds-filename")
		nsPos := argParser.Args.GetFlagPos("--gdrive-creds-filename")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasGDriveCredFilename, genericArgs["gdrive-creds-filename"]))
	} else if os.Getenv("GDRIVE_CREDENTIAL_FILENAME") != "" {
		genericArgs["gdrive-creds-filename"] = os.Getenv("GDRIVE_CREDENTIAL_FILENAME")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasGDriveCredFilenameEnv, genericArgs["gdrive-creds-filename"]))
	}
	if argParser.Args.HasFlag("--gdrive-creds-path") {
		genericArgs["gdrive-creds-path"] = argParser.Args.GetFlagNextVal("--gdrive-creds-path")
		nsPos := argParser.Args.GetFlagPos("--gdrive-creds-path")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasGDriveCredPath, genericArgs["gdrive-creds-path"]))
	} else if os.Getenv("GDRIVE_CREDENTIAL_PATH") != "" {
		genericArgs["gdrive-creds-path"] = os.Getenv("GDRIVE_CREDENTIAL_PATH")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasGDriveCredPathEnv, genericArgs["gdrive-creds-path"]))
	}
	if argParser.Args.HasFlag("--gdrive-creds-domain") {
		genericArgs["gdrive-creds-domain"] = argParser.Args.GetFlagNextVal("--gdrive-creds-domain")
		nsPos := argParser.Args.GetFlagPos("--gdrive-creds-domain")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasGDriveCredDomain, genericArgs["gdrive-creds-domain"]))
	} else if os.Getenv("GDRIVE_CREDENTIAL_DOMAIN") != "" {
		genericArgs["gdrive-creds-domain"] = os.Getenv("GDRIVE_CREDENTIAL_DOMAIN")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasGDriveCredDomainEnv, genericArgs["gdrive-creds-domain"]))
	}
	if argParser.Args.HasFlag("--gsheet-mode") {
		genericArgs["output-type"] = "gsheet"
		genericArgs["gsheet-mode"] = argParser.Args.GetFlagNextVal("--gsheet-mode")
		nsPos := argParser.Args.GetFlagPos("--gsheet-mode")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasGSheetMode, genericArgs["gsheet-mode"]))
	}

	// mail options
	if argParser.Args.HasFlag("--mail-to") {
		genericArgs["output-type"] = "mail"
		genericArgs["mail-to"] = argParser.Args.GetFlagNextVal("--mail-to")
		nsPos := argParser.Args.GetFlagPos("--mail-to")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasMailTo, genericArgs["mail-to"]))
	} else if genericArgs["output-type"] == "mail" && os.Getenv("SXCOLLECTOR_TO") != "" {
		genericArgs["mail-to"] = os.Getenv("SXCOLLECTOR_TO")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasMailToEnv, genericArgs["mail-to"]))
	}
	if argParser.Args.HasFlag("--mail-from") {
		genericArgs["output-type"] = "mail"
		genericArgs["mail-from"] = argParser.Args.GetFlagNextVal("--mail-from")
		nsPos := argParser.Args.GetFlagPos("--mail-from")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasMailFrom, genericArgs["mail-from"]))
	} else if genericArgs["output-type"] == "mail" && os.Getenv("MAIL_FROM") != "" {
		genericArgs["mail-from"] = os.Getenv("MAIL_FROM")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasMailFromEnv, genericArgs["mail-from"]))
	}
	if argParser.Args.HasFlag("--mail-subject") {
		genericArgs["mail-subject"] = argParser.Args.GetFlagNextVal("--mail-subject")
		nsPos := argParser.Args.GetFlagPos("--mail-subject")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasMailSubject, genericArgs["mail-subject"]))
	} else if genericArgs["output-type"] == "mail" && os.Getenv("MAIL_SUBJECT") != "" {
		genericArgs["mail-subject"] = os.Getenv("MAIL_SUBJECT")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasMailSubjectEnv, genericArgs["mail-subject"]))
	}
	if argParser.Args.HasFlag("--smtp-host") {
		genericArgs["smtp-host"] = argParser.Args.GetFlagNextVal("--smtp-host")
		nsPos := argParser.Args.GetFlagPos("--smtp-host")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasSmtpHost, genericArgs["smtp-host"]))
	} else if genericArgs["output-type"] == "mail" && os.Getenv("SMTP_HOST") != "" {
		genericArgs["smtp-host"] = os.Getenv("SMTP_HOST")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasSmtpHostEnv, genericArgs["smtp-host"]))
	}
	if argParser.Args.HasFlag("--smtp-port") {
		genericArgs["smtp-port"] = argParser.Args.GetFlagNextVal("--smtp-port")
		nsPos := argParser.Args.GetFlagPos("--smtp-port")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasSmtpPort, genericArgs["smtp-port"]))
	} else if genericArgs["output-type"] == "mail" && os.Getenv("SMTP_PORT") != "" {
		genericArgs["smtp-port"] = os.Getenv("SMTP_PORT")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasSmtpPortEnv, genericArgs["smtp-port"]))
	}
	if argParser.Args.HasFlag("--smtp-username") {
		genericArgs["smtp-username"] = argParser.Args.GetFlagNextVal("--smtp-username")
		nsPos := argParser.Args.GetFlagPos("--smtp-username")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasSmtpUsername, genericArgs["smtp-username"]))
	} else if genericArgs["output-type"] == "mail" && os.Getenv("SMTP_USERNAME") != "" {
		genericArgs["smtp-username"] = os.Getenv("SMTP_USERNAME")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasSmtpUsernameEnv, genericArgs["smtp-username"]))
	}
	if argParser.Args.HasFlag("--smtp-password") {
		genericArgs["smtp-password"] = argParser.Args.GetFlagNextVal("--smtp-password")
		nsPos := argParser.Args.GetFlagPos("--smtp-password")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasSmtpPassword, "xxxxxx"))
	} else if genericArgs["output-type"] == "mail" && os.Getenv("SMTP_PASSWORD") != "" {
		genericArgs["smtp-password"] = os.Getenv("SMTP_PASSWORD")
		display.Debug(fmt.Sprintf(dbgSXQAPCheckHasSmtpPasswordEnv, "xxxxxx"))
	}
	if argParser.Args.HasFlag("--smtp-insecure") {
		genericArgs["smtp-insecure"] = "true"
		nsPos := argParser.Args.GetFlagPos("--smtp-insecure")
		argParser.Args.RemoveFlagFromPos(nsPos)
		display.Debug(dbgSXQAPCheckHasSmtpInsecure)
	} else if os.Getenv("SMTP_INSECURE") == "true" {
		genericArgs["insecure"] = "true"
		display.Debug(dbgSXQAPCheckHasSmtpInsecureEnv)
	}
	// process trailling optionn as subaction
	if len(argParser.Args.ParsedArgs) > 1 && strings.HasPrefix(argParser.Args.SubAction, "--") {
		argParser.Args.SubAction = ""
	}
	argParser.PluginGenericsArgs = genericArgs
	return nil
}

// used to scan arguments remove the globals one and return a new arguments
// list without them
func (argParser *sxCollectorArgParser) Prepare() error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	needK8sClient := false
	hasKubeconfig := false
	kubeconfigFile := ""
	display.Debug(dbgSXQAPPrepare)
	args := argParser.Args
	errCheck := argParser.Check()
	if errCheck != nil {
		display.ExitError(
			fmt.Sprintf(
				errSXQAPPrepareCheck,
				errCheck,
			),
			30,
		)
	}

	if args.TopAction != "" {
		display.Debug(fmt.Sprintf(dbgSXQAPPrepareAction, args.TopAction))
		switch args.TopAction {
		case "version":
			return nil
		default:
			pluginName := sxPluginCore.GetNameBySubcommand(args.TopAction)
			if pluginName == "" {
				display.ExitError(fmt.Sprintf(dbgGetNameBySubcommandNotFound, args.TopAction), 20)
			}
			plugin, errLoad := sxPluginCore.LoadPlugin(pluginName)
			if errLoad != nil {
				display.ExitError(errLoad.Error(), 20)
			}
			display.Debug(fmt.Sprintf(dbgSXQAPPrepareActionFoundPlugin, plugin.GetName()))
			needK8sClient = plugin.NeedK8sClient()
			argParser.Plugin = plugin
		}
	} else {
		display.Debug(dbgSXQAPPrepareNoAction)
	}
	if args.SubAction != "" {
		display.Debug(fmt.Sprintf(dbgSXQAPPrepareSubAction, args.SubAction))
	} else {
		display.Debug(dbgSXQAPPrepareNoSubAction)
	}
	// check global flags namespace and kubeconfig
	if argParser.Args.HasFlag("--namespace") {
		argParser.HasNamespace = true
		argParser.Namespace = argParser.Args.GetFlagNextVal("--namespace")
		nsPos := argParser.Args.GetFlagPos("--namespace")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
	}
	if argParser.Args.HasFlag("--kubeconfig") {
		hasKubeconfig = true
		kubeconfigFile = argParser.Args.GetFlagNextVal("--kubeconfig")
		nsPos := argParser.Args.GetFlagPos("--kubeconfig")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
	}
	// load k8sclient if needed
	forceInCluster := true
	display.Debug(fmt.Sprintf(dbgSXQAPPrepareNeedK8sClient, args.TopAction, needK8sClient))
	if needK8sClient && !(argParser.Args.HasFlag("--help") || args.SubAction == "help") {

		kubeconfig := sxKCfg.NewKubeConfig()
		if hasKubeconfig {
			display.Debug(fmt.Sprintf(dbgSXQAPPrepareHasKubeconfigArg, kubeconfigFile))
			if kubeconfigFile != "" {
				_, errLoadFile := kubeconfig.LoadFromFile(kubeconfigFile)
				if errLoadFile != nil {
					display.ExitError(errLoadFile.Error(), 20)
				} else {
					forceInCluster = false
				}
			} else {
				display.ExitError(dbgSXQAPPrepareNoKubeconfigFile, 20)
			}
		} else {
			_, errLoadEnv := kubeconfig.LoadFromEnv(false)
			if errLoadEnv != nil {
				display.Debug(fmt.Sprintf(errPrepareNoKubeEnv, errLoadEnv))
				_, errLoadHome := kubeconfig.LoadFromHome()
				if errLoadHome != nil {
					display.Debug(fmt.Sprintf(errPrepareNoKubeHome, errLoadHome))
				} else {
					forceInCluster = false
				}
			} else {
				forceInCluster = false
			}

		}
		k8sclient := sxKCli.NewK8sClient(kubeconfig.GetPath(), false, forceInCluster)
		if !argParser.HasNamespace {
			argParser.Namespace = k8sclient.GetCurrentNamespace()
		}
		argParser.Plugin.Load(k8sclient)
		argParser.PluginLoaded = true
	} else {
		argParser.Plugin.LoadNoK8s()
		argParser.PluginLoaded = true
	}
	return nil
}

// used to scan arguments remove the globals one and return a new arguments
// list without them
func (argParser *sxCollectorArgParser) Exec() error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(dbgSXQAPExec)
	args := argParser.Args

	switch args.TopAction {
	case "version":
		display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
		DisplayVersion()
	default:
		display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
		if argParser.PluginLoaded {
			if argParser.Args.IsHelp || argParser.Args.HasFlag("--help") || args.SubAction == "help" || argParser.Args.TopAction == "" {
				argParser.DisplayHelp()
			} else {
				concatenatedArgs := ""
				if len(argParser.Args.ParsedArgs) > 1 {
					concatenatedArgs = strings.Join(argParser.Args.ParsedArgs[2:], " ")
				}
				errExec := argParser.Plugin.Exec(concatenatedArgs, argParser.PluginGenericsArgs)
				if errExec != nil {
					display.ExitError(errExec.Error(), 20)
				}
				display.Debug(fmt.Sprintf(dbgSXQAPExecPluginAction, argParser.Plugin.GetName()))
				errOutput := argParser.Plugin.Output(concatenatedArgs, argParser.PluginGenericsArgs)
				if errOutput != nil {
					display.ExitError(errOutput.Error(), 20)
				}
			}
		} else {
			display.Debug(fmt.Sprintf(dbgSXQAPExecPluginNotLoaded, argParser.Plugin.GetName()))
			if argParser.Args.IsHelp || argParser.Args.HasFlag("--help") || args.SubAction == "help" {
				argParser.DisplayHelp()
			}
		}
	}
	return nil
}

// Return the help message
func (argParser *sxCollectorArgParser) DisplayHelp() {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(dbgSXCLHelp)

	// Check for main subcommands
	if len(argParser.Args.ParsedArgs) <= 1 {
		sxCollector.DisplayHelpCmd()
	} else {
		fmt.Println(argParser.Plugin.GetHelp())
	}
}
