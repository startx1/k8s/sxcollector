package plugincore

import (
	sxKCli "gitlab.com/startx1/k8s/go-libs/pkg/k8sclient"
)

const (
	GroupNameSXCollector = "plugincore"
)

// Plugin interface definition
type PluginInterface interface {
	GetHelp() string
	GetName() string
	GetSubcommand() string
	NeedK8sClient() bool
	NeedOCP() bool
	Exec(string, map[string]string) error
	Output(string, map[string]string) error
	Read() string
	Init() error
	Load(*sxKCli.K8sClient) error
	LoadNoK8s() error
	RecordResultCSV(string, bool, bool) error
	RecordResultTAB(bool, bool) error
	RecordResultMail(map[string]string) error
	RecordResultConfluence(map[string]string) error
}
