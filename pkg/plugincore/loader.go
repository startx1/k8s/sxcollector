package plugincore

import (
	"errors"
	"fmt"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
)

const (
	dbgRegisterPluginOK            = "Plugin %s registered"
	dbgLoadPluginOK                = "Loading plugin %s"
	dbgLoadPluginNOK               = "could not load plugin %s because it is not registered"
	dbgInitializePluginsStart      = "Initializing plugins"
	dbgInitializePluginsNOK        = "Error loading plugin %s: %v"
	dbgGetNameBySubcommandNotFound = "could not find plugin for subcommand %s"
)

// PluginRegistry holds registered plugin constructors
var PluginRegistry = map[string]func() PluginInterface{}

// RegisterPlugin allows plugins to register themselves by name
func RegisterPlugin(name string, constructor func() PluginInterface) {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	PluginRegistry[name] = constructor
	display.Debug(fmt.Sprintf(dbgRegisterPluginOK, name))
}

// LoadPlugin dynamically loads a plugin by name
func LoadPlugin(name string) (PluginInterface, error) {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	constructor, exists := PluginRegistry[name]
	if !exists {
		errMsg := fmt.Sprintf(dbgLoadPluginNOK, name)
		display.Error(errMsg)
		return nil, errors.New(errMsg)
	}
	if name != "Blank" {
		display.Debug(fmt.Sprintf(dbgLoadPluginOK, name))
	}
	return constructor(), nil
}

// GetNameBySubcommand return the plugin name for a subcommand
func GetNameBySubcommand(subcommand string) string {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	for name, constructor := range PluginRegistry {
		plugin := constructor()
		if subcommand == plugin.GetSubcommand() {
			return name
		}
	}
	display.Debug(fmt.Sprintf(dbgGetNameBySubcommandNotFound, subcommand))
	return ""
}

// InitializePlugins dynamically loads and initializes all registered plugins
func InitializePlugins() {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(dbgInitializePluginsStart)
	for name, constructor := range PluginRegistry {
		plugin := constructor()
		err := plugin.Init()
		if err != nil {
			display.Error(fmt.Sprintf(dbgInitializePluginsNOK, name, err))
		}
	}
}
