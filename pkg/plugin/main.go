package plugin

// Importer les types de route OpenShift

// Importer le schéma du client Kubernetes

const (
	GroupNameSXCollector                        = "plugin"
	DefaultPlugin                               = "Blank"
	DefaultPrecision                            = 1
	dbgPluginInit                               = "%s : Initialize plugin"
	dbgPluginLoadConnected                      = "%s : K8sClient is connected"
	dbgPluginExtractRQDataFromNamespace         = "%s : Start extracting resourcequota data from namespace %s"
	dbgPluginLoadNotConnected                   = "%s : K8sClient is not connected"
	dbgPluginLoadNotUsed                        = "%s : K8sClient is not used"
	dbgPluginLoaded                             = "%s : Plugin is loaded"
	dbgPluginExec                               = "%s : Start creating response for subcommand %s"
	errPluginExecNOK                            = "error recording result : %v"
	errPluginRecordResultWriteHeader            = "error writing csv headers : %v"
	errPluginRecordResultWriteUnits             = "error writing csv units header : %v"
	errPluginRecordResultWriteLine              = "error writing csv row : %v"
	errPluginRecordResultWriteFlush             = "error writing csv content to buffer : %v"
	dbgPluginGenerateRaw                        = "%s : Start generating Raw content"
	dbgPluginGenerateRawPrometheusData          = "%s : Start getting Prometheus data from %s"
	dbgPluginGenerateRawPrometheusDataQuery     = "%s : Start getting Prometheus data for query %s"
	errPluginGenerateRaw                        = "error generating Raw content : %v"
	errPluginGenerateRawGetK8S                  = "error getting K8s backend : %v"
	errPluginGenerateRawGetOCP                  = "error getting Openshift backend : %v"
	errPluginGenerateRawGetClient               = "error getting Openshif client backend : %v"
	errPluginGenerateRawGetPodsInfo             = "error getting pods info for node %s : %v"
	errPluginGenerateRawRegexNOK                = "could not compile regex %s because %v"
	errPluginGenerateRawGetIsOCP                = "could not find namespaces because %v"
	errPluginGenerateRawFindNsNOK               = "could not find namespaces : %v"
	dbgPluginRecordResultConfluenceBegin        = "%s : Begin creating Confluence content"
	dbgPluginRecordResultConfluenceEnd          = "%s : End creating Confluence content"
	valPluginRecordResultConfluenceTitle        = "Report SXCollector %s at %s"
	valPluginRecordResultConfluenceDateFormat   = "2006-01-02 15:04:05"
	valPluginRecordResultConfluenceContent      = "<h2>Report summary</h2><br/><br/>This is a report generated by SXCollector %s at %s<br/><h2>Report content</h2>"
	dbgPluginRecordResultConfluenceAppend       = "Page %s is appended in %s"
	errPluginRecordResultConfluenceAppend       = "error while appending confluence page: %v"
	dbgPluginRecordResultConfluenceUpdate       = "Page %s is updated in %s"
	errPluginRecordResultConfluenceUpdate       = "error while updating confluence page: %v"
	dbgPluginRecordResultConfluenceCreate       = "Page %s is created in %s (parent: %s)"
	errPluginRecordResultConfluenceCreate       = "error while creating confluence page: %v"
	dbgPluginRecordResultGSheetBegin            = "%s : Begin creating GSheet content"
	dbgPluginRecordResultGSheetEnd              = "%s : End creating GSheet content"
	valPluginRecordResultGSheetSpreadsheetTitle = "Report SXCollector"
	valPluginRecordResultGSheetSheetTitle       = "%s-%s"
	valPluginRecordResultGDriveCredsFilename    = "credentials.json"
	valPluginRecordResultGDriveCredsPath        = "/tmp"
	valPluginRecordResultGDriveCredsDomain      = "example.com"
	valPluginRecordResultGSheetDateFormat       = "2006-01-02"
	dbgPluginRecordResultGSheetAppend           = "Spreadsheet %s is appended in %s"
	errPluginRecordResultGSheetAppend           = "error while appending spreadsheet : %v"
	dbgPluginRecordResultGSheetUpdate           = "Spreadsheet %s is updated in %s"
	errPluginRecordResultGSheetUpdate           = "error while updating spreadsheet : %v"
	dbgPluginRecordResultGSheetCreate           = "Spreadsheet %s is created with sheet %s)"
	errPluginRecordResultGSheetCreate           = "error while creating spreadsheet : %v"
	valPluginRecordResultMailTitle              = "Report SXCollector %s at %s"
	valPluginRecordResultMailDateFormat         = "2006-01-02 15:04:05"
	valPluginRecordResultMailContent            = "Hello\n\nThis is a report generated by SXCollector %s at %s\n Please read the attachmennts for more informations."
)
