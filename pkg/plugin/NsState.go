package plugin

import (
	"context"
	"errors"
	"fmt"
	"log"
	"regexp"
	"sort"
	"strings"
	"time"

	sxConfluence "gitlab.com/startx1/k8s/go-libs/pkg/confluence"
	sxGDrive "gitlab.com/startx1/k8s/go-libs/pkg/gdrive"
	sxKCli "gitlab.com/startx1/k8s/go-libs/pkg/k8sclient"
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	sxPluginCore "gitlab.com/startx1/k8s/sxcollector/pkg/plugincore"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Struct of the NsState plugin (implement PluginInterface)
type NsState struct {
	kc            *sxKCli.K8sClient
	needK8sClient bool
	needOCP       bool
	Name          string
	Subcommand    string
	HelpMsg       string
	IsLoaded      bool
	Rows          [][]string
	Result        string
	header        []string
	units         []string
}

// Get the Name of the plugin
func (c *NsState) GetName() string {
	return c.Name
}

// Get the Subcommand of the plugin
func (c *NsState) GetSubcommand() string {
	return c.Subcommand
}

// Get the NeedK8sClient of the plugin
func (c *NsState) NeedK8sClient() bool {
	return c.needK8sClient
}

// Get the NeedOCP of the plugin
func (c *NsState) NeedOCP() bool {
	return c.needOCP
}

// Get the Help Message
func (c *NsState) GetHelp() string {
	return c.HelpMsg
}

// Return the output of the plugin
func (c *NsState) Exec(subcommand string, pluginArgs map[string]string) error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPluginExec, c.Name, subcommand))
	err := c.GenerateRaw(subcommand, pluginArgs)
	if err != nil {
		display.Error(fmt.Sprintf(errPluginGenerateRaw, err))
		return err
	}
	switch {
	case pluginArgs["output-type"] == "mail":
		err = c.RecordResultMail(pluginArgs)
	case pluginArgs["output-type"] == "confluence":
		err = c.RecordResultConfluence(pluginArgs)
	case pluginArgs["output-type"] == "gsheet":
		err = c.RecordResultGSheet(pluginArgs)
	case pluginArgs["output-format"] == "csv":
		err = c.RecordResultCSV(
			pluginArgs["output-csv-separator"],
			pluginArgs["output-noheaders"] != "true",
			pluginArgs["output-nounits"] != "true",
		)
	default:
		err = c.RecordResultTAB(
			pluginArgs["output-noheaders"] != "true",
			pluginArgs["output-nounits"] != "true",
		)
	}
	if err != nil {
		display.Error(fmt.Sprintf(errPluginExecNOK, err))
		return err
	}
	return nil
}

// Return the output of the plugin
func (c *NsState) GenerateRaw(subcommand string, pluginArgs map[string]string) error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPluginGenerateRaw, c.Name))

	// Define the regexp term to filter namespaces
	regexNs := "*"
	if subcommand != "" {
		regexNs = subcommand
	}

	// compile the regexp
	pattern := "^" + strings.ReplaceAll(regexp.QuoteMeta(regexNs), `\*`, ".*") + "$"
	regex, err := regexp.Compile(pattern)
	if err != nil {
		return fmt.Errorf(errPluginGenerateRawRegexNOK, regexNs, err)
	}

	// List all namespaces
	namespaces, err := c.kc.Clientset.CoreV1().Namespaces().List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		return fmt.Errorf(errPluginGenerateRawFindNsNOK, err)
	}

	c.Rows = [][]string{}
	// Filter namespaces matching the regexp pattern
	for _, ns := range namespaces.Items {
		if regex.MatchString(ns.Name) {
			age := fmt.Sprintf("%d", int(time.Since(ns.CreationTimestamp.Time).Hours()))
			created := ns.CreationTimestamp.String()
			status := string(ns.Status.Phase)
			component := ""
			partof := ""
			podSecEnforce := ""
			ocpMonitor := ""
			description := ns.Annotations["openshift.io/description"]
			longName := ns.Annotations["openshift.io/display-name"]
			requester := ns.Annotations["openshift.io/requester"]
			nodeSelector := ns.Annotations["openshift.io/node-selector"]
			for key := range ns.Labels {
				if key == "pod-security.kubernetes.io/enforce" {
					podSecEnforce = ns.Labels[key]
				}
				if key == "openshift.io/cluster-monitoring" {
					ocpMonitor = ns.Labels[key]
				}
				if key == "app.kubernetes.io/managed-by" {
					requester = ns.Labels[key]
				}
				if key == "app.kubernetes.io/component" {
					component = ns.Labels[key]
				}
				if key == "app.kubernetes.io/part-of" {
					partof = ns.Labels[key]
				}
			}

			row := []string{
				ns.Name,
				created,
				age,
				status,
				podSecEnforce,
				ocpMonitor,
				description,
				longName,
				requester,
				nodeSelector,
				component,
				partof,
			}
			c.Rows = append(c.Rows, row)
		}
	}

	// Sort by Namespace after processing
	sort.Slice(c.Rows, func(i, j int) bool {
		return c.Rows[i][0] < c.Rows[j][0]
	})

	return nil
}

// Send the content to mail
func (c *NsState) RecordResultMail(pluginArgs map[string]string) error {
	errGen := c.RecordResultCSV(
		pluginArgs["output-csv-separator"],
		pluginArgs["output-noheaders"] != "true",
		pluginArgs["output-nounits"] != "true",
	)
	if errGen != nil {
		return errGen
	}
	formattedTime := time.Now().Format(valPluginRecordResultMailDateFormat)
	if pluginArgs["mail-title"] == "" {
		pluginArgs["mail-title"] = fmt.Sprintf(valPluginRecordResultMailTitle, c.Name, formattedTime)
	} else {
		pluginArgs["mail-title"] = fmt.Sprintf(pluginArgs["mail-title"], c.Name, formattedTime)
	}
	title := pluginArgs["mail-title"]
	message := fmt.Sprintf(
		valPluginRecordResultMailContent,
		c.Name,
		formattedTime,
	)
	filename := fmt.Sprintf("%s.csv", c.Name)

	mail := HelperPrepareMail(
		c.Name,
		pluginArgs["mail-from"],
		pluginArgs["mail-to"],
		title,
		message,
		filename,
		[]byte(c.Result),
	)

	_, errMailer := HelperSendMail(
		c.Name,
		pluginArgs["smtp-host"],
		pluginArgs["smtp-port"],
		pluginArgs["smtp-username"],
		pluginArgs["smtp-password"],
		pluginArgs["smtp-insecure"] == "true",
		mail,
	)
	if errMailer != nil {
		return errMailer
	}
	return nil
}

// Record a confluence page
func (c *NsState) RecordResultConfluence(pluginArgs map[string]string) error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPluginRecordResultConfluenceBegin, c.Name))
	errGen := c.RecordResultHTML(
		pluginArgs["output-noheaders"] != "true",
		pluginArgs["output-nounits"] != "true",
	)
	if errGen != nil {
		return errGen
	}

	formattedTime := time.Now().Format(valPluginRecordResultConfluenceDateFormat)
	if pluginArgs["confluence-title"] == "" {
		pluginArgs["confluence-title"] = fmt.Sprintf(valPluginRecordResultConfluenceTitle, c.Name, formattedTime)
	}
	if pluginArgs["confluence-content"] == "" {
		pluginArgs["confluence-content"] = fmt.Sprintf(
			valPluginRecordResultConfluenceContent,
			c.Name,
			formattedTime,
		)
	}

	confluence := sxConfluence.NewConfluence(
		pluginArgs["confluence-url"],
		pluginArgs["confluence-username"],
		pluginArgs["confluence-apitoken"],
		pluginArgs["confluence-spacekey"],
	)
	confluencePage := sxConfluence.NewConfluencePage(
		pluginArgs["confluence-spacekey"],
		pluginArgs["confluence-pageid"],
		pluginArgs["confluence-parentid"],
		pluginArgs["confluence-title"],
		pluginArgs["confluence-content"]+c.Result,
	)
	switch pluginArgs["confluence-mode"] {
	case "append":
		_, errAppend := confluence.CreateOrAppendPage(confluencePage, "<br/>")
		if errAppend != nil {
			return fmt.Errorf(errPluginRecordResultConfluenceAppend, errAppend)
		} else {
			display.Info(
				fmt.Sprintf(
					dbgPluginRecordResultConfluenceAppend,
					pluginArgs["confluence-pageid"],
					pluginArgs["confluence-spacekey"],
				),
			)
		}
	case "update":
		_, errUpdate := confluence.UpdatePage(confluencePage)
		if errUpdate != nil {
			return fmt.Errorf(errPluginRecordResultConfluenceUpdate, errUpdate)
		} else {
			display.Info(
				fmt.Sprintf(
					dbgPluginRecordResultConfluenceUpdate,
					pluginArgs["confluence-pageid"],
					pluginArgs["confluence-spacekey"],
				),
			)
		}
	case "create":
		_, errCreate := confluence.CreatePage(confluencePage)
		if errCreate != nil {
			return fmt.Errorf(errPluginRecordResultConfluenceCreate, errCreate)
		} else {
			display.Info(
				fmt.Sprintf(
					dbgPluginRecordResultConfluenceCreate,
					pluginArgs["confluence-title"],
					pluginArgs["confluence-spacekey"],
					pluginArgs["confluence-parentid"],
				),
			)
		}
	default:
		_, errCreate := confluence.CreatePage(confluencePage)
		if errCreate != nil {
			return fmt.Errorf(errPluginRecordResultConfluenceCreate, errCreate)
		} else {
			display.Info(
				fmt.Sprintf(
					dbgPluginRecordResultConfluenceCreate,
					pluginArgs["confluence-title"],
					pluginArgs["confluence-spacekey"],
					pluginArgs["confluence-parentid"],
				),
			)
		}
	}

	switch {
	case pluginArgs["output-format"] == "csv":
		errGen = c.RecordResultCSV(
			pluginArgs["output-csv-separator"],
			pluginArgs["output-noheaders"] != "true",
			pluginArgs["output-nounits"] != "true",
		)
	default:
		errGen = c.RecordResultTAB(
			pluginArgs["output-noheaders"] != "true",
			pluginArgs["output-nounits"] != "true",
		)
	}
	if errGen != nil {
		return errGen
	}
	display.Debug(fmt.Sprintf(dbgPluginRecordResultConfluenceEnd, c.Name))
	return nil
}

// Record a google sheet
func (c *NsState) RecordResultGSheet(pluginArgs map[string]string) error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPluginRecordResultGSheetBegin, c.Name))
	mapContent, errGen := HelperGenerateMap(
		c.Name,
		c.header,
		c.units,
		c.Rows,
		pluginArgs["output-noheaders"] != "true",
		pluginArgs["output-nounits"] != "true",
	)
	if errGen != nil {
		return errGen
	}
	formattedTime := time.Now().Format(valPluginRecordResultGSheetDateFormat)
	if pluginArgs["gsheet-sheet-title"] == "" {
		pluginArgs["gsheet-sheet-title"] = fmt.Sprintf(valPluginRecordResultGSheetSheetTitle, c.Name, formattedTime)
	}
	if pluginArgs["gsheet-spreadsheet-title"] == "" {
		pluginArgs["gsheet-spreadsheet-title"] = valPluginRecordResultGSheetSpreadsheetTitle
	}
	if pluginArgs["gdrive-creds-filename"] == "" {
		pluginArgs["gdrive-creds-filename"] = valPluginRecordResultGDriveCredsFilename
	}
	if pluginArgs["gdrive-creds-path"] == "" {
		pluginArgs["gdrive-creds-path"] = valPluginRecordResultGDriveCredsPath
	}
	if pluginArgs["gdrive-creds-domain"] == "" {
		pluginArgs["gdrive-creds-domain"] = valPluginRecordResultGDriveCredsDomain
	}

	gdrive := sxGDrive.NewGDrive(
		pluginArgs["gdrive-creds-filename"],
		pluginArgs["gdrive-creds-path"],
		pluginArgs["gdrive-creds-domain"],
	)
	switch pluginArgs["gsheet-mode"] {
	case "append":
		gspreadsheet := sxGDrive.NewGDriveSpreadsheet(gdrive, pluginArgs["gsheet-spreadsheet-id"])
		err := gspreadsheet.InitSpreadsheet()
		if err != nil {
			return fmt.Errorf(errPluginRecordResultGSheetAppend, err)
		}
		err = gspreadsheet.LoadSpreadsheet()
		if err != nil {
			return fmt.Errorf(errPluginRecordResultGSheetAppend, err)
		} else {
			_, err := gspreadsheet.UpsertSheet(pluginArgs["gsheet-sheet-title"])
			if err != nil {
				log.Fatalf("Unable to get sheet: %v", err)
			}
			err = HelperGdriveSpreadsheetUpdate(c.Name, gspreadsheet, pluginArgs["gsheet-sheet-title"], mapContent)
			if err != nil {
				return fmt.Errorf(errPluginRecordResultGSheetAppend, err)
			} else {
				display.Info(
					fmt.Sprintf(
						dbgPluginRecordResultGSheetAppend,
						gspreadsheet.SpreadsheetID,
						pluginArgs["gsheet-sheet-title"],
					),
				)
			}
		}
	case "update":
		gspreadsheet := sxGDrive.NewGDriveSpreadsheet(gdrive, pluginArgs["gsheet-spreadsheet-id"])
		err := gspreadsheet.InitSpreadsheet()
		if err != nil {
			return fmt.Errorf(errPluginRecordResultGSheetUpdate, err)
		}
		err = gspreadsheet.LoadSpreadsheet()
		if err != nil {
			return fmt.Errorf(errPluginRecordResultGSheetUpdate, err)
		} else {
			_, err := gspreadsheet.UpsertSheet(pluginArgs["gsheet-sheet-title"])
			if err != nil {
				log.Fatalf("Unable to get sheet: %v", err)
			}
			err = HelperGdriveSpreadsheetUpdate(c.Name, gspreadsheet, pluginArgs["gsheet-sheet-title"], mapContent)
			if err != nil {
				return fmt.Errorf(errPluginRecordResultGSheetUpdate, err)
			} else {
				display.Info(
					fmt.Sprintf(
						dbgPluginRecordResultGSheetUpdate,
						gspreadsheet.SpreadsheetID,
						pluginArgs["gsheet-sheet-title"],
					),
				)
			}
		}
	case "create":
		gspreadsheet := sxGDrive.NewGDriveSpreadsheet(gdrive, "")
		err := gspreadsheet.InitSpreadsheet()
		if err != nil {
			return fmt.Errorf(errPluginRecordResultGSheetCreate, err)
		}
		err = gspreadsheet.AddSpreadsheet(pluginArgs["gsheet-spreadsheet-title"], true)
		if err != nil {
			return fmt.Errorf(errPluginRecordResultGSheetCreate, err)
		} else {
			_, err := gspreadsheet.UpsertSheet(pluginArgs["gsheet-sheet-title"])
			if err != nil {
				log.Fatalf("Unable to get sheet: %v", err)
			}
			err = HelperGdriveSpreadsheetUpdate(c.Name, gspreadsheet, pluginArgs["gsheet-sheet-title"], mapContent)
			if err != nil {
				return fmt.Errorf(errPluginRecordResultGSheetCreate, err)
			} else {
				display.Info(
					fmt.Sprintf(
						dbgPluginRecordResultGSheetCreate,
						gspreadsheet.SpreadsheetID,
						pluginArgs["gsheet-sheet-title"],
					),
				)
			}
		}
	default:
		gspreadsheet := sxGDrive.NewGDriveSpreadsheet(gdrive, "")
		err := gspreadsheet.InitSpreadsheet()
		if err != nil {
			return fmt.Errorf(errPluginRecordResultGSheetCreate, err)
		}
		err = gspreadsheet.AddSpreadsheet(pluginArgs["gsheet-spreadsheet-title"], true)
		if err != nil {
			return fmt.Errorf(errPluginRecordResultGSheetCreate, err)
		} else {
			_, err := gspreadsheet.UpsertSheet(pluginArgs["gsheet-sheet-title"])
			if err != nil {
				log.Fatalf("Unable to get sheet: %v", err)
			}
			err = HelperGdriveSpreadsheetUpdate(c.Name, gspreadsheet, pluginArgs["gsheet-sheet-title"], mapContent)
			if err != nil {
				return fmt.Errorf(errPluginRecordResultGSheetCreate, err)
			} else {
				display.Info(
					fmt.Sprintf(
						dbgPluginRecordResultGSheetCreate,
						gspreadsheet.SpreadsheetID,
						pluginArgs["gsheet-sheet-title"],
					),
				)
			}
		}
	}

	switch {
	case pluginArgs["output-format"] == "csv":
		errGen = c.RecordResultCSV(
			pluginArgs["output-csv-separator"],
			pluginArgs["output-noheaders"] != "true",
			pluginArgs["output-nounits"] != "true",
		)
	default:
		errGen = c.RecordResultTAB(
			pluginArgs["output-noheaders"] != "true",
			pluginArgs["output-nounits"] != "true",
		)
	}
	if errGen != nil {
		return errGen
	}
	display.Debug(fmt.Sprintf(dbgPluginRecordResultGSheetEnd, c.Name))
	return nil
}

// Record the HTML version of the result
func (c *NsState) RecordResultHTML(hasHeader bool, hasSubHeader bool) error {
	htmlString, err := HelperGenerateHTML(
		c.Name,
		c.header,
		c.units,
		c.Rows,
		hasHeader,
		hasSubHeader,
	)
	if err != nil {
		return err
	}
	c.Result = htmlString
	return nil
}

// Record the CSV version of the result
func (c *NsState) RecordResultCSV(sep string, hasHeader bool, hasSubHeader bool) error {
	csvString, err := HelperGenerateCSV(
		c.Name,
		c.header,
		c.units,
		c.Rows,
		sep,
		hasHeader,
		hasSubHeader,
	)
	if err != nil {
		return err
	}
	c.Result = csvString
	return nil
}

// Record the TAB version of the result
func (c *NsState) RecordResultTAB(hasHeader bool, hasSubHeader bool) error {
	tabString, err := HelperGenerateTAB(
		c.Name,
		c.header,
		c.units,
		c.Rows,
		hasHeader,
		hasSubHeader,
	)
	if err != nil {
		return err
	}
	c.Result = tabString
	return nil
}

// Return the output of the plugin result
func (c *NsState) Read() string {
	return c.Result
}

// Output the result
func (c *NsState) Output(subcommand string, pluginArgs map[string]string) error {
	if pluginArgs["output-type"] == "file" {
		err := HelperWriteFile(c.Name, pluginArgs["output-file"], c.Read())
		if err != nil {
			return err
		}
		return nil
	} else if pluginArgs["output-type"] == "mail" {
		return nil
	} else {
		fmt.Println(c.Read())
	}
	return nil
}

// Load the plugin
func (c *NsState) Load(kc *sxKCli.K8sClient) error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	c.kc = kc
	if c.kc.IsConnected() {
		c.IsLoaded = true
		display.Debug(fmt.Sprintf(dbgPluginLoadConnected, c.Name))
		return nil
	} else {
		c.IsLoaded = false
		msg := fmt.Sprintf(dbgPluginLoadNotConnected, c.Name)
		display.Debug(msg)
		return errors.New(msg)
	}
}

// Load the plugin without K8S
func (c *NsState) LoadNoK8s() error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPluginLoaded, c.Name))
	c.IsLoaded = true
	return nil
}

// Init the plugin
func (c *NsState) Init() error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPluginInit, c.Name))
	return nil
}

// Register NsState plugin in the PluginRegistry
func init() {
	helpMessage := `
-- NAMESPACE STATE PLUGIN ---
-- Require : Kubernetes

ns-state subcommand allow you to extract your kubernetes namespaces informations into a csv file. 

Usage: sxcollector ns-state [NS_PATTERN] [OPTIONS]...

Arguments:
  NS_PATTERN                    A regex to used for multiple namespace selection. (use all namespaces if not provided)

Generic options:
  --insecure                    Skip TLS verification for prometheus communication (default false)

Output options:
  --output-format FORMAT        Set the formating of the generated content. Could be tab, json or csv (default is tab)
  --output-csv-separator SEP    Define the separator sign to use for the csv format (default is ,)
  --output-noheaders            Disable headers in the result (default is false)
  --output-nounits              Disable unit header in the result (default is false)
  --output-file FILENAME        Define the output file. If not set, stdout will be used (default is stdout)

Prometheus options:
  --insecure                    Skip TLS verification for prometheus communication (default false)
  --from DATE                   Define the starting date. ex: 2024-01-01 00:00:00 (default is now)
  --to DATE                     Define the ending date. ex: 2024-01-03. Have precedence over duration if both are set ( no default)
  --duration DURATION           Define the period duration. ex: 1y. Could be overwrite by --to (default is 1m)
  --precision PRECISION         A factor that divide the duration. Used for slicing prometheus data (default is 1 = all)

Mail options:
  --mail-to EMAIL               Define the destination mail. If set, output will be of type mail. SMTP params must be set
  --mail-from EMAIL             Define the source mail. If set, output will be of type mail. If not use MAIL_FROM env
  --mail-subject SUBJECT        Customize the mail subject. Default is "Report SXCollector %s at %s" where first arg is name of the plugin and second the date of execution
  --smtp-host HOST              Define the SMTP host. If output type is mail and host is not set, use the SMTP_HOST env var
  --smtp-port PORT              Define the SMTP port. If output type is mail and port is not set, use the SMTP_PORT env var
  --smtp-username USERNAME      Define the SMTP username. If output type is mail and username is not set, use the SMTP_USERNAME env var
  --smtp-password PASSWORD      Define the SMTP password. If output type is mail and password is not set, use the SMTP_PASSWORD env var
  --smtp-insecure               Define if SMTP connection is insecure. If output type is mail and --smtp-insecure is not set, use the SMTP_INSECURE=true env var

Confluence options:
  --confluence-url URL            The confluence base URL ex: https://example.atlassian.net/wiki. If --confluence-url is not set, use the CONFLUENCE_BASE_URL env var
  --confluence-username USERNAME  The confluence username ex: user@example.com. If --confluence-username is not set, use the CONFLUENCE_USERNAME env var
  --confluence-apitoken APITOKEN  The confluence API Token, see https://id.atlassian.com/manage-profile/security/api-tokens. If --confluence-apitoken is not set, use the CONFLUENCE_API_TOKEN env var
  --confluence-spacekey SPACEKEY  The confluence Space key. If --confluence-spacekey is not set, use the CONFLUENCE_SPACE_KEY env var
  --confluence-title TITLE        The confluence page title (or section if --confluence-mode=append). If --confluence-title is not set, use the CONFLUENCE_PAGE_TITLE env var
  --confluence-mode               Define the default confluence mode (could be create, update, append). Default is create
  --confluence-content CONTENT    The confluence page content (prepended to the generated content). If --confluence-content is not set, use the CONFLUENCE_PAGE_HTML_CONTENT env var 
  --confluence-pageid PAGEID      The confluence pageID (required except for --confluence-mode=create). If --confluence-pageid is not set, use the CONFLUENCE_PAGE_ID env var
  --confluence-parentid PARENTID  The confluence parentID (useful with --confluence-mode=create). If --confluence-parentid is not set, use the CONFLUENCE_PARENT_ID env var
Generic options:
  --debug                       Activates debug mode for detailed troubleshooting information.
  --help                        Displays this help message and exits.
  --kubeconfig FILEPATH         Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided)

Environment variables :

  SMTP_HOST                     Define the SMTP host in replacement of the --smtp-host argument
  SMTP_PORT                     Define the SMTP port in replacement of the --smtp-port argument
  SMTP_USERNAME                 Define the SMTP username in replacement of the --smtp-username argument
  SMTP_PASSWORD                 Define the SMTP password in replacement of the --smtp-password argument
  MAIL_FROM                     Define the source mail in replacement of the --mail-from argument
  MAIL_SUBJECT                  Define the source mail in replacement of the --mail-subject argument
  CONFLUENCE_BASE_URL           Define the confluence base URL in replacement of the --confluence-url argument
  CONFLUENCE_USERNAME           Define the confluence username in replacement of the --confluence-username argument
  CONFLUENCE_API_TOKEN          Define the confluence API Token in replacement of the --confluence-apitoken argument
  CONFLUENCE_SPACE_KEY          Define the confluence Space key in replacement of the --confluence-spacekey argument
  CONFLUENCE_PAGE_TITLE         Define the page title in replacement of the --confluence-title argument
  CONFLUENCE_PAGE_HTML_CONTENT  Define the page content in replacement of the --confluence-content argument
  CONFLUENCE_PAGE_ID            Define the confluence pageID in replacement of the --confluence-pageid argument
  CONFLUENCE_PARENT_ID          Define the confluence parentID in replacement of the --confluence-parentid argument
  SXCOLLECTOR_FORMAT            Define the output format in replacement of the --output-format argument
  SXCOLLECTOR_TO                Define the destination mail in replacement of the --mail-to argument
  SXCOLLECTOR_DURATION          Define the prometheus duration in replacement of the --duration argument
  SXCOLLECTOR_PRECISION         Define the prometheus precision in replacement of the --precision argument
  SXCOLLECTOR_INSECURE          Enable the insecure connection for prometheus in replacement of the --insecure argument
  SXCOLLECTOR_NOHEADERS         Disable the headers in replacement of the --output-noheaders argument
  SXCOLLECTOR_NOUNITS           Disable the units in replacement of the --output-nounits argument
  SXCOLLECTOR_SEP               Define the separator character in replacement of the --output-csv-separator argument

Examples:
  Display all namespaces state in a tabulated format :
  $ sxcollector ns-state
  Export all namespaces state into a csv, comma-separated output into a file :
  $ sxcollector ns-state --output-format csv --output-file /tmp/sxcollector.csv
  Display states of namespaces starting with kube into a csv format with '|' as separator :
  $ sxcollector ns-state kube* --output-format csv --output-csv-separator "|"

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	header := []string{
		"Namespace",
		"Created",
		"Age",
		"Status",
		"podSecEnforce",
		"ocpMonitor",
		"Description",
		"LongName",
		"Requester",
		"NodeSelector",
		"Component",
		"PartOf",
	}
	units := []string{
		"Namespace name",
		"Date of creation",
		"Age (in hours)",
		"State of the namespace",
		"PSP at the namespace level",
		"Openshift monitor enabled",
		"Openshift description",
		"Long namespace name",
		"the requester",
		"Openshift NodeSelector",
		"Component of the cluster",
		"bellong to this app",
	}

	sxPluginCore.RegisterPlugin("NsState", func() sxPluginCore.PluginInterface {
		return &NsState{
			Name:          "NsState",
			Subcommand:    "ns-state",
			HelpMsg:       helpMessage,
			needK8sClient: true,
			needOCP:       true,
			header:        header,
			units:         units,
		}
	})
}
