package plugin

import (
	"context"
	"fmt"

	// Importer les types de route OpenShift

	sxGDrive "gitlab.com/startx1/k8s/go-libs/pkg/gdrive"
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	"google.golang.org/api/sheets/v4"
	// Importer le schéma du client Kubernetes
)

const (
	dbgPHGdriveSpreadsheetUpdateBegin = "%s : Start creating in spreadsheet %s, the sheet %s"
	dbgPHGdriveSpreadsheetUpdateEnd   = "%s : End creating in spreadsheet %s, the sheet %s"
)

// Return the output of the plugin in [][]string
func HelperGdriveSpreadsheetUpdate(pluginName string, gspreadsheet *sxGDrive.GDriveSpreadsheet, sheetTitle string, mapContent [][]string) error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPHGdriveSpreadsheetUpdateBegin, pluginName, gspreadsheet.SpreadsheetID, sheetTitle))

	var vr sheets.ValueRange
	vr.Values = make([][]interface{}, len(mapContent))
	for i, row := range mapContent {
		vr.Values[i] = make([]interface{}, len(row))
		for j, cell := range row {
			vr.Values[i][j] = cell
		}
	}
	rangeData := fmt.Sprintf("%s!A1", sheetTitle)
	_, err := gspreadsheet.
		Service.
		Spreadsheets.
		Values.
		Update(
			gspreadsheet.SpreadsheetID,
			rangeData,
			&vr,
		).
		ValueInputOption("RAW").
		Context(context.Background()).
		Do()
	if err != nil {
		return err
	}
	display.Debug(fmt.Sprintf(dbgPHGdriveSpreadsheetUpdateEnd, pluginName, gspreadsheet.SpreadsheetID, sheetTitle))
	return nil
}
