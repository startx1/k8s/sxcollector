package plugin

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	// Importer les types de route OpenShift

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	// Importer le schéma du client Kubernetes
)

// Get the start date, stop date and step time from the generic arguments
func HelperGetPeriodParams(pluginName string, genericArgs map[string]string, precision int) (time.Time, time.Time, time.Duration, string, string, error) {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPHGetPeriodParams, pluginName))
	now := time.Now()
	startDate := now.AddDate(0, 0, -1)
	stopDate := now.AddDate(0, 0, 0)
	stepTime := time.Hour * 24
	stepPeriodFull := "1h"
	stepPeriodRythm := "1h"
	if periodStart, exists := genericArgs["from"]; exists {
		startDateNew, err := time.Parse(time.DateTime, periodStart)
		if err != nil {
			display.Error(fmt.Sprintf(errPHGetPeriodParamsParseStartNOK, periodStart, err))
			return startDate, stopDate, stepTime, stepPeriodFull, stepPeriodRythm, err
		} else {
			startDate = startDateNew
			display.Debug(fmt.Sprintf(dbgPHGetPeriodParamsParseStartOK, pluginName, startDate.String()))
		}
	} else {
		display.Warning(wrnPHGetPeriodParamsNoStart)
	}

	if periodStop, exists := genericArgs["to"]; exists {
		stopDate, err := time.Parse(time.DateOnly, periodStop)
		if err != nil {
			display.Error(fmt.Sprintf(errPHGetPeriodParamsParseStopNOK, periodStop, err))
			return startDate, stopDate, stepTime, stepPeriodFull, stepPeriodRythm, err
		} else {
			display.Debug(fmt.Sprintf(dbgPHGetPeriodParamsParseStopOK, pluginName, stopDate.String()))
		}
	}

	if startDate.After(stopDate) {
		display.Debug(fmt.Sprintf(dbgPHGetPeriodParamsParseStartAfterStop, pluginName, startDate.String(), stopDate.String()))
		stopDateTampon := stopDate
		stopDate = startDate
		startDate = stopDateTampon
		return startDate, stopDate, stepTime, stepPeriodFull, stepPeriodRythm, nil
	}

	if periodDuration, exists := genericArgs["duration"]; exists {
		switch {
		case strings.HasSuffix(periodDuration, "y"):
			display.Debug(fmt.Sprintf(dbgPHGetPeriodParamsParseDurationStart, pluginName, strings.TrimSuffix(periodDuration, "m"), "year"))
			years, err := strconv.Atoi(strings.TrimSuffix(periodDuration, "y"))
			if err == nil {
				stopDate = startDate.AddDate(years, 0, 0)
			} else {
				display.Error(fmt.Sprintf(errPHGetPeriodParamsParseDurationNOK, periodDuration, err))
				return startDate, stopDate, stepTime, stepPeriodFull, stepPeriodRythm, err
			}
		case strings.HasSuffix(periodDuration, "M"):
			display.Debug(fmt.Sprintf(dbgPHGetPeriodParamsParseDurationStart, pluginName, strings.TrimSuffix(periodDuration, "M"), "month"))
			month, err := strconv.Atoi(strings.TrimSuffix(periodDuration, "M"))
			if err == nil {
				stopDate = startDate.AddDate(0, month, 0)
			} else {
				display.Error(fmt.Sprintf(errPHGetPeriodParamsParseDurationNOK, periodDuration, err))
				return startDate, stopDate, stepTime, stepPeriodFull, stepPeriodRythm, err
			}
		case strings.HasSuffix(periodDuration, "w"):
			display.Debug(fmt.Sprintf(dbgPHGetPeriodParamsParseDurationStart, pluginName, strings.TrimSuffix(periodDuration, "w"), "week"))
			week, err := strconv.Atoi(strings.TrimSuffix(periodDuration, "w"))
			if err == nil {
				stopDate = startDate.AddDate(0, 0, week*7)
			} else {
				display.Error(fmt.Sprintf(errPHGetPeriodParamsParseDurationNOK, periodDuration, err))
				return startDate, stopDate, stepTime, stepPeriodFull, stepPeriodRythm, err
			}
		case strings.HasSuffix(periodDuration, "d"):
			display.Debug(fmt.Sprintf(dbgPHGetPeriodParamsParseDurationStart, pluginName, strings.TrimSuffix(periodDuration, "d"), "day"))
			days, err := strconv.Atoi(strings.TrimSuffix(periodDuration, "d"))
			if err == nil {
				stopDate = startDate.AddDate(0, 0, days)
			} else {
				display.Error(fmt.Sprintf(errPHGetPeriodParamsParseDurationNOK, periodDuration, err))
				return startDate, stopDate, stepTime, stepPeriodFull, stepPeriodRythm, err
			}
		case strings.HasSuffix(periodDuration, "m"):
			display.Debug(fmt.Sprintf(dbgPHGetPeriodParamsParseDurationStart, pluginName, strings.TrimSuffix(periodDuration, "m"), "minute"))
			minutes, err := strconv.Atoi(strings.TrimSuffix(periodDuration, "m"))
			if err == nil {
				stopDate = startDate.Add(time.Duration(minutes) * time.Minute)
			} else {
				display.Error(fmt.Sprintf(errPHGetPeriodParamsParseDurationNOK, periodDuration, err))
				return startDate, stopDate, stepTime, stepPeriodFull, stepPeriodRythm, err
			}
		}
		if strings.HasPrefix(genericArgs["duration"], "-") {
			stopDateTampon := stopDate
			stopDate = startDate
			startDate = stopDateTampon
		}
	}

	stepDuration, stepPeriodFull, stepPeriodRythm, errDiff := HelperGetPeriodDiff(pluginName, startDate, stopDate, precision)
	if errDiff != nil {
		return startDate, stopDate, stepTime, stepPeriodFull, stepPeriodRythm, errDiff
	}
	stepTime = stepDuration

	display.Debug(fmt.Sprintf(dbgPHGetPeriodParamsParseResultPeriodOK, pluginName, genericArgs["duration"]))
	display.Debug(fmt.Sprintf(dbgPHGetPeriodParamsParseResultStepOK, pluginName, stepTime.String()))
	display.Debug(fmt.Sprintf(dbgPHGetPeriodParamsParseResultStepFullOK, pluginName, stepPeriodFull))
	display.Debug(fmt.Sprintf(dbgPHGetPeriodParamsParseResultStepRythmOK, pluginName, stepPeriodRythm))
	display.Debug(fmt.Sprintf(dbgPHGetPeriodParamsParseResultStartOK, pluginName, startDate.String()))
	display.Debug(fmt.Sprintf(dbgPHGetPeriodParamsParseResultEndOK, pluginName, stopDate.String()))
	return startDate, stopDate, stepTime, stepPeriodFull, stepPeriodRythm, nil
}

// Get the start date, stop date and step time from the generic arguments
func HelperGetPeriodDiff(pluginName string, startDate time.Time, stopDate time.Time, precision int) (time.Duration, string, string, error) {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPHGetPeriodDiff, pluginName, startDate.String(), stopDate.String()))
	stepFactor := float64(precision)
	stepDuration := time.Hour
	stepPeriodRythm := ""
	stepPeriodFull := ""

	diff := stopDate.Sub(startDate)
	hoursDiff := diff.Hours()
	minDiff := diff.Minutes()

	stepDuration = time.Hour * time.Duration(float64(int(hoursDiff/stepFactor)))

	if hoursDiff <= 1 {
		stepPeriodRythm = strconv.Itoa(int(minDiff/stepFactor)) + "m"
		stepPeriodFull = strconv.Itoa(int(minDiff)) + "m"
		stepDuration = time.Duration(float64(int(minDiff/stepFactor) * int(time.Minute)))
	} else {
		stepPeriodRythm = strconv.Itoa(int(hoursDiff/stepFactor)) + "h"
		stepPeriodFull = strconv.Itoa(int(hoursDiff)) + "h"
	}

	return stepDuration, stepPeriodFull, stepPeriodRythm, nil
}
