package plugin

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"os"
	"strings"

	// Importer les types de route OpenShift

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	// Importer le schéma du client Kubernetes
)

// Return the output of the plugin in tabular format
func HelperGenerateTAB(pluginName string, headers []string, units []string, rows [][]string, hasHeader bool, hasSubHeader bool) (string, error) {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPHGenerateTAB, pluginName))

	maxColLengths := make([]int, 100)
	// Find maximum length for each column of the data
	if len(rows) > 0 {
		for _, row := range rows {
			for i, column := range row {
				if len(column) > maxColLengths[i] {
					maxColLengths[i] = len(column) + 1
				}
			}
		}
	} else {
		display.Warning(wrnPHGenerateTABNoData)
	}
	// If header is enabled, find maximum length for header column
	if hasHeader {
		if len(maxColLengths) == 0 {
			maxColLengths = make([]int, len(headers))
		}
		for i, row := range headers {
			if len(row) > maxColLengths[i] {
				maxColLengths[i] = len(row) + 1
			}
		}
	}
	// If sub-header is enabled, find maximum length for header column
	if hasSubHeader {
		if len(maxColLengths) == 0 {
			maxColLengths = make([]int, len(units))
		}
		for i, row := range units {
			if len(row) > maxColLengths[i] {
				maxColLengths[i] = len(row) + 1
			}
		}
	}

	// Prepare string buffer
	output := ""

	// If header is enabled, add the header line to the buffer
	if hasHeader {
		for i, header := range headers {
			output = output + fmt.Sprintf("%-*s ", maxColLengths[i], header)
		}
		output = output + "\n"
	}

	// If sub-header is enabled, add the unit line to the buffer
	if hasSubHeader {
		for i, unit := range units {
			output = output + fmt.Sprintf("%-*s ", maxColLengths[i], unit)
		}
		output = output + "\n"
	}

	// If header or sub-header is enabled, add the separation line to the buffer
	if hasHeader || hasSubHeader {
		for _, length := range maxColLengths {
			output = output + strings.Repeat("-", length) + " "
		}
		output = output + "\n"
	}

	// add content lines to the buffer
	for _, row := range rows {
		for i, column := range row {
			output = output + fmt.Sprintf("%-*s ", maxColLengths[i], column)
		}
		output = output + "\n"
	}

	return output, nil
}

// Return the output of the plugin in HTML format
func HelperGenerateHTML(pluginName string, header []string, units []string, rows [][]string, hasHeader bool, hasSubHeader bool) (string, error) {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPHGenerateHTML, pluginName))
	var sb strings.Builder
	sb.WriteString("<table border=\"1\">\n")
	if hasHeader {
		sb.WriteString("  <tr>\n")
		for _, hd := range header {
			sb.WriteString(fmt.Sprintf("    <th>%s</th>\n", hd))
		}
		sb.WriteString("  </tr>\n")
	}
	if hasSubHeader {
		sb.WriteString("  <tr>\n")
		for _, unit := range units {
			sb.WriteString(fmt.Sprintf("    <th>%s</th>\n", unit))
		}
		sb.WriteString("  </tr>\n")
	}
	for _, line := range rows {
		sb.WriteString("  <tr>\n")
		for _, cell := range line {
			sb.WriteString(fmt.Sprintf("    <td>%s</td>\n", cell))
		}
		sb.WriteString("  </tr>\n")
	}
	sb.WriteString("</table>\n")
	return sb.String(), nil
}

// Return the output of the plugin in CSV format
func HelperGenerateCSV(pluginName string, header []string, units []string, rows [][]string, sep string, hasHeader bool, hasSubHeader bool) (string, error) {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPHGenerateCSV, pluginName))

	var buffer bytes.Buffer
	writer := csv.NewWriter(&buffer)
	writer.Comma = rune(sep[0])
	if hasHeader {
		if err := writer.Write(header); err != nil {
			display.Error(fmt.Sprintf(errPluginRecordResultWriteHeader, err))
			return "", err
		}
	}
	if hasSubHeader {
		if err := writer.Write(units); err != nil {
			display.Error(fmt.Sprintf(errPluginRecordResultWriteUnits, err))
			return "", err
		}
	}
	for _, line := range rows {
		if err := writer.Write(line); err != nil {
			display.Error(fmt.Sprintf(errPluginRecordResultWriteLine, err))
			return "", err
		}
	}
	writer.Flush()
	if err := writer.Error(); err != nil {
		display.Error(fmt.Sprintf(errPluginRecordResultWriteFlush, err))
		return "", err
	}
	return buffer.String(), nil
}

// Return the output of the plugin in [][]string
func HelperGenerateMap(pluginName string, headers []string, units []string, rows [][]string, hasHeader bool, hasSubHeader bool) ([][]string, error) {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPHGenerateMAP, pluginName))

	maxColLengths := make([]int, 100)
	if len(rows) > 0 {
		for _, row := range rows {
			for i, column := range row {
				if len(column) > maxColLengths[i] {
					maxColLengths[i] = len(column) + 1
				}
			}
		}
	} else {
		display.Warning(wrnPHGenerateMAPNoData)
	}
	if hasHeader {
		if len(maxColLengths) == 0 {
			maxColLengths = make([]int, len(headers))
		}
		for i, row := range headers {
			if len(row) > maxColLengths[i] {
				maxColLengths[i] = len(row) + 1
			}
		}
	}
	if hasSubHeader {
		if len(maxColLengths) == 0 {
			maxColLengths = make([]int, len(units))
		}
		for i, row := range units {
			if len(row) > maxColLengths[i] {
				maxColLengths[i] = len(row) + 1
			}
		}
	}
	output := [][]string{}

	if hasHeader {
		output = append(output, headers)
	}
	if hasSubHeader {
		output = append(output, units)
	}
	output = append(output, rows...)
	return output, nil
}

// write content to a file
func HelperWriteFile(pluginName string, filename string, data string) error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPHWriteFile, pluginName, filename))
	// Open the file in append mode, create it if it doesn't exist
	file, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		display.Error(fmt.Sprintf(errPHWriteFileOpenNOK, filename, err))
		return err
	}
	defer file.Close()
	// Write the data to the file with a new line
	_, err = file.WriteString(data)
	if err != nil {
		display.Error(fmt.Sprintf(errPHWriteFileWriteNOK, filename, err))
		return err
	}
	return nil
}
