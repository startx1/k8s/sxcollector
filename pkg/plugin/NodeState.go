package plugin

import (
	"context"
	"errors"
	"fmt"
	"log"
	"sort"
	"strconv"
	"strings"
	"time"

	sxConfluence "gitlab.com/startx1/k8s/go-libs/pkg/confluence"
	sxGDrive "gitlab.com/startx1/k8s/go-libs/pkg/gdrive"
	sxKCli "gitlab.com/startx1/k8s/go-libs/pkg/k8sclient"
	sxProm "gitlab.com/startx1/k8s/go-libs/pkg/prometheus"
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	sxPluginCore "gitlab.com/startx1/k8s/sxcollector/pkg/plugincore"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	corev1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// Struct of the NodeState plugin (implement PluginInterface)
type NodeState struct {
	kc            *sxKCli.K8sClient
	needK8sClient bool
	needOCP       bool
	Name          string
	Subcommand    string
	HelpMsg       string
	IsLoaded      bool
	Rows          [][]string
	Result        string
	header        []string
	units         []string
}

// Get the Name of the plugin
func (c *NodeState) GetName() string {
	return c.Name
}

// Get the Subcommand of the plugin
func (c *NodeState) GetSubcommand() string {
	return c.Subcommand
}

// Get the NeedK8sClient of the plugin
func (c *NodeState) NeedK8sClient() bool {
	return c.needK8sClient
}

// Get the NeedOCP of the plugin
func (c *NodeState) NeedOCP() bool {
	return c.needOCP
}

// Get the Help Message
func (c *NodeState) GetHelp() string {
	return c.HelpMsg
}

// Return the output of the plugin
func (c *NodeState) Exec(subcommand string, pluginArgs map[string]string) error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPluginExec, c.Name, subcommand))
	err := c.GenerateRaw(subcommand, pluginArgs)
	if err != nil {
		display.Error(fmt.Sprintf(errPluginGenerateRaw, err))
		return err
	}
	switch {
	case pluginArgs["output-type"] == "mail":
		err = c.RecordResultMail(pluginArgs)
	case pluginArgs["output-type"] == "confluence":
		err = c.RecordResultConfluence(pluginArgs)
	case pluginArgs["output-type"] == "gsheet":
		err = c.RecordResultGSheet(pluginArgs)
	case pluginArgs["output-format"] == "csv":
		err = c.RecordResultCSV(
			pluginArgs["output-csv-separator"],
			pluginArgs["output-noheaders"] != "true",
			pluginArgs["output-nounits"] != "true",
		)
	default:
		err = c.RecordResultTAB(
			pluginArgs["output-noheaders"] != "true",
			pluginArgs["output-nounits"] != "true",
		)
	}
	if err != nil {
		display.Error(fmt.Sprintf(errPluginExecNOK, err))
		return err
	}
	return nil
}

// Return the output of the plugin
func (c *NodeState) GenerateRaw(subcommand string, pluginArgs map[string]string) error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPluginGenerateRaw, c.Name))

	// Get Openshift Prometheus connection detail
	prom := sxProm.NewPrometheus(c.kc)
	errPromConnect := prom.SetPrometheusOCP()
	if errPromConnect != nil {
		display.ExitError(errPromConnect.Error(), 20)
	}
	resultMetrics, err := c.GenerateRawPrometheusData(prom, pluginArgs)
	if err != nil {
		return err
	}
	rowsMetrics := [][]string{}
	for nsName, values := range resultMetrics {
		consoCpuCores := fmt.Sprintf("%.2f", values[0])
		consoMemoryGB := fmt.Sprintf("%.2f", values[1])
		consoEphemeralStorage := fmt.Sprintf("%.2f", values[2])
		consoPods := fmt.Sprintf("%v", values[3])
		consoContainers := fmt.Sprintf("%v", values[4])

		// Ecrire la ligne dans la sortie Raw
		row := []string{
			nsName,
			consoCpuCores,
			consoMemoryGB,
			consoEphemeralStorage,
			consoPods,
			consoContainers,
		}
		rowsMetrics = append(rowsMetrics, row)
	}

	// get kubernetes stuff
	nodes, err := c.kc.Clientset.CoreV1().Nodes().List(context.TODO(), corev1.ListOptions{})
	if err != nil {
		display.Error(fmt.Sprintf(errPluginGenerateRawGetK8S, err))
		return err
	}

	c.Rows = [][]string{}
	for _, node := range nodes.Items {
		nodeAge := fmt.Sprintf("%d", int(time.Since(node.CreationTimestamp.Time).Hours()))
		nodeStatus := "NotReady"
		for _, condition := range node.Status.Conditions {
			if condition.Type == v1.NodeReady && condition.Status == v1.ConditionTrue {
				nodeStatus = "Ready"
				break
			}
		}
		// Récupérer le rôle du node (d'après les labels)
		role := "none"
		instanceType := ""
		FDRegion := ""
		FDZone := ""
		allocCpuCores := fmt.Sprintf("%.2f", float64(node.Status.Allocatable.Cpu().MilliValue())/1000)
		allocMemoryGB := fmt.Sprintf("%.2f", float64(node.Status.Allocatable.Memory().Value())/(1024*1024*1024))
		allocEphemeralStorage := fmt.Sprintf("%.2f", float64(node.Status.Allocatable.StorageEphemeral().Value())/(1024*1024*1024))
		allocPods := node.Status.Allocatable.Pods().String()

		for key := range node.Labels {
			if strings.HasPrefix(key, "node-role.kubernetes.io/") {
				role = strings.TrimPrefix(key, "node-role.kubernetes.io/")
			}
			if key == "kubernetes.io/role" {
				role = node.Labels[key]
			}
			if key == "beta.kubernetes.io/instance-type" || key == "node.kubernetes.io/instance-type" {
				instanceType = node.Labels[key]
			}
			if key == "failure-domain.beta.kubernetes.io/region" || key == "topology.kubernetes.io/region" {
				FDRegion = node.Labels[key]
			}
			if key == "failure-domain.beta.kubernetes.io/zone" || key == "topology.kubernetes.io/zone" {
				FDZone = node.Labels[key]
			}
		}
		// recuperation des requests et limits
		pods, err := c.kc.Clientset.CoreV1().Pods(corev1.NamespaceAll).List(context.TODO(), corev1.ListOptions{
			FieldSelector: "spec.nodeName=" + node.Name,
		})
		if err != nil {
			display.Error(fmt.Sprintf(errPluginGenerateRawGetPodsInfo, node.Name, err))
			continue
		}
		usedCpu := ""
		usedCpuFloat := float64(0)
		for _, line := range rowsMetrics {
			if line[0] == node.Name {
				usedCpuFloat, _ = strconv.ParseFloat(line[1], 64)
				usedCpu = fmt.Sprintf("%.2f", usedCpuFloat)
			}
		}
		usedMem := ""
		usedMemFloat := float64(0)
		for _, line := range rowsMetrics {
			if line[0] == node.Name {
				usedMemFloat, _ = strconv.ParseFloat(line[2], 64)
				usedMem = fmt.Sprintf("%.2f", usedMemFloat)
			}
		}
		usedES := "0.00"
		usedESFloat := float64(0)
		for _, line := range rowsMetrics {
			if line[0] == node.Name {

				usedESFloat, _ = strconv.ParseFloat(line[3], 64)
				usedES = fmt.Sprintf("%.2f", usedESFloat/(1024*1024*1024))
			}
		}
		totalPodsUsed := "0"
		usedPodFloat := float64(0)
		for _, line := range rowsMetrics {
			if line[0] == node.Name {

				usedPodFloat, _ = strconv.ParseFloat(line[4], 64)
				totalPodsUsed = fmt.Sprintf("%.0f", usedPodFloat)
			}
		}
		totalContainersUsed := "0"
		usedContainerFloat := float64(0)
		for _, line := range rowsMetrics {
			if line[0] == node.Name {

				usedContainerFloat, _ = strconv.ParseFloat(line[5], 64)
				totalContainersUsed = fmt.Sprintf("%.0f", usedContainerFloat)
			}
		}

		// Variables pour accumuler les totaux de requests et limits
		var totalMemRequests, totalMemLimits, totalCpuRequests, totalCpuLimits, totalESRequests, totalESLimits resource.Quantity
		var totalPods, totalContainers int64
		for _, pod := range pods.Items {
			totalPods = totalPods + 1
			for _, container := range pod.Spec.Containers {
				totalContainers = totalContainers + 1
				memRequests := container.Resources.Requests.Memory()
				memLimits := container.Resources.Limits.Memory()
				if memRequests != nil {
					totalMemRequests.Add(*memRequests)
				}
				if memLimits != nil {
					totalMemLimits.Add(*memLimits)
				}
				cpuRequests := container.Resources.Requests.Cpu()
				cpuLimits := container.Resources.Limits.Cpu()
				if cpuRequests != nil {
					totalCpuRequests.Add(*cpuRequests)
				}
				if cpuLimits != nil {
					totalCpuLimits.Add(*cpuLimits)
				}
				ephemeralStorageRequests := container.Resources.Requests.StorageEphemeral()
				ephemeralStorageLimits := container.Resources.Limits.StorageEphemeral()

				// Ajouter les requests et limits de storage éphémère en octets
				if ephemeralStorageRequests != nil {
					totalESRequests.Add(*ephemeralStorageRequests)
				}
				if ephemeralStorageLimits != nil {
					totalESLimits.Add(*ephemeralStorageLimits)
				}

			}
		}
		totalPodsDisplay := fmt.Sprintf("%.0f", float64(totalPods))
		totalContainersDisplay := fmt.Sprintf("%.0f", float64(totalContainers))
		requestMemoryGB := fmt.Sprintf("%.2f", float64(totalMemRequests.Value())/(1024*1024*1024))
		limitsMemoryGB := fmt.Sprintf("%.2f", float64(totalMemLimits.Value())/(1024*1024*1024))
		requestCpu := fmt.Sprintf("%.2f", float64(totalCpuRequests.MilliValue()/1000))
		limitsCpu := fmt.Sprintf("%.2f", float64(totalCpuLimits.MilliValue())/1000)
		requestES := fmt.Sprintf("%.2f", float64(totalESRequests.Value()/(1024*1024*1024)))
		limitsES := fmt.Sprintf("%.2f", float64(totalESLimits.Value())/(1024*1024*1024))

		// Ecrire la ligne dans la sortie Raw
		row := []string{
			node.Name,
			role,
			nodeStatus,
			nodeAge,
			FDRegion,
			FDZone,
			instanceType,
			allocPods,
			totalPodsDisplay,
			totalPodsUsed,
			totalContainersDisplay,
			totalContainersUsed,
			allocMemoryGB,
			requestMemoryGB,
			limitsMemoryGB,
			usedMem,
			allocCpuCores,
			requestCpu,
			limitsCpu,
			usedCpu,
			allocEphemeralStorage,
			requestES,
			limitsES,
			usedES,
		}
		c.Rows = append(c.Rows, row)
	}

	// Sort by Namespace after processing
	sort.Slice(c.Rows, func(i, j int) bool {
		return c.Rows[i][0] < c.Rows[j][0]
	})

	return nil
}

// Send the content to mail
func (c *NodeState) RecordResultMail(pluginArgs map[string]string) error {
	errGen := c.RecordResultCSV(
		pluginArgs["output-csv-separator"],
		pluginArgs["output-noheaders"] != "true",
		pluginArgs["output-nounits"] != "true",
	)
	if errGen != nil {
		return errGen
	}
	formattedTime := time.Now().Format(valPluginRecordResultMailDateFormat)
	if pluginArgs["mail-title"] == "" {
		pluginArgs["mail-title"] = fmt.Sprintf(valPluginRecordResultMailTitle, c.Name, formattedTime)
	} else {
		pluginArgs["mail-title"] = fmt.Sprintf(pluginArgs["mail-title"], c.Name, formattedTime)
	}
	title := pluginArgs["mail-title"]
	message := fmt.Sprintf(
		valPluginRecordResultMailContent,
		c.Name,
		formattedTime,
	)
	filename := fmt.Sprintf("%s.csv", c.Name)

	mail := HelperPrepareMail(
		c.Name,
		pluginArgs["mail-from"],
		pluginArgs["mail-to"],
		title,
		message,
		filename,
		[]byte(c.Result),
	)

	_, errMailer := HelperSendMail(
		c.Name,
		pluginArgs["smtp-host"],
		pluginArgs["smtp-port"],
		pluginArgs["smtp-username"],
		pluginArgs["smtp-password"],
		pluginArgs["smtp-insecure"] == "true",
		mail,
	)
	if errMailer != nil {
		return errMailer
	}
	return nil
}

// Record a confluence page
func (c *NodeState) RecordResultConfluence(pluginArgs map[string]string) error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPluginRecordResultConfluenceBegin, c.Name))
	errGen := c.RecordResultHTML(
		pluginArgs["output-noheaders"] != "true",
		pluginArgs["output-nounits"] != "true",
	)
	if errGen != nil {
		return errGen
	}

	formattedTime := time.Now().Format(valPluginRecordResultConfluenceDateFormat)
	if pluginArgs["confluence-title"] == "" {
		pluginArgs["confluence-title"] = fmt.Sprintf(valPluginRecordResultConfluenceTitle, c.Name, formattedTime)
	}
	if pluginArgs["confluence-content"] == "" {
		pluginArgs["confluence-content"] = fmt.Sprintf(
			valPluginRecordResultConfluenceContent,
			c.Name,
			formattedTime,
		)
	}

	confluence := sxConfluence.NewConfluence(
		pluginArgs["confluence-url"],
		pluginArgs["confluence-username"],
		pluginArgs["confluence-apitoken"],
		pluginArgs["confluence-spacekey"],
	)
	confluencePage := sxConfluence.NewConfluencePage(
		pluginArgs["confluence-spacekey"],
		pluginArgs["confluence-pageid"],
		pluginArgs["confluence-parentid"],
		pluginArgs["confluence-title"],
		pluginArgs["confluence-content"]+c.Result,
	)
	switch pluginArgs["confluence-mode"] {
	case "append":
		_, errAppend := confluence.CreateOrAppendPage(confluencePage, "<br/>")
		if errAppend != nil {
			return fmt.Errorf(errPluginRecordResultConfluenceAppend, errAppend)
		} else {
			display.Info(
				fmt.Sprintf(
					dbgPluginRecordResultConfluenceAppend,
					pluginArgs["confluence-pageid"],
					pluginArgs["confluence-spacekey"],
				),
			)
		}
	case "update":
		_, errUpdate := confluence.UpdatePage(confluencePage)
		if errUpdate != nil {
			return fmt.Errorf(errPluginRecordResultConfluenceUpdate, errUpdate)
		} else {
			display.Info(
				fmt.Sprintf(
					dbgPluginRecordResultConfluenceUpdate,
					pluginArgs["confluence-pageid"],
					pluginArgs["confluence-spacekey"],
				),
			)
		}
	case "create":
		_, errCreate := confluence.CreatePage(confluencePage)
		if errCreate != nil {
			return fmt.Errorf(errPluginRecordResultConfluenceCreate, errCreate)
		} else {
			display.Info(
				fmt.Sprintf(
					dbgPluginRecordResultConfluenceCreate,
					pluginArgs["confluence-title"],
					pluginArgs["confluence-spacekey"],
					pluginArgs["confluence-parentid"],
				),
			)
		}
	default:
		_, errCreate := confluence.CreatePage(confluencePage)
		if errCreate != nil {
			return fmt.Errorf(errPluginRecordResultConfluenceCreate, errCreate)
		} else {
			display.Info(
				fmt.Sprintf(
					dbgPluginRecordResultConfluenceCreate,
					pluginArgs["confluence-title"],
					pluginArgs["confluence-spacekey"],
					pluginArgs["confluence-parentid"],
				),
			)
		}
	}

	switch {
	case pluginArgs["output-format"] == "csv":
		errGen = c.RecordResultCSV(
			pluginArgs["output-csv-separator"],
			pluginArgs["output-noheaders"] != "true",
			pluginArgs["output-nounits"] != "true",
		)
	default:
		errGen = c.RecordResultTAB(
			pluginArgs["output-noheaders"] != "true",
			pluginArgs["output-nounits"] != "true",
		)
	}
	if errGen != nil {
		return errGen
	}
	display.Debug(fmt.Sprintf(dbgPluginRecordResultConfluenceEnd, c.Name))
	return nil
}

// Record a google sheet
func (c *NodeState) RecordResultGSheet(pluginArgs map[string]string) error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPluginRecordResultGSheetBegin, c.Name))
	mapContent, errGen := HelperGenerateMap(
		c.Name,
		c.header,
		c.units,
		c.Rows,
		pluginArgs["output-noheaders"] != "true",
		pluginArgs["output-nounits"] != "true",
	)
	if errGen != nil {
		return errGen
	}
	formattedTime := time.Now().Format(valPluginRecordResultGSheetDateFormat)
	if pluginArgs["gsheet-sheet-title"] == "" {
		pluginArgs["gsheet-sheet-title"] = fmt.Sprintf(valPluginRecordResultGSheetSheetTitle, c.Name, formattedTime)
	}
	if pluginArgs["gsheet-spreadsheet-title"] == "" {
		pluginArgs["gsheet-spreadsheet-title"] = valPluginRecordResultGSheetSpreadsheetTitle
	}
	if pluginArgs["gdrive-creds-filename"] == "" {
		pluginArgs["gdrive-creds-filename"] = valPluginRecordResultGDriveCredsFilename
	}
	if pluginArgs["gdrive-creds-path"] == "" {
		pluginArgs["gdrive-creds-path"] = valPluginRecordResultGDriveCredsPath
	}
	if pluginArgs["gdrive-creds-domain"] == "" {
		pluginArgs["gdrive-creds-domain"] = valPluginRecordResultGDriveCredsDomain
	}

	gdrive := sxGDrive.NewGDrive(
		pluginArgs["gdrive-creds-filename"],
		pluginArgs["gdrive-creds-path"],
		pluginArgs["gdrive-creds-domain"],
	)
	switch pluginArgs["gsheet-mode"] {
	case "append":
		gspreadsheet := sxGDrive.NewGDriveSpreadsheet(gdrive, pluginArgs["gsheet-spreadsheet-id"])
		err := gspreadsheet.InitSpreadsheet()
		if err != nil {
			return fmt.Errorf(errPluginRecordResultGSheetAppend, err)
		}
		err = gspreadsheet.LoadSpreadsheet()
		if err != nil {
			return fmt.Errorf(errPluginRecordResultGSheetAppend, err)
		} else {
			_, err := gspreadsheet.UpsertSheet(pluginArgs["gsheet-sheet-title"])
			if err != nil {
				log.Fatalf("Unable to get sheet: %v", err)
			}
			err = HelperGdriveSpreadsheetUpdate(c.Name, gspreadsheet, pluginArgs["gsheet-sheet-title"], mapContent)
			if err != nil {
				return fmt.Errorf(errPluginRecordResultGSheetAppend, err)
			} else {
				display.Info(
					fmt.Sprintf(
						dbgPluginRecordResultGSheetAppend,
						gspreadsheet.SpreadsheetID,
						pluginArgs["gsheet-sheet-title"],
					),
				)
			}
		}
	case "update":
		gspreadsheet := sxGDrive.NewGDriveSpreadsheet(gdrive, pluginArgs["gsheet-spreadsheet-id"])
		err := gspreadsheet.InitSpreadsheet()
		if err != nil {
			return fmt.Errorf(errPluginRecordResultGSheetUpdate, err)
		}
		err = gspreadsheet.LoadSpreadsheet()
		if err != nil {
			return fmt.Errorf(errPluginRecordResultGSheetUpdate, err)
		} else {
			_, err := gspreadsheet.UpsertSheet(pluginArgs["gsheet-sheet-title"])
			if err != nil {
				log.Fatalf("Unable to get sheet: %v", err)
			}
			err = HelperGdriveSpreadsheetUpdate(c.Name, gspreadsheet, pluginArgs["gsheet-sheet-title"], mapContent)
			if err != nil {
				return fmt.Errorf(errPluginRecordResultGSheetUpdate, err)
			} else {
				display.Info(
					fmt.Sprintf(
						dbgPluginRecordResultGSheetUpdate,
						gspreadsheet.SpreadsheetID,
						pluginArgs["gsheet-sheet-title"],
					),
				)
			}
		}
	case "create":
		gspreadsheet := sxGDrive.NewGDriveSpreadsheet(gdrive, "")
		err := gspreadsheet.InitSpreadsheet()
		if err != nil {
			return fmt.Errorf(errPluginRecordResultGSheetCreate, err)
		}
		err = gspreadsheet.AddSpreadsheet(pluginArgs["gsheet-spreadsheet-title"], true)
		if err != nil {
			return fmt.Errorf(errPluginRecordResultGSheetCreate, err)
		} else {
			_, err := gspreadsheet.UpsertSheet(pluginArgs["gsheet-sheet-title"])
			if err != nil {
				log.Fatalf("Unable to get sheet: %v", err)
			}
			err = HelperGdriveSpreadsheetUpdate(c.Name, gspreadsheet, pluginArgs["gsheet-sheet-title"], mapContent)
			if err != nil {
				return fmt.Errorf(errPluginRecordResultGSheetCreate, err)
			} else {
				display.Info(
					fmt.Sprintf(
						dbgPluginRecordResultGSheetCreate,
						gspreadsheet.SpreadsheetID,
						pluginArgs["gsheet-sheet-title"],
					),
				)
			}
		}
	default:
		gspreadsheet := sxGDrive.NewGDriveSpreadsheet(gdrive, "")
		err := gspreadsheet.InitSpreadsheet()
		if err != nil {
			return fmt.Errorf(errPluginRecordResultGSheetCreate, err)
		}
		err = gspreadsheet.AddSpreadsheet(pluginArgs["gsheet-spreadsheet-title"], true)
		if err != nil {
			return fmt.Errorf(errPluginRecordResultGSheetCreate, err)
		} else {
			_, err := gspreadsheet.UpsertSheet(pluginArgs["gsheet-sheet-title"])
			if err != nil {
				log.Fatalf("Unable to get sheet: %v", err)
			}
			err = HelperGdriveSpreadsheetUpdate(c.Name, gspreadsheet, pluginArgs["gsheet-sheet-title"], mapContent)
			if err != nil {
				return fmt.Errorf(errPluginRecordResultGSheetCreate, err)
			} else {
				display.Info(
					fmt.Sprintf(
						dbgPluginRecordResultGSheetCreate,
						gspreadsheet.SpreadsheetID,
						pluginArgs["gsheet-sheet-title"],
					),
				)
			}
		}
	}

	switch {
	case pluginArgs["output-format"] == "csv":
		errGen = c.RecordResultCSV(
			pluginArgs["output-csv-separator"],
			pluginArgs["output-noheaders"] != "true",
			pluginArgs["output-nounits"] != "true",
		)
	default:
		errGen = c.RecordResultTAB(
			pluginArgs["output-noheaders"] != "true",
			pluginArgs["output-nounits"] != "true",
		)
	}
	if errGen != nil {
		return errGen
	}
	display.Debug(fmt.Sprintf(dbgPluginRecordResultGSheetEnd, c.Name))
	return nil
}

// Record the HTML version of the result
func (c *NodeState) RecordResultHTML(hasHeader bool, hasSubHeader bool) error {
	htmlString, err := HelperGenerateHTML(
		c.Name,
		c.header,
		c.units,
		c.Rows,
		hasHeader,
		hasSubHeader,
	)
	if err != nil {
		return err
	}
	c.Result = htmlString
	return nil
}

// Record the CSV version of the result
func (c *NodeState) RecordResultCSV(sep string, hasHeader bool, hasSubHeader bool) error {
	csvString, err := HelperGenerateCSV(
		c.Name,
		c.header,
		c.units,
		c.Rows,
		sep,
		hasHeader,
		hasSubHeader,
	)
	if err != nil {
		return err
	}
	c.Result = csvString
	return nil
}

// Record the TAB version of the result
func (c *NodeState) RecordResultTAB(hasHeader bool, hasSubHeader bool) error {
	tabString, err := HelperGenerateTAB(
		c.Name,
		c.header,
		c.units,
		c.Rows,
		hasHeader,
		hasSubHeader,
	)
	if err != nil {
		return err
	}
	c.Result = tabString
	return nil
}

// Return the output of the plugin result
func (c *NodeState) Read() string {
	return c.Result
}

// Output the result
func (c *NodeState) Output(subcommand string, pluginArgs map[string]string) error {
	if pluginArgs["output-type"] == "file" {
		err := HelperWriteFile(c.Name, pluginArgs["output-file"], c.Read())
		if err != nil {
			return err
		}
		return nil
	} else if pluginArgs["output-type"] == "mail" {
		return nil
	} else {
		fmt.Println(c.Read())
	}
	return nil
}

// Load the plugin
func (c *NodeState) Load(kc *sxKCli.K8sClient) error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	c.kc = kc
	if c.kc.IsConnected() {
		c.IsLoaded = true
		display.Debug(fmt.Sprintf(dbgPluginLoadConnected, c.Name))
		return nil
	} else {
		c.IsLoaded = false
		msg := fmt.Sprintf(dbgPluginLoadNotConnected, c.Name)
		display.Debug(msg)
		return errors.New(msg)
	}
}

// Load the plugin without K8S
func (c *NodeState) LoadNoK8s() error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPluginLoaded, c.Name))
	c.IsLoaded = true
	return nil
}

// Init the plugin
func (c *NodeState) Init() error {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPluginInit, c.Name))
	return nil
}

// Register NodeState plugin in the PluginRegistry
func init() {
	helpMessage := `
-- NODE-STATE PLUGIN ---
-- Require : Openshift

node-state subcommand allow you to extract your openshift nodes state into a csv file. 

Usage: sxcollector node-state [OPTIONS]...

Generic options:
  --insecure                    Skip TLS verification for prometheus communication (default false)

Output options:
  --output-format FORMAT        Set the formating of the generated content. Could be tab, json or csv (default is tab)
  --output-csv-separator SEP    Define the separator sign to use for the csv format (default is ,)
  --output-noheaders            Disable headers in the result (default is false)
  --output-nounits              Disable unit header in the result (default is false)
  --output-file FILENAME        Define the output file. If not set, stdout will be used (default is stdout)

Prometheus options:
  --insecure                    Skip TLS verification for prometheus communication (default false)
  --from DATE                   Define the starting date. ex: 2024-01-01 00:00:00 (default is now)
  --to DATE                     Define the ending date. ex: 2024-01-03. Have precedence over duration if both are set ( no default)
  --duration DURATION           Define the period duration. ex: 1y. Could be overwrite by --to (default is 1m)
  --precision PRECISION         A factor that divide the duration. Used for slicing prometheus data (default is 1 = all)

Mail options:
  --mail-to EMAIL               Define the destination mail. If set, output will be of type mail. SMTP params must be set
  --mail-from EMAIL             Define the source mail. If set, output will be of type mail. If not use MAIL_FROM env
  --mail-subject SUBJECT        Customize the mail subject. Default is "Report SXCollector %s at %s" where first arg is name of the plugin and second the date of execution
  --smtp-host HOST              Define the SMTP host. If output type is mail and host is not set, use the SMTP_HOST env var
  --smtp-port PORT              Define the SMTP port. If output type is mail and port is not set, use the SMTP_PORT env var
  --smtp-username USERNAME      Define the SMTP username. If output type is mail and username is not set, use the SMTP_USERNAME env var
  --smtp-password PASSWORD      Define the SMTP password. If output type is mail and password is not set, use the SMTP_PASSWORD env var
  --smtp-insecure               Define if SMTP connection is insecure. If output type is mail and --smtp-insecure is not set, use the SMTP_INSECURE=true env var

Confluence options:
  --confluence-url URL            The confluence base URL ex: https://example.atlassian.net/wiki. If --confluence-url is not set, use the CONFLUENCE_BASE_URL env var
  --confluence-username USERNAME  The confluence username ex: user@example.com. If --confluence-username is not set, use the CONFLUENCE_USERNAME env var
  --confluence-apitoken APITOKEN  The confluence API Token, see https://id.atlassian.com/manage-profile/security/api-tokens. If --confluence-apitoken is not set, use the CONFLUENCE_API_TOKEN env var
  --confluence-spacekey SPACEKEY  The confluence Space key. If --confluence-spacekey is not set, use the CONFLUENCE_SPACE_KEY env var
  --confluence-title TITLE        The confluence page title (or section if --confluence-mode=append). If --confluence-title is not set, use the CONFLUENCE_PAGE_TITLE env var
  --confluence-mode               Define the default confluence mode (could be create, update, append). Default is create
  --confluence-content CONTENT    The confluence page content (prepended to the generated content). If --confluence-content is not set, use the CONFLUENCE_PAGE_HTML_CONTENT env var 
  --confluence-pageid PAGEID      The confluence pageID (required except for --confluence-mode=create). If --confluence-pageid is not set, use the CONFLUENCE_PAGE_ID env var
  --confluence-parentid PARENTID  The confluence parentID (useful with --confluence-mode=create). If --confluence-parentid is not set, use the CONFLUENCE_PARENT_ID env var
Generic options:
  --debug                       Activates debug mode for detailed troubleshooting information.
  --help                        Displays this help message and exits.
  --kubeconfig FILEPATH         Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided)

Environment variables :

  SMTP_HOST                     Define the SMTP host in replacement of the --smtp-host argument
  SMTP_PORT                     Define the SMTP port in replacement of the --smtp-port argument
  SMTP_USERNAME                 Define the SMTP username in replacement of the --smtp-username argument
  SMTP_PASSWORD                 Define the SMTP password in replacement of the --smtp-password argument
  MAIL_FROM                     Define the source mail in replacement of the --mail-from argument
  MAIL_SUBJECT                  Define the source mail in replacement of the --mail-subject argument
  CONFLUENCE_BASE_URL           Define the confluence base URL in replacement of the --confluence-url argument
  CONFLUENCE_USERNAME           Define the confluence username in replacement of the --confluence-username argument
  CONFLUENCE_API_TOKEN          Define the confluence API Token in replacement of the --confluence-apitoken argument
  CONFLUENCE_SPACE_KEY          Define the confluence Space key in replacement of the --confluence-spacekey argument
  CONFLUENCE_PAGE_TITLE         Define the page title in replacement of the --confluence-title argument
  CONFLUENCE_PAGE_HTML_CONTENT  Define the page content in replacement of the --confluence-content argument
  CONFLUENCE_PAGE_ID            Define the confluence pageID in replacement of the --confluence-pageid argument
  CONFLUENCE_PARENT_ID          Define the confluence parentID in replacement of the --confluence-parentid argument
  SXCOLLECTOR_FORMAT            Define the output format in replacement of the --output-format argument
  SXCOLLECTOR_TO                Define the destination mail in replacement of the --mail-to argument
  SXCOLLECTOR_DURATION          Define the prometheus duration in replacement of the --duration argument
  SXCOLLECTOR_PRECISION         Define the prometheus precision in replacement of the --precision argument
  SXCOLLECTOR_INSECURE          Enable the insecure connection for prometheus in replacement of the --insecure argument
  SXCOLLECTOR_NOHEADERS         Disable the headers in replacement of the --output-noheaders argument
  SXCOLLECTOR_NOUNITS           Disable the units in replacement of the --output-nounits argument
  SXCOLLECTOR_SEP               Define the separator character in replacement of the --output-csv-separator argument

Examples:
  Export node state into a csv, comma-separated output :
  $ sxcollector node-state
  Export node state into a csv, comma-separated output into a file :
  $ sxcollector node-state --output /tmp/sxcollector.csv
  Export node state with separator '|' into /tmp/sxcollector.csv :
  $ sxcollector node-state --sep "|" --output /tmp/sxcollector.csv

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	header := []string{
		"Name",
		"Role",
		"Status",
		"Age",
		"Region",
		"Zone",
		"InstanceType",
		"AllocatablePods",
		"DeclaredPods",
		"RunningPods",
		"DeclaredContainers",
		"RunningContainers",
		"AllocatableMemoryGB",
		"RequestMemoryGB",
		"LimitsMemoryGB",
		"UsedMemoryGB",
		"AllocatableCpu",
		"RequestCpu",
		"LimitsCpu",
		"usedCPU",
		"AllocatableEphemeralStorage",
		"RequestEphemeralStorage",
		"LimitsEphemeralStorage",
		"usedEphemeralStorage",
	}
	units := []string{
		"Node name",
		"Role of the node",
		"State of the node",
		"since creation (in hours)",
		"Failure domain region",
		"Failure domain zone",
		"Type of hardware instance",
		"Nbr of pods allocatable",
		"Nbr of pods declared",
		"Nbr of pods running",
		"Nbr of containers declared",
		"Nbr of containers running",
		"Memory allocable (in GB)",
		"Memory requested (in GB)",
		"Memory limits (in GB)",
		"Memory used (in GB)",
		"CPU allocatable (in milicore)",
		"CPU requested (in milicore)",
		"CPU limits (in milicore)",
		"CPU used (in milicore)",
		"ephemeral storage allocatable (in GB)",
		"ephemeral storage requested (in GB)",
		"ephemeral storage limits (in GB)",
		"ephemeral storage used (in GB)",
	}

	sxPluginCore.RegisterPlugin("NodeState", func() sxPluginCore.PluginInterface {
		return &NodeState{
			Name:          "NodeState",
			Subcommand:    "node-state",
			HelpMsg:       helpMessage,
			needK8sClient: true,
			needOCP:       true,
			header:        header,
			units:         units,
		}
	})
}

func (c *NodeState) GenerateRawPrometheusData(prom *sxProm.Prometheus, pluginArgs map[string]string) (map[string][]float64, error) {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPluginGenerateRawPrometheusData, c.Name, prom.URL))
	// Result map where key is node and value is the aggregated string for that node
	nodeResults := make(map[string][]float64)
	precision := 1
	if _, ok := pluginArgs["duration"]; !ok || pluginArgs["duration"] == "" {
		pluginArgs["duration"] = "1h"
	}
	// Créez une nouvelle API v1 pour interroger Prometheus
	insecure := false
	if pluginArgs["insecure"] == "true" {
		insecure = true
	}
	promApi, err := prom.GetPrometheusConnection(insecure)
	if err != nil {
		return nodeResults, err
	}
	startDate, stopDate, stepTime, stepPeriodFull, _, err := HelperGetPeriodParams(c.Name, pluginArgs, precision)
	if err != nil {
		return nodeResults, err
	}

	// Requêtes Prometheus pour CPU, Mémoire, Disques éphémères et PV par node
	queries := map[string]string{
		"cpu_usage":       `sum(increase(container_cpu_usage_seconds_total{node!="", pod!=""}[` + stepPeriodFull + `])) by (node)`,
		"memory_usage":    `max(max_over_time(container_memory_usage_bytes{node!="", pod!=""}[` + stepPeriodFull + `])) by (node)`,
		"ephemeral_disk":  `sum(container_fs_usage_bytes{node!="", pod!=""}) by (node)`,
		"persistent_disk": `sum(kube_persistentvolumeclaim_resource_requests_storage_bytes{node!=""}) by (node)`,
		"pod_usage":       `count(kube_pod_info{node!=""}) by (node)`,
		"container_usage": `count(container_cpu_usage_seconds_total{node!=""}) by (node)`,
	}

	results, errMQ := prom.ExecPrometheusMultipleQuery(queries, promApi, startDate, stopDate, stepTime, "node", "", false)
	if errMQ != nil {
		return nodeResults, errMQ
	} else {
		for queryName, queryResults := range results {
			display.Debug(fmt.Sprintf(dbgPluginGenerateRawPrometheusDataQuery, c.Name, queryName))
			for nodeName, nodeRaw := range queryResults {
				for _, nodeVal := range nodeRaw {
					if _, exists := nodeResults[nodeName]; !exists {
						nodeResults[nodeName] = make([]float64, len(c.header))
					}
					switch queryName {
					case "cpu_usage":
						nodeResults[nodeName][0] = nodeVal["valueHigh"]
					case "memory_usage":
						nodeResults[nodeName][1] = nodeVal["valueHigh"] / (1024 * 1024 * 1024)
					case "ephemeral_disk":
						nodeResults[nodeName][2] = nodeVal["valueHigh"] / (1024 * 1024 * 1024)
					case "persistent_disk":
						nodeResults[nodeName][3] = nodeVal["valueHigh"] / (1024 * 1024 * 1024)
					case "pod_usage":
						nodeResults[nodeName][4] = nodeVal["valueHigh"]
					case "container_usage":
						nodeResults[nodeName][5] = nodeVal["valueHigh"]
					}
				}
			}
		}
	}
	return nodeResults, nil
}
