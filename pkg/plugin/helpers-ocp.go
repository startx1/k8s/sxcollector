package plugin

import (
	"context"
	"strings"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"

	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func HelperNodeParseHardware(node *v1.Node) (string, string) {
	providerID := node.Spec.ProviderID
	if providerID != "" {
		switch {
		case strings.HasPrefix(providerID, "aws://"):
			return "cloud", "aws"
		case strings.HasPrefix(providerID, "gce://"):
			return "cloud", "gce"
		case strings.HasPrefix(providerID, "azure://"):
			return "cloud", "azure"
		case strings.HasPrefix(providerID, "openstack://"):
			return "cloud", "openstack"
		case strings.HasPrefix(providerID, "digitalocean://"):
			return "cloud", "digitalocean"
		case strings.HasPrefix(providerID, "aliyun://"):
			return "cloud", "alibaba"
		case strings.HasPrefix(providerID, "ibm://"):
			return "cloud", "ibm"
		case strings.HasPrefix(providerID, "hcloud://"):
			return "cloud", "hetzner"
		case strings.HasPrefix(providerID, "oci://"):
			return "cloud", "oracle"
		case strings.HasPrefix(providerID, "cloudstack://"):
			return "cloud", "cloudstack"
		case strings.HasPrefix(providerID, "metal3://"):
			return "baremetal", "metal3"
		case strings.HasPrefix(providerID, "nutanix://"):
			return "virtual", "nutanix"
		case strings.HasPrefix(providerID, "vsphere://"):
			return "virtual", "vsphere"
		case strings.HasPrefix(providerID, "kvm://"):
			return "virtual", "kvm"
		}
	} else {
		return "baremetal", "unknown"
	}
	return "unknown", "unknown"
}

// Return the output of the plugin
func HelperGetAllSubscriptionsList(dynamicClient *dynamic.DynamicClient) ([]unstructured.Unstructured, error) {
	result := []unstructured.Unstructured{}

	// Define the GVR for Subscriptions
	subscriptionGVR := schema.GroupVersionResource{
		Group:    "operators.coreos.com",
		Version:  "v1alpha1",
		Resource: "subscriptions",
	}

	// List all Subscriptions in all namespaces
	subscriptionList, errSubs := dynamicClient.Resource(subscriptionGVR).Namespace(metav1.NamespaceAll).List(context.TODO(), metav1.ListOptions{})
	if errSubs != nil {
		return result, errSubs
	}

	return subscriptionList.Items, nil
}
