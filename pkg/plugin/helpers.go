package plugin

import (
	"strings"
)

const (
	dbgPHGenerateCSV                              = "%s : Start generating result to csv format"
	dbgPHGenerateHTML                             = "%s : Start generating result to html format"
	dbgPHGenerateTAB                              = "%s : Start generating result to Tabulated format"
	wrnPHGenerateTABNoData                        = "No data returned to display"
	dbgPHGenerateMAP                              = "%s : Start generating result to Mapped format"
	wrnPHGenerateMAPNoData                        = "No data to display"
	dbgPHWriteFile                                = "%s : Start writing result to file %s"
	errPHWriteFileOpenNOK                         = "could not open file %s because %v"
	errPHWriteFileWriteNOK                        = "could not write result to file %s because %v"
	dbgPHGetPeriodParams                          = "%s : Start getting period parameters"
	dbgPHGetPeriodDiff                            = "%s : Start diff of date between %s and %s"
	errPHGetPeriodParamsParseStartNOK             = "could not parse period start %s because %v"
	errPHGetPeriodParamsParseStopNOK              = "could not parse period stop %s because %v"
	errPHGetPeriodParamsParseDurationNOK          = "could not parse period duration %s because %v"
	dbgPHGetPeriodParamsParseStartOK              = "%s : Period start at %s"
	dbgPHGetPeriodParamsParseStopOK               = "%s : Period stop at %s"
	dbgPHGetPeriodParamsParseDurationStart        = "%s : Period duration is %s %s"
	dbgPHGetPeriodParamsParseResultPeriodOK       = "%s : Period : duration is %s"
	dbgPHGetPeriodParamsParseResultStepOK         = "%s : Period : step is %s"
	dbgPHGetPeriodParamsParseResultStepFullOK     = "%s : Period : step full is %s"
	dbgPHGetPeriodParamsParseResultStepRythmOK    = "%s : Period : step rythm is %s"
	dbgPHGetPeriodParamsParseResultStartOK        = "%s : Period : start date is %s"
	dbgPHGetPeriodParamsParseResultEndOK          = "%s : Period : end date is is %s"
	wrnPHGetPeriodParamsNoStart                   = "no start date provided. use the last 24 hours"
	dbgPHGetPeriodParamsParseStartAfterStop       = "%s : Period start date %s is after stop date %s"
	wrnPluginExtractRQDataFromNamespaceGen        = "Failed to generate resourcequota %s in namespace %s : %v"
	wrnPluginExtractRQDataFromNamespaceCreate     = "Failed to create resourcequota %s in namespace %s : %v"
	dbgPluginExtractRQDataFromNamespaceCreateRQOK = "Created SXQuotas %s in %s"
	wrnPluginExtractRQDataFromNamespaceLoadNOK    = "Failed to load resourcequota %s in %s : %v"
	dbgPluginExtractRQDataFromNamespaceGetUsed    = "Get usage for resourcequota %s in %s : %d entries"
	wrnPluginExtractRQDataFromNamespaceDeleteNOK  = "Failed to delete SXQuotas %s in %s : %v"
	dbgPluginExtractRQDataFromNamespaceDeleteOK   = "Delete SXQuotas %s in %s"
)

func getStringAfterSep(str, sep string) (string, bool) {
	idx := strings.Index(str, sep)
	if idx == -1 {
		return "", false // Separator not found
	}
	result := str[idx+len(sep):]
	return result, true
}
