package plugin

import (
	"fmt"

	// Importer les types de route OpenShift

	sxMailer "gitlab.com/startx1/k8s/go-libs/pkg/mailer"
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	// Importer le schéma du client Kubernetes
)

const (
	dbgPHPrepareMail = "%s : Prepare mail to %s, subject %s"
	dbgPHSendMail    = "%s : Start sending mail to %s, subject %s"
	dbgPHSendMailOK  = "%s : Mail to %s sended, subject %s"
)

// Get the start date, stop date and step time from the generic arguments
func HelperSendMail(pluginName string, host string, port string, username string, password string, insecure bool, mail *sxMailer.MailerMail) (*sxMailer.Mailer, error) {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPHSendMail, pluginName, mail.To, mail.Subject))
	mailer := sxMailer.NewMailer(host, port, username, password)
	errSend := mail.Send(mailer, insecure)
	if errSend != nil {
		return mailer, errSend
	} else {
		display.Debug(fmt.Sprintf(dbgPHSendMailOK, pluginName, mail.To, mail.Subject))
	}
	return mailer, nil
}

// Get the start date, stop date and step time from the generic arguments
func HelperPrepareMail(pluginName string, from string, to string, subject string, message string, attachmentName string, attachmentContent []byte) *sxMailer.MailerMail {
	display := sxUtils.NewCmdDisplay(GroupNameSXCollector)
	display.Debug(fmt.Sprintf(dbgPHPrepareMail, pluginName, to, subject))

	mail := sxMailer.NewMailerMail(
		from,
		to,
		subject,
		message,
	)
	if attachmentName != "" {
		mail.AddAttachment(attachmentName, attachmentContent)
	}
	return mail
}
