/*
Copyright 2021 Startx, member of LaHSC

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package collector

import (
	"context"
	"fmt"

	sxKCli "gitlab.com/startx1/k8s/go-libs/pkg/k8sclient"
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	// GroupAPI is the group API scope use in this package
	GroupAPI = "sxcollector.k8s.startx.fr"
	// GroupName is the group name use in this package
	GroupName = "collector"
	// Warn and error messages
	warnErrorIncrement           = "limit %s with value %s could not increment with %s : %v"
	errLoadErr                   = "limitRange '%s' in namespace '%s' has an error : %v"
	errLoadFound                 = "limitRange '%s' in namespace '%s' is not found : %v"
	errMergeTplFound             = "error getting template : %v"
	errMergeTplFoundQ            = "failed to get destCollector: %s in %s : %v"
	errApplyExec                 = "failed to apply Collector '%s' in namespace '%s' update to the cluster : %v"
	errDetectMinMaxFromPod       = "error getting all pods in namespace '%s' : %v"
	errDetectMinMaxFromContainer = "error getting all containers in namespace '%s' : %v"
	// Debug messages
	ddInitNewRQ                           = "Init Collector object"
	ddInitnewRQ                           = "Init a new Collector object"
	ddInitLoad                            = "Load the limitRanges %s from the cluster"
	ddInitResize                          = "Resize the limitRanges %s to %s (scope: %s)"
	ddInitAdjust                          = "Adjust the limitRanges %s to %s (scope: %s)"
	ddInitMergeTpl                        = "Merge %s template into the %s Collector (obj)"
	ddInitApply                           = "Apply the limitRanges %s in namespace %s the cluster"
	ddInitGetYaml                         = "Return yaml content of the limitRanges %s in namespace %s"
	ddInitPrint                           = "Print content of the limitRanges %s in namespace %s"
	ddInitincrementCollectorValues        = "Increments the limitRanges list"
	ddIncrementRV                         = "Increment from %s to %s for %s"
	ddMergeTplAddVal                      = "Adding %s with value %s into the %s Collector"
	dbgQtStart                            = "DEBUG the limitRange"
	dbgQtCollector                        = "DEBUG - limitRange resource UUID         : %s"
	dbgQtValName                          = "DEBUG - limitRange name                  : %s"
	dbgQtValNamespace                     = "DEBUG - limitRange namespace             : %s"
	dbgQtValIsLoaded                      = "DEBUG - limitRange is loaded             : %t"
	dbgQtValCollectorRule                 = "DEBUG - limitRange define a limit for %s : %s"
	ddMergeTplScanType                    = "Start scanning template limit %s"
	ddMergeTplTypeCreate                  = "template limit %s not found in destination, creating it"
	ddMergeTplScanTypeCat                 = "Start scanning template limit category (Min, Max, Default, ...) for %s type"
	ddMergeTplTypeCatCreate               = "template limit category %s/%s not found in destination, creating it"
	ddMergeTplScanTypeCatResources        = "Start scanning template limit for %s type"
	ddMergeTplScanTypeCatResource         = "Start scanning template limit for %s type, %s category and %s resource with val %s"
	ddMergeTplScanTypeCatResourceCreate   = "template limit resource %s/%s/%s not found in destination, creating it with val %s"
	ddMergeTplScanTypeCatResourceUpdate   = "template limit resource %s/%s/%s found with lower value, updating it with val %s"
	ddMergeTplScanTypeCatResourceNoUpdate = "template limit resource %s/%s/%s found with higher value, nothing to do"
	ddInitAdjustAllForContainer           = "Init ddInitAdjustAllForContainer (increment %s, scope %s)"
	ddInitAdjustAllForPod                 = "Init ddInitAdjustAllForPod (increment %s, scope %s)"
	ddInitAdjustAllForTypeCategory        = "Init AdjustAllForTypeCategory %s - %s (increment %s)"
	ddInitIncrementAllForType             = "Init IncrementAllForType %s (increment %s, scope %s)"
	ddInitAdjustAllForType                = "Init AdjustAllForType %s (increment %s)"
	ddInitIncrementAllForTypeCategory     = "Init IncrementAllForTypeCategory %s - %s (increment %s)"
	ddInitDetectMinMaxFromPod             = "MinMax : Start Detect POD MinMax in namespace %s"
	ddInitDetectMinMaxFromContainer       = "MinMax : Start Detect CONTAINER MinMax in namespace %s"
	ddInitDetectMinMaxChecking            = "MinMax : - Checking pod %s in namespace %s"
	ddInitDetectMinMaxUpdateMin           = "MinMax :   - %s - %s : Minimum = %s"
	ddInitDetectMinMaxUpdateMax           = "MinMax :   - %s - %s : Maximum = %s"
)

//
// Definitions of the Collectors object
//

// Collectors represent the object properties
type Collectors struct {
	k8sclient *sxKCli.K8sClient
	Collector *v1.LimitRange
	Name      string
	Namespace string
	IsLoaded  bool
	display   *sxUtils.CmdDisplay
}

// NewRQs create a new Collectors ready to use the clientset
// to interact with the kubernetes cluster
func NewRQ(name string, namespace string, k8sclient *sxKCli.K8sClient) *Collectors {
	// sxUtils.DisplayDebug(GroupName,ddInitNewRQ)
	limitRange := newRQ(name, namespace)
	return &Collectors{
		k8sclient: k8sclient,
		Name:      name,
		Namespace: namespace,
		Collector: limitRange,
		IsLoaded:  false,
		display:   sxUtils.NewCmdDisplay(GroupName),
	}
}

// newRQ create a new CollectorSpec with name and namespace set
// to the given parameters. everitying else is default
func newRQ(name string, namespace string) *v1.LimitRange {
	// display := sxUtils.NewCmdDisplay(GroupName)
	// display.Debug(ddInitnewRQ)
	// Define the Collector spec.
	limitRangeSpec := v1.LimitRangeSpec{
		Limits: []v1.LimitRangeItem{
			{
				Type: v1.LimitTypePod,
				Max: map[v1.ResourceName]resource.Quantity{
					v1.ResourceCPU:    resource.MustParse("1"),
					v1.ResourceMemory: resource.MustParse("2Gi"),
				},
				Default: map[v1.ResourceName]resource.Quantity{
					v1.ResourceCPU:    resource.MustParse("500m"),
					v1.ResourceMemory: resource.MustParse("500Mi"),
				},
			},
		},
	}

	// Define the Collector object
	limitRange := &v1.LimitRange{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: limitRangeSpec,
	}
	return limitRange
}

// Load load the limitRanges %s from the cluster
// or return an error
func (rq *Collectors) Load() error {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug(fmt.Sprintf(ddInitLoad, rq.Name))
	limitrange, err := rq.k8sclient.Clientset.CoreV1().LimitRanges(rq.Namespace).Get(
		context.TODO(),
		rq.Name,
		metav1.GetOptions{},
	)
	if err != nil {
		messageErr := errLoadErr
		if errors.IsNotFound(err) {
			messageErr = errLoadFound
		}
		return fmt.Errorf(
			messageErr,
			rq.Name,
			rq.Namespace,
			err,
		)
	}
	rq.Collector = limitrange
	rq.IsLoaded = true
	return nil
}
