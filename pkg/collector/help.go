package collector

import (
	"fmt"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
)

// DisplayHelpCmdDescribe display the help message for the
// sxcollector describe sub-command
func DisplayHelpCmd() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the sxcollector help message")
	helpMessage := `
sxcollector allow you to interact and observe limitrange resource into your
running kubernetes cluster. 

Usage: sxcollector COMMAND SUBCOMMAND [OPTIONS]...

Commands:
- cluster-sub            Get the full list of subscriptions     (now)
- node-info              Get the full description of the nodes  (now)
- node-state             Get the state of the nodes             (now)
- node-billing           Get the nodes billing for redhat       (now)
- node-conso             Get the consumtion of the nodes        (period)
- node-ns-conso          Get the consumtion of the nodes and NS (period)
- ns-conso               Get the consumtion of the namespaces   (period)
- ns-info                Get the full description of namespaces (now)
- ns-state               Get the state of the namespaces        (now)

Output options:
  --output-format FORMAT        Set the formating of the generated content. Could be tab, json or csv (default is tab)
  --output-csv-separator SEP    Define the separator sign to use for the csv format (default is ,)
  --output-noheaders            Disable headers in the result (default is false)
  --output-nounits              Disable unit header in the result (default is false)
  --output-file FILENAME        Define the output file. If not set, stdout will be used (default is stdout)

Prometheus options:
  --insecure                    Skip TLS verification for prometheus communication (default false)
  --from DATE                   Define the starting date. ex: 2024-01-01 00:00:00 (default is now)
  --to DATE                     Define the ending date. ex: 2024-01-03. Have precedence over duration if both are set ( no default)
  --duration DURATION           Define the period duration. ex: 1y. Could be overwrite by --to (default is 1m)
  --precision PRECISION         A factor that divide the duration. Used for slicing prometheus data (default is 1 = all)

Mail options:
  --mail-to EMAIL               Define the destination mail. If set, output will be of type mail. SMTP params must be set
  --mail-from EMAIL             Define the source mail. If set, output will be of type mail. If not use MAIL_FROM env
  --mail-subject SUBJECT        Customize the mail subject. Default is "Report SXCollector %s at %s" where first arg is name of the plugin and second the date of execution
  --smtp-host HOST              Define the SMTP host. If output type is mail and host is not set, use the SMTP_HOST env var
  --smtp-port PORT              Define the SMTP port. If output type is mail and port is not set, use the SMTP_PORT env var
  --smtp-username USERNAME      Define the SMTP username. If output type is mail and username is not set, use the SMTP_USERNAME env var
  --smtp-password PASSWORD      Define the SMTP password. If output type is mail and password is not set, use the SMTP_PASSWORD env var
  --smtp-insecure               Define if SMTP connection is insecure. If output type is mail and --smtp-insecure is not set, use the SMTP_INSECURE=true env var

Confluence options:
  --confluence-url URL            The confluence base URL ex: https://example.atlassian.net/wiki. If --confluence-url is not set, use the CONFLUENCE_BASE_URL env var
  --confluence-username USERNAME  The confluence username ex: user@example.com. If --confluence-username is not set, use the CONFLUENCE_USERNAME env var
  --confluence-apitoken APITOKEN  The confluence API Token, see https://id.atlassian.com/manage-profile/security/api-tokens. If --confluence-apitoken is not set, use the CONFLUENCE_API_TOKEN env var
  --confluence-spacekey SPACEKEY  The confluence Space key. If --confluence-spacekey is not set, use the CONFLUENCE_SPACE_KEY env var
  --confluence-title TITLE        The confluence page title (or section if --confluence-mode=append). If --confluence-title is not set, use the CONFLUENCE_PAGE_TITLE env var
  --confluence-mode               Define the default confluence mode (could be create, update, append). Default is create
  --confluence-content CONTENT    The confluence page content (prepended to the generated content). If --confluence-content is not set, use the CONFLUENCE_PAGE_HTML_CONTENT env var 
  --confluence-pageid PAGEID      The confluence pageID (required except for --confluence-mode=create). If --confluence-pageid is not set, use the CONFLUENCE_PAGE_ID env var
  --confluence-parentid PARENTID  The confluence parentID (useful with --confluence-mode=create). If --confluence-parentid is not set, use the CONFLUENCE_PARENT_ID env var

Generic options:
  --debug                       Activates debug mode for detailed troubleshooting information.
  --help                        Displays this help message and exits.
  --kubeconfig FILEPATH         Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided)

Environment variables :

  SMTP_HOST                     Define the SMTP host in replacement of the --smtp-host argument
  SMTP_PORT                     Define the SMTP port in replacement of the --smtp-port argument
  SMTP_USERNAME                 Define the SMTP username in replacement of the --smtp-username argument
  SMTP_PASSWORD                 Define the SMTP password in replacement of the --smtp-password argument
  MAIL_FROM                     Define the source mail in replacement of the --mail-from argument
  MAIL_SUBJECT                  Define the source mail in replacement of the --mail-subject argument
  CONFLUENCE_BASE_URL           Define the confluence base URL in replacement of the --confluence-url argument
  CONFLUENCE_USERNAME           Define the confluence username in replacement of the --confluence-username argument
  CONFLUENCE_API_TOKEN          Define the confluence API Token in replacement of the --confluence-apitoken argument
  CONFLUENCE_SPACE_KEY          Define the confluence Space key in replacement of the --confluence-spacekey argument
  CONFLUENCE_PAGE_TITLE         Define the page title in replacement of the --confluence-title argument
  CONFLUENCE_PAGE_HTML_CONTENT  Define the page content in replacement of the --confluence-content argument
  CONFLUENCE_PAGE_ID            Define the confluence pageID in replacement of the --confluence-pageid argument
  CONFLUENCE_PARENT_ID          Define the confluence parentID in replacement of the --confluence-parentid argument
  SXCOLLECTOR_FORMAT            Define the output format in replacement of the --output-format argument
  SXCOLLECTOR_TO                Define the destination mail in replacement of the --mail-to argument
  SXCOLLECTOR_DURATION          Define the prometheus duration in replacement of the --duration argument
  SXCOLLECTOR_PRECISION         Define the prometheus precision in replacement of the --precision argument
  SXCOLLECTOR_INSECURE          Enable the insecure connection for prometheus in replacement of the --insecure argument
  SXCOLLECTOR_NOHEADERS         Disable the headers in replacement of the --output-noheaders argument
  SXCOLLECTOR_NOUNITS           Disable the units in replacement of the --output-nounits argument
  SXCOLLECTOR_SEP               Define the separator character in replacement of the --output-csv-separator argument

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}
