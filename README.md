# sxCollector [![release](https://img.shields.io/badge/release-v0.2.6-blue.svg)](https://gitlab.com/startx1/k8s/sxcollector/-/releases/v0.2.6) [![last commit](https://img.shields.io/gitlab/last-commit/startx1/k8s/sxcollector.svg)](https://gitlab.com/startx1/k8s/sxcollector) [![Doc](https://readthedocs.org/projects/sxcollector/badge)](https://sxcollector.readthedocs.io) 

This project is focused on producing a `sxcollector` command line who can 
interact with an Openshift or a Kubernetes cluster and allow collecting various 
metrics and cluster informations about your targeted cluster.

More information can be found on the [official documentation](https://sxcollector.readthedocs.io).

## Getting started

- [Install the `sxcollector` binary](https://sxcollector.readthedocs.io/en/latest/installation/) [(download)](https://gitlab.com/startx1/k8s/sxcollector/-/raw/stable/bin/sxcollector)
- Test it with `sxcollector version` subcommand
- Log into a kubernetes cluster `kubectl set-context ...` or an openshit cluster with `oc login ...`
- [using the `sxcollector` binary](https://sxcollector.readthedocs.io/en/latest/subc/index.md) and all it's subcommands :
    - [**cluster-sub**](https://sxcollector.readthedocs.io/en/latest/subc/ClusterSub.md) : Get the full list of subscriptions (Openshift)
    - [**node-state**](https://sxcollector.readthedocs.io/en/latest/subc/NodeState.md) : Get the state of the cluster nodes (Kubernetes)
    - [**node-info**](https://sxcollector.readthedocs.io/en/latest/subc/NodeInfo.md) : Get the detailled info of the cluster nodes (Kubernetes)
    - [**node-conso**](https://sxcollector.readthedocs.io/en/latest/subc/NodeConso.md) : Get the detailled consumptions of the cluster nodes (Openshift)
    - [**node-ns-conso**](https://sxcollector.readthedocs.io/en/latest/subc/NodeNsConso.md) : Get the detailled consumptions of the cluster nodes and namespace (Openshift)
    - [**node-billing**](https://sxcollector.readthedocs.io/en/latest/subc/NodeBilling.md) : Get the detailled informations for the billing of the cluster (Openshift)
    - [**ns-state**](https://sxcollector.readthedocs.io/en/latest/subc/NsState.md) : Get the state of the cluster namespaces (Kubernetes)
    - [**ns-info**](https://sxcollector.readthedocs.io/en/latest/subc/NsInfo.md) : Get the detailled info of the cluster namespaces (Kubernetes)
    - [**ns-conso**](https://sxcollector.readthedocs.io/en/latest/subc/NsConso.md) : Get the detailled consumptions of the cluster namespaces (Openshift)
    - [**ns-deep**](https://sxcollector.readthedocs.io/en/latest/subc/NsDeep.md) : Get the detailled consumptions of kinds for each cluster namespaces (Kubernetes)

