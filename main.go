package main

import (
	"os"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	sxCollectorUtils "gitlab.com/startx1/k8s/sxcollector/pkg/utils"
)

func main() {
	display := sxUtils.NewCmdDisplay("main")
	display.Debug("Start the main function")
	argsCollector := sxCollectorUtils.NewSXCollectorArgParser(os.Args)
	argsCollector.Prepare()
	argsCollector.Exec()
}
