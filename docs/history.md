# Release history

## version 1.0.x (champagnac)

_The objectif of this release is to stabilize the full repository content and offer a stable release of sxcollector._

## version 0.4.x (chaumeil)

_The objectif of this release is to benefit from feedback and focus on creating a small community rhythm._

## version 0.3.x (chassagne)

_The objectif of this release is to communicate about the availability of this application._

## version 0.2.x (chastang)

The objectif of this release is to provide integration with the kubectl krew plugin mechanism, as well
as rpm, deb, homebrew, snap and container version.

### History

| Release                                                           | Date       | Description                                                                                                |
| ----------------------------------------------------------------- | ---------- | ---------------------------------------------------------------------------------------------------------- |
| [0.2.6](https://gitlab.com/startx1/k8s/go-libs/-/tags/v0.2.6)     | 2025-01-24 | Update all dependencie and add support for custom mail subject with --mail-subject or env var MAIL_SUBJECT |
| [0.2.5](https://gitlab.com/startx1/k8s/go-libs/-/tags/v0.2.5)     | 2024-11-12 | Add support for a second key in prometheus MultiQuery response for NodeNsConso                             |
| [0.2.1](https://gitlab.com/startx1/k8s/sxcollector/-/tags/v0.2.1) | 2024-11-12 | Upgrade to go version 1.23.3                                                                               |

## version 0.1.x (chauzu)

The objectif of this release is to use a common startx library for both sxcollector and sxcollector.

| Release                                                            | Date       | Description                                                                       |
| ------------------------------------------------------------------ | ---------- | --------------------------------------------------------------------------------- |
| [0.1.17](https://gitlab.com/startx1/k8s/go-libs/-/tags/v0.1.17)    | 2024-11-12 | Update all dependencies                                                           |
| [0.1.15](https://gitlab.com/startx1/k8s/go-libs/-/tags/v0.1.15)    | 2024-10-17 | Add support for output in google Spreadsheet                                      |
| [0.1.11](https://gitlab.com/startx1/k8s/go-libs/-/tags/v0.1.11)    | 2024-10-16 | Sort all subcommand in a unified way (node or namespace)                          |
| [0.1.9](https://gitlab.com/startx1/k8s/go-libs/-/tags/v0.1.9)      | 2024-10-15 | Enable the unauthenticated SMTP relay                                             |
| [0.1.7](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.1.7) | 2024-10-14 | Add support for create and update confluence output                               |
| [0.1.5](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.1.5) | 2024-10-13 | Add support for confluence output and upgrade all                                 |
| [0.1.1](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.1.1) | 2024-10-11 | Improve smtp-insecure                                                             |
| [0.1.0](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.1.0) | 2024-10-10 | Adding examples and docs for container, kustomize, openshift, raw, argocd and cli |


## version 0.0.x (champeaux)

This version is a POC for testing purpose.

The objectif of this release is to create the repository structure and fundamentals of the project.

### History

| Release                                                              | Date       | Description                                                                             |
| -------------------------------------------------------------------- | ---------- | --------------------------------------------------------------------------------------- |
| [0.0.37](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.37) | 2024-10-09 | Upgrade all dependencies and add env var SXCOLLECTOR_FORMAT                             |
| [0.0.35](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.35) | 2024-10-08 | Create container version and upgrade all dependencies                                   |
| [0.0.33](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.33) | 2024-10-07 | Release a static binary                                                                 |
| [0.0.31](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.31) | 2024-10-05 | Update package and debug argParser                                                      |
| [0.0.29](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.29) | 2024-10-05 | Add the support for mail output                                                         |
| [0.0.27](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.27) | 2024-10-04 | Add the ns-deep subcommand                                                              |
| [0.0.25](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.25) | 2024-10-03 | Move prometheus interaction to the sx-golib                                             |
| [0.0.23](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.23) | 2024-10-02 | Add the cluster-sub subcommand                                                          |
| [0.0.21](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.21) | 2024-10-01 | Add the node-billing subcommand                                                         |
| [0.0.19](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.19) | 2024-09-29 | Improving the prometheus request for ns-conso, node-conso et node-state subcommand      |
| [0.0.17](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.17) | 2024-09-26 | Adding initial structure for ns-info et ns-state subcommand                             |
| [0.0.15](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.15) | 2024-09-26 | Adding tabulated output format (now default) and stable ArgParser for period parameters |
| [0.0.11](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.11) | 2024-09-25 | Update dependencies for openshift-api                                                   |
| [0.0.9](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.9)   | 2024-09-24 | Update dependencies for security and debug help subcommand                              |
| [0.0.7](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.7)   | 2024-09-15 | Adding plugin subcommand for output 5sep, file, noheaders and nounits                   |
| [0.0.5](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.5)   | 2024-09-14 | Stable for ns and node subcommand                                                       |
| [0.0.3](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.3)   | 2024-09-08 | Change the code structure, unstable                                                     |
| [0.0.1](https://gitlab.com/startx1/k8s/sxcollector/-/tags/vv0.0.1)   | 2024-06-08 | Init the first release of the sxlimit package derivated from the sxlimits project       |
