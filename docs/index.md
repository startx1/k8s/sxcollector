# Startx Collector (sxcollector) 

[![release](https://img.shields.io/badge/release-v0.2.6-blue.svg)](https://gitlab.com/startx1/k8s/sxcollector/-/releases/v0.2.6) [![last commit](https://img.shields.io/gitlab/last-commit/startx1/k8s/sxcollector.svg)](https://gitlab.com/startx1/k8s/sxcollector) [![Doc](https://readthedocs.org/projects/sxcollector/badge)](https://sxcollector.readthedocs.io) 

The `sxcollector` command line can interact with a kubernetes or openshift cluster and allow operations 
to collect various cluster informations and extract them into reports to enable rgular analysis.

## Getting started

- [Install the `sxcollector` binary](installation.md)
- Test it with `sxcollector version` subcommand
- Log into a kubernetes cluster `kubectl set-context ...` or an openshit cluster with `oc login ...`
- [using the `sxcollector` binary](subc/index.md) and all it's subcommands :
    - [**cluster-sub**](subc/ClusterSub.md) : Get the full list of subscriptions (Openshift)
    - [**node-state**](subc/NodeState.md) : Get the state of the cluster nodes (Kubernetes)
    - [**node-info**](subc/NodeInfo.md) : Get the detailled info of the cluster nodes (Kubernetes)
    - [**node-conso**](subc/NodeConso.md) : Get the detailled consumptions of the cluster nodes (Openshift)
    - [**node-ns-conso**](subc/NodeNsConso.md) : Get the detailled consumptions of the cluster nodes and namespaces (Openshift)
    - [**node-billing**](subc/NodeBilling.md) : Get the detailled informations for the billing of the cluster (Openshift)
    - [**ns-state**](subc/NsState.md) : Get the state of the cluster namespaces (Kubernetes)
    - [**ns-info**](subc/NsInfo.md) : Get the detailled info of the cluster namespaces (Kubernetes)
    - [**ns-conso**](subc/NsConso.md) : Get the detailled consumptions of the cluster namespaces (Openshift)
    - [**ns-deep**](subc/NsDeep.md) : Get the detailled consumptions of kinds for each cluster namespaces (Kubernetes)
