# Kustomize Examples

**sxcollector** can be deployed in every kubernetes cluster using the kustomize structure.
You can load the **sxcollector** using `kubectl apply -k` if you use a working copy of the 
sxcollector in your manager node. 

## Requirements

### Clone the sxcollector repository

```bash
git clone https://gitlab.com/startx1/k8s/sxcollector.git
```

### Go to the kustomize structure

```bash
cd sxcollector/examples/kustomize
```

### Connect to your kubernetes cluster


### Connect to a cluster 

#### Kubernetes cluster

```bash
kubectl \
   config set-credentials \
   mycluster/myuser \
   --username=kubeuser \
   --password=kubepassword
kubectl \
   config set-cluster \
   mycluster \
   --insecure-skip-tls-verify=true \
   --server=https://mycluster.kubernetes.com
kubectl \
   config set-context \
   mycluster/myuser \
   --user=mycluster/myuser \
   --namespace=default \
   --cluster=mycluster
kubectl \
   config use-context \
   mycluster/myuser
```

#### Openshift cluster

```bash
oc login  \
   --username=kubeuser \
   --password=kubepassword \
   https://mycluster.openshift.com
```

#### Default 

You can change the default kubeconfig using environment variable 

```bash
export KUBECONFIG=/home/user/mykubeconfig
```

By default the /home/user/.kube/config file will be used. ensure that the default context is
connected and can interact with a kubernetes cluster.


## Deploy SXCollector

### Full default

Deploy all the stack in the **sxcollector** namespace.

```bash
kubectl apply -k .
```

### Full demo

Deploy all the stack in the **sxcollector-demo** namespace.

```bash
kubectl apply -k overlays/full-demo/
```

### Step-by-step

#### Create the namespace

```bash
kubectl apply -k overlays/ns/
```

create the following components :

- namespace : **sxcollector**
- serviceaccount : **sxcollector** in sxcollector NS
- imagestream : **sxcollector** in sxcollector NS
- clusterRoleBinding : **sxcollector-cluster-admin** enable ServiceAccount sxcollector in sxcollector NS to be cluster-admin

#### Configure SXCollector

You must create a structure to configure the sxcollector various jobs to work.
2 examples of configuration are profided :

- config-mail: Define various kind of environements variables in order to send CSV using email replays
```bash
kubectl apply -k overlays/config-mail/
```
- config-tab : Define default environments variables in order to display Tabulated content into jobs outputs
```bash
kubectl apply -k overlays/config-tab/
```

create the following components :

- secret : **sxcollector**

#### Deploy jobs examples

```bash
kubectl apply -k overlays/jobs
```

create the following components :

- jobs : **node-info-instant** in sxcollector NS
- jobs : **ns-info-instant** in sxcollector NS

#### Deploy cronjobs examples

```bash
kubectl apply -k overlays/cronjobs
```

create the following components :

- cronjobs : **node-conso-mail-monthly** in sxcollector NS
- cronjobs : **ns-state-mail-hourly** in sxcollector NS
- cronjobs : **cluster-sub-mail-monthly** in sxcollector NS
- cronjobs : **node-state-mail-hourly** in sxcollector NS
- cronjobs : **node-billing-mail-monthly** in sxcollector NS
- cronjobs : **ns-conso-mail-weekly** in sxcollector NS
- cronjobs : **node-conso-confluence-monthly** in sxcollector NS
- cronjobs : **ns-state-confluence-hourly** in sxcollector NS
- cronjobs : **cluster-sub-confluence-monthly** in sxcollector NS
- cronjobs : **node-state-confluence-hourly** in sxcollector NS
- cronjobs : **node-billing-confluence-monthly** in sxcollector NS
- cronjobs : **ns-conso-confluence-weekly** in sxcollector NS

#### Deploy debugger

If you whan, you can deploy a deployment using the same configuration (image, secret)
as the jobs and cronjobs instances. It will run as a sleeping daemon unless you 
use it manually (kubectl rsh ...) in order to execute `sxcollector` commands manually.

```bash
kubectl apply -k overlays/debug
```

create the following components :

- deployment : **sxcollector-debug**
