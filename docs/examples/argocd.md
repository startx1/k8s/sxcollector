# ArgoCD Examples

**sxcollector** can be deployed using ArgoCD for a better tracing of the applyed configuration
and using a gitops approch for the deployment of your configuration.

## Requirements

### Clone the sxcollector repository

```bash
git clone https://gitlab.com/startx1/k8s/sxcollector.git
```

### Go to the kustomize structure

```bash
cd sxcollector/examples/argocd
```

### Connect to your kubernetes cluster

#### Kubernetes cluster

```bash
kubectl \
   config set-credentials \
   mycluster/myuser \
   --username=kubeuser \
   --password=kubepassword
kubectl \
   config set-cluster \
   mycluster \
   --insecure-skip-tls-verify=true \
   --server=https://mycluster.kubernetes.com
kubectl \
   config set-cluster \
   mycluster \
   --insecure-skip-tls-verify=true \
   --server=https://mycluster.kubernetes.com
kubectl \
   config set-context \
   mycluster/myuser \
   --user=mycluster/myuser \
   --namespace=default \
   --cluster=mycluster
kubectl \
   config use-context \
   mycluster/myuser
```

#### Openshift cluster

```bash
oc login  \
   --username=kubeuser \
   --password=kubepassword \
   https://mycluster.openshift.com
```

#### Default 

You can change the default kubeconfig using environment variable 

```bash
export KUBECONFIG=/home/user/mykubeconfig
```

By default the /home/user/.kube/config file will be used. ensure that the default context is
connected and can interact with a kubernetes cluster.


## Deploy SXCollector

### Deploy in kubernetes cluster

```bash
kubectl apply -k overlays/kubernetes
```

### Deploy in openshift cluster

```bash
kubectl apply -k overlays/openshift
```

## Deployment result

create the following components :

- application : **sxcollector** that will create and autosync
  - namespace : **sxcollector**
  - serviceaccount : **sxcollector** in sxcollector NS
  - imagestream : **sxcollector** in sxcollector NS
  - clusterRoleBinding : **sxcollector-cluster-admin** enable ServiceAccount sxcollector in sxcollector NS to be cluster-admin
  - secret : **sxcollector**
  - jobs : **node-info-instant** in sxcollector NS
  - jobs : **ns-info-instant** in sxcollector NS
  - cronjobs : **node-conso-monthly** in sxcollector NS
  - cronjobs : **ns-state-hourly** in sxcollector NS
  - cronjobs : **cluster-sub-monthly** in sxcollector NS
  - cronjobs : **node-state-hourly** in sxcollector NS
  - cronjobs : **node-billing-monthly** in sxcollector NS
  - cronjobs : **ns-conso-weekly** in sxcollector NS
  - deployment : **sxcollector-debug**
