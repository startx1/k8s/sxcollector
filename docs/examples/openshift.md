# Openshift Examples

**sxcollector** can be deployed in an Openshift cluster using the template 
resource and through the openshift catalog.
You can load the **sxcollector** example structure into the catalog, and use the cli 
as well as the web console to deploy one or multiple sxcollector scheduled jobs. 

## Load the SXCollector templates into catalog

This command will add the sxcollector templates into the openshift global catalog. 
You must have the write access to the **openshift** namespace.

```
export BASE_URL=https://gitlab.com/startx1/k8s/sxcollector/-/raw/main/docs/examples/openshift
oc apply -f ${BASE_URL}/template-sxcollector-ns.yaml
oc apply -f ${BASE_URL}/template-sxcollector-app.yaml
```

## Create a new project

This command will create a new `sxcollector-test` namespace with the appropriate configuration
(ClusterRoleBinding).

```
SXNS=sxcollector-test
oc process \
   sxcollector-ns \
   -n openshift \
   -p APP_NAME=sxcollector \
   -p APP_NS=$SXNS \
   | \
   oc apply -f -
```

## Deploy sxcollector

This command will create a bunch of new `sxcollector` cronjobs into the `sxcollector-test`
namespace with generic example configuration that must be adapted.

```
SXNS=sxcollector-test
oc process \
   sxcollector-app \
   -n openshift \
   -p APP_NAME=sxcollector \
   -p APP_NS=$SXNS \
   | \
   oc apply -f -
```
