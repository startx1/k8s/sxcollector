# Container Examples

**sxcollector** can be run with every container runtimes (crio, podman, docker,...).
This example page use podman, but you can easilly adjust command for crioctl or docker 
runtimes.

## Requirements

### Install the sxcollector image

```bash
podman pull docker.io/startx/sxcollector:latest
```

## Using sxcollector container

SXCollector container execute by default the sxcollector binary. You use pass to your 
container all the sxcollector parameters you require to get the appropriate collected data.

### List the availables commands

```bash
podman run docker.io/startx/sxcollector:latest --help
```

### Get the node-info sended by mail

```bash
podman run \
   docker.io/startx/sxcollector:latest \
   --insecure \
   --mail-from dev@example.com \
   --mail-to dev@example.com \
   --smtp-host smtp.gmail.com \
   --smtp-port "587" \
   --smtp-username dev@example.com \
   --smtp-password "mypassword" \
   --smtp-insecure \
   --output-format csv
```

### Get the node-info recorded in confluence

```bash
podman run \
   docker.io/startx/sxcollector:latest \
   --insecure \
   --confluence-mode create \
   --confluence-url https://example.atlassian.net/wiki \
   --confluence-username user@example.com \
   --confluence-apitoken mytoken \
   --confluence-spacekey MYSPACE \
   --confluence-title "My new page" \
   --confluence-content "<h1>this is a page<h1/>" \
   --confluence-parentid "123456" \
   --confluence-pageid "12345678"
```

### Sub-command availables

- [**cluster-sub**](../subc/ClusterSub.md) : Get the full list of subscriptions (Openshift)
- [**node-state**](../subc/NodeState.md) : Get the state of the cluster nodes (Kubernetes)
- [**node-info**](../subc/NodeInfo.md) : Get the detailled info of the cluster nodes (Kubernetes)
- [**node-conso**](../subc/NodeConso.md) : Get the detailled consumptions of the cluster nodes (Openshift)
- [**node-ns-conso**](../subc/NodeNsConso.md) : Get the detailled consumptions of the cluster nodes and namespaces (Openshift)
- [**node-billing**](../subc/NodeBilling.md) : Get the detailled informations for the billing of the cluster (Openshift)
- [**ns-state**](../subc/NsState.md) : Get the state of the cluster namespaces (Kubernetes)
- [**ns-info**](../subc/NsInfo.md) : Get the detailled info of the cluster namespaces (Kubernetes)
- [**ns-conso**](../subc/NsConso.md) : Get the detailled consumptions of the cluster namespaces (Openshift)
- [**ns-deep**](../subc/NsDeep.md) : Get the detailled consumptions of kinds for each cluster namespaces (Kubernetes)