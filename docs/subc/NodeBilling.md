# sxcollector node-billing

This subcommand allow you to get a billing range of information about your nodes for Openshift cluster. 
You will get a result with the following content :

- **Install Date** : Creation date
- **Product Name** : Product installed
- **Role** : Role of this node
- **SLA** : SLA for this product
- **Version** : Used for tracking versions requiring ELS
- **Node Name** : Server hostname
- **Server Hardware** : Indicate if host is Physical or Virtual
- **Hypervisor Name** : Indicate the name of the VMWare Virtual Host
- **Cluster ID** : OpenShift Cluster ID
- **Sockets on Physical Server** : Number of Sockets (if Physical)
- **Cores on Physical Server** : Number of Cores (if Physical)
- **Hypervisor Cores** : Total number of cores on the hypervisor
- **Virtual Cores** : Number of Virtual CPUs a (Virtual)
- **Hyperthreading** : Indicate if hyperthreading is enabled

All result will be sort by nodeName and multiple execution of this subcommand will return the same ordered list as long as the node naming convention is respected.

## Usage

```bash
sxcollector node-billing [OPTIONS]...
```

### Generic options

All the generics options could be used for this subcommand. You shoud [read to the generics options](../options/generic.md) for the full list of parameters and environments variables for theses options.

###  Output options

All the formating options could be used for this subcommand. You shoud [read to the formating options](../options/output.md) for the full list of parameters and environments variables for theses options.

###  Mail options

All the mail options could be used for this subcommand. You shoud [read to the mails options](../options/mail.md) for the full list of parameters and environments variables for theses options.


### Confluence options

All the confluences options could be used for this subcommand. You shoud [read to the confluences options](../options/confluence.md) for the full list of parameters and environments variables for theses options.

### Google Spreasheet options

All the google spreasheet options could be used for this subcommand. You shoud [read to the spreadsheet options](../options/gsheet.md) for the full list of parameters and environments variables for theses options.

## Examples

### Return the nodes billing informations

```bash
sxcollector node-billing 
```

#### Output


```bash
Install Date  Product Name                  Role                         SLA                   Version                                   Node Name                                  Server Hardware                          Hypervisor Name                              Cluster ID                            Sockets on Physical Server       Cores on Physical Server       Hypervisor Cores                         Virtual Cores                       Hyperthreading                         
Creation date Product installed             Role of this node            SLA for this product  Used for tracking versions requiring ELS  Server hostname                            Indicate if host is Physical or Virtual  ndicate the name of the VMWare Virtual Host  OpenShift Cluster ID                  Number of Sockets (if Physical)  Number of Cores (if Physical)  Total number of cores on the hypervisor  Number of Virtual CPUs a (Virtual)  Indicate if hyperthreading is enabled  
------------- ----------------------------- ---------------------------- --------------------- ----------------------------------------- ------------------------------------------ ---------------------------------------- -------------------------------------------- ------------------------------------- -------------------------------- ------------------------------ ---------------------------------------- ----------------------------------- --------------------------------------                                                                                       
2024-10-01    Openshift Container Platform  master,control-plane,worker  Standard              4.15.27                                   ip-1-1-1-1.eu-west-3.compute.internal  cloud                                    aws                                          45d89b6c-bf29-4520-9f38-410143373ced                                                                                                           8                                   false                                  
2024-10-01    Openshift Container Platform  control-plane,master,worker  Standard              4.15.27                                   ip-1-1-1-2.eu-west-3.compute.internal cloud                                    aws                                          45d89b6c-bf29-4520-9f38-410143373ced                                                                                                           8                                   false                                  
2024-10-01    Openshift Container Platform  master,control-plane,worker  Standard              4.15.27                                   ip-1-1-1-3.eu-west-3.compute.internal cloud                                    aws                                          45d89b6c-bf29-4520-9f38-410143373ced                                                                                                           8                                   false                                  
```

### Return the nodes billing informations in CSV

```bash
sxcollector node-billing --output-format csv
```

#### Output


```csv
Install Date,Product Name,Role,SLA,Version,Node Name,Server Hardware,Hypervisor Name,Cluster ID,Sockets on Physical Server,Cores on Physical Server,Hypervisor Cores,Virtual Cores,Hyperthreading
Creation date,Product installed,Role of this node,SLA for this product,Used for tracking versions requiring ELS,Server hostname,Indicate if host is Physical or Virtual,ndicate the name of the VMWare Virtual Host,OpenShift Cluster ID,Number of Sockets (if Physical),Number of Cores (if Physical),Total number of cores on the hypervisor,Number of Virtual CPUs a (Virtual),Indicate if hyperthreading is enabled
2024-10-01,Openshift Container Platform,"master,control-plane,worker",Standard,4.15.27,ip-1-1-1-1.eu-west-3.compute.internal,cloud,aws,45d89b6c-bf29-4520-9f38-410143373ced,,,,8,false
2024-10-01,Openshift Container Platform,"control-plane,worker,master",Standard,4.15.27,ip-1-1-1-2.eu-west-3.compute.internal,cloud,aws,45d89b6c-bf29-4520-9f38-410143373ced,,,,8,false
2024-10-01,Openshift Container Platform,"master,control-plane,worker",Standard,4.15.27,ip-1-1-1-3.eu-west-3.compute.internal,cloud,aws,45d89b6c-bf29-4520-9f38-410143373ced,,,,8,false
```
