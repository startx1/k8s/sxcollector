# sxcollector node-info

This subcommand allow you to get a info range of information about your nodes within a Kubernetes cluster. 
You will get a result with the following content :

- **Name** : Node name
- **Role** : Role of the node
- **Status** : State of the node
- **Age** : since creation (in hours)
- **Region** : Failure domain region
- **Zone** : Failure domain zone
- **InstanceType** : Type of hardware instance
- **Internal IP** : Private IP
- **External IP** : Public IP
- **Processor** : Processor architecture
- **OS** : OS name and version
- **Kernel** : Kernel version
- **Kubelet** : Kubelet version
- **chassisID** : ID of the Baremetal chassis
- **machineID** : ID of the machine
- **systemUUID** : ID of the system
- **CapacityCpu** : Number of cores capacity
- **AllocatableCpu** : Number of allocatable cores
- **CapacityThreads** : Number of threads capacity
- **AllocatableThreads** : Number of allocatable threads
- **CapacityMemoryGB** : Memory capacity (in GB)
- **AllocatableMemoryGB** : Memory allocatable (in GB)
- **CapacityEphemeralStorage** : ephemeral storage capacity (in GB)
- **AllocatableEphemeralStorage** : ephemeral storage allocatable (in GB)
- **CapacityPods** : Nbr of pods capacity
- **AllocatablePods** : Nbr of pods allocatable

All result will be sort by node name and multiple execution of this subcommand will return the same ordered list.

## Usage

```bash
sxcollector node-info [OPTIONS]...
```

### Generic options

All the generics options could be used for this subcommand. You shoud [read to the generics options](../options/generic.md) for the full list of parameters and environments variables for theses options.

###  Output options

All the formating options could be used for this subcommand. You shoud [read to the formating options](../options/output.md) for the full list of parameters and environments variables for theses options.

###  Mail options

All the mail options could be used for this subcommand. You shoud [read to the mails options](../options/mail.md) for the full list of parameters and environments variables for theses options.


### Confluence options

All the confluences options could be used for this subcommand. You shoud [read to the confluences options](../options/confluence.md) for the full list of parameters and environments variables for theses options.

### Google Spreasheet options

All the google spreasheet options could be used for this subcommand. You shoud [read to the spreadsheet options](../options/gsheet.md) for the full list of parameters and environments variables for theses options.

## Examples

### Return the nodes info informations

```bash
sxcollector node-info 
```

#### Output


```bash
Name                                       Role              Status             Age                        Region                 Zone                 InstanceType               Internal IP  External IP  Processor               OS                                                            Kernel                        Kubelet           chassisID                             machineID                         systemUUID                            CapacityCpu               AllocatableCpu               CapacityThreads             AllocatableThreads             CapacityMemoryGB         AllocatableMemoryGB         CapacityEphemeralStorage            AllocatableEphemeralStorage            CapacityPods          AllocatablePods          
Node name                                  Role of the node  State of the node  since creation (in hours)  Failure domain region  Failure domain zone  Type of hardware instance  Private IP   Public IP    Processor architecture  OS name and version                                           Kernel version                Kubelet version   ID of the Baremetal chassis           ID of the machine                 ID of the system                      Number of cores capacity  Number of allocatable cores  Number of threads capacity  Number of allocatable threads  Memory capacity (in GB)  Memory allocatable (in GB)  ephemeral storage capacity (in GB)  ephemeral storage allocatable (in GB)  Nbr of pods capacity  Nbr of pods allocatable  
------------------------------------------ ----------------- ------------------ -------------------------- ---------------------- -------------------- -------------------------- ------------ ------------ ----------------------- ------------------------------------------------------------- ----------------------------- ----------------- ------------------------------------- --------------------------------- ------------------------------------- ------------------------- ---------------------------- --------------------------- ------------------------------ ------------------------ --------------------------- ----------------------------------- -------------------------------------- --------------------- ------------------------                                                                           
ip-1-1-1-1.eu-west-3.compute.internal      worker            Ready              103                        eu-west-3              eu-west-3a           t3a.2xlarge                10.18.14.83               amd64                   Red Hat Enterprise Linux CoreOS 415.92.202408020931-0 (Plow)  5.14.0-284.77.1.el9_2.x86_64  v1.28.11+add48d0  a83ad5ca-fa01-4e9f-a4e8-3b7b3d9f15b7  ec2caf31d7b442883639515dbf471378  ec2caf31-d7b4-4288-3639-515dbf471378  8                         7.00                         16                          16                             31.08                    29.99                       49.44                               43.49                                  250                   250                      
ip-1-1-1-2.eu-west-3.compute.internal      master            Ready              103                        eu-west-3              eu-west-3b           t3a.2xlarge                10.18.62.114              amd64                   Red Hat Enterprise Linux CoreOS 415.92.202408020931-0 (Plow)  5.14.0-284.77.1.el9_2.x86_64  v1.28.11+add48d0  95601a80-e2ae-4ab1-a6c2-acd658ff726a  ec24beea74d1c2d63e470ab85c2125b4  ec24beea-74d1-c2d6-3e47-0ab85c2125b4  8                         7.00                         16                          16                             31.08                    29.99                       49.44                               43.49                                  250                   250                      
ip-1-1-1-3.eu-west-3.compute.internal      worker            Ready              103                        eu-west-3              eu-west-3c           t3a.2xlarge                10.18.85.175              amd64                   Red Hat Enterprise Linux CoreOS 415.92.202408020931-0 (Plow)  5.14.0-284.77.1.el9_2.x86_64  v1.28.11+add48d0  3a36f02a-fc1b-4670-a376-03992144c6ef  ec2e1299dc3fbaff06bca301263166ea  ec2e1299-dc3f-baff-06bc-a301263166ea  8                         7.00                         16                          16                             31.08                    29.99                       49.44                               43.49                                  250                   250                      
```

### Return the nodes info informations in CSV

```bash
sxcollector node-info --output-format csv
```

#### Output


```csv
Name,Role,Status,Age,Region,Zone,InstanceType,Internal IP,External IP,Processor,OS,Kernel,Kubelet,chassisID,machineID,systemUUID,CapacityCpu,AllocatableCpu,CapacityThreads,AllocatableThreads,CapacityMemoryGB,AllocatableMemoryGB,CapacityEphemeralStorage,AllocatableEphemeralStorage,CapacityPods,AllocatablePods
Node name,Role of the node,State of the node,since creation (in hours),Failure domain region,Failure domain zone,Type of hardware instance,Private IP,Public IP,Processor architecture,OS name and version,Kernel version,Kubelet version,ID of the Baremetal chassis,ID of the machine,ID of the system,Number of cores capacity,Number of allocatable cores,Number of threads capacity,Number of allocatable threads,Memory capacity (in GB),Memory allocatable (in GB),ephemeral storage capacity (in GB),ephemeral storage allocatable (in GB),Nbr of pods capacity,Nbr of pods allocatable
ip-1-1-1-1.eu-west-3.compute.internal,master,Ready,70,eu-west-3,eu-west-3a,t3a.2xlarge,10.18.14.83,,amd64,Red Hat Enterprise Linux CoreOS 415.92.202408020931-0 (Plow),5.14.0-284.77.1.el9_2.x86_64,v1.28.11+add48d0,a83ad5ca-fa01-4e9f-a4e8-3b7b3d9f15b7,ec2caf31d7b442883639515dbf471378,ec2caf31-d7b4-4288-3639-515dbf471378,8,7.00,16,16,31.08,29.99,49.44,43.49,250,250
ip-1-1-1-2.eu-west-3.compute.internal,worker,Ready,70,eu-west-3,eu-west-3b,t3a.2xlarge,10.18.62.114,,amd64,Red Hat Enterprise Linux CoreOS 415.92.202408020931-0 (Plow),5.14.0-284.77.1.el9_2.x86_64,v1.28.11+add48d0,95601a80-e2ae-4ab1-a6c2-acd658ff726a,ec24beea74d1c2d63e470ab85c2125b4,ec24beea-74d1-c2d6-3e47-0ab85c2125b4,8,7.00,16,16,31.08,29.99,49.44,43.49,250,250
ip-1-1-1-3.eu-west-3.compute.internal,worker,Ready,70,eu-west-3,eu-west-3c,t3a.2xlarge,10.18.85.175,,amd64,Red Hat Enterprise Linux CoreOS 415.92.202408020931-0 (Plow),5.14.0-284.77.1.el9_2.x86_64,v1.28.11+add48d0,3a36f02a-fc1b-4670-a376-03992144c6ef,ec2e1299dc3fbaff06bca301263166ea,ec2e1299-dc3f-baff-06bc-a301263166ea,8,7.00,16,16,31.08,29.99,49.44,43.49,250,250
```
