# sxcollector node-ns-conso

This subcommand allow you to get the conso of all your nodes and namespaces within an Openshift cluster. 
You will get a result with the following content :

- **Node** : Node name
- **Namespace** : Namespace name
- **CPU** : CPU consumed (in core)
- **Memory** : Memory consumed (in GB)
- **Ephemeral** : Ephemeral consumed (in GB)
- **Persistent** : Persistent consumed (in GB)
- **Pods** : Number of pods
- **Containers** : Number of containers

All result will be sort by node name and multiple execution of this subcommand will return the same ordered list.

## Usage

```bash
sxcollector node-ns-conso [OPTIONS]...
```

### Generic options

All the generics options could be used for this subcommand. You shoud [read to the generics options](../options/generic.md) for the full list of parameters and environments variables for theses options.

###  Output options

All the formating options could be used for this subcommand. You shoud [read to the formating options](../options/output.md) for the full list of parameters and environments variables for theses options.

###  Mail options

All the mail options could be used for this subcommand. You shoud [read to the mails options](../options/mail.md) for the full list of parameters and environments variables for theses options.


### Confluence options

All the confluences options could be used for this subcommand. You shoud [read to the confluences options](../options/confluence.md) for the full list of parameters and environments variables for theses options.

### Google Spreasheet options

All the google spreasheet options could be used for this subcommand. You shoud [read to the spreadsheet options](../options/gsheet.md) for the full list of parameters and environments variables for theses options.
### Period options

All the periods options could be used for this subcommand. You shoud [read to the period options](../options/period.md) for the full list of parameters and environments variables for theses options.

## Examples

### Return the nodes conso informations

```bash
sxcollector node-ns-conso \
---duration 2d \
--from "2024-10-02 00:00:00"
```

#### Output


```bash
Name                                        CPU              MemoryUsage            EphemeralUsage                PersistentUsage                PodsMax        ContainersMax        
Node name                                   CPU/second used  MB of avg memory used  MB of ephemeral storage used  MB of persistent storage used  Max pods used  Max containers used  
------------------------------------------- ---------------- ---------------------- ----------------------------- ------------------------------ -------------- --------------------                                                                                              
ip-1-1-1-1.eu-west-3.compute.internal       385736           0                      1861                          0                              90             313                  
ip-1-1-1-2.eu-west-3.compute.internal       323096           0                      323                           0                              100            338                  
ip-1-1-1-3.eu-west-3.compute.internal       575099           0                      1947                          0                              85             291                  
```

### Return the nodes conso informations in CSV

```bash
sxcollector node-ns-conso \
---duration 2d \
--from "2024-10-02 00:00:00" \
--output-format csv 
```

#### Output


```csv
Node,CPU,Memory,Ephemeral,Persistent,Pods,Containers
Node name,CPU consumed (in core),Memory consumed (in GB),Ephemeral consumed (in GB),Persistent consumed (in GB),Pods count,Containers count
ip-1-1-1-1.eu-west-3.compute.internal,325828,0,314,0,100,338
ip-1-1-1-2.eu-west-3.compute.internal,611526,0,1957,0,87,292
ip-1-1-1-3.eu-west-3.compute.internal,365190,0,1848,0,89,309
```
