# sxcollector ns-info

This subcommand allow you to get a info range of information about your namespaces within a Kubernetes cluster. 
You will get a result with the following content :

- xxxx
- yyyy
- 
- **Namespace** : Namespace name
- **Created** : Date of creation
- **Age** : Age (in hours)
- **Status** : State of the namespace
- **podSecEnforce** : PSP at the namespace level
- **ocpMonitor** : Openshift monitor enabled
- **Description** : Openshift description
- **LongName** : Long namespace name
- **Requester** : the requester
- **NodeSelector** : Openshift NodeSelector
- **Component** : Component of the cluster
- **PartOf** : bellong to this app

All result will be sort by namespace and multiple execution of this subcommand will return the same ordered list.

## Usage

```bash
sxcollector ns-info [OPTIONS]...
```

### Generic options

All the generics options could be used for this subcommand. You shoud [read to the generics options](../options/generic.md) for the full list of parameters and environments variables for theses options.

###  Output options

All the formating options could be used for this subcommand. You shoud [read to the formating options](../options/output.md) for the full list of parameters and environments variables for theses options.

###  Mail options

All the mail options could be used for this subcommand. You shoud [read to the mails options](../options/mail.md) for the full list of parameters and environments variables for theses options.


### Confluence options

All the confluences options could be used for this subcommand. You shoud [read to the confluences options](../options/confluence.md) for the full list of parameters and environments variables for theses options.

### Google Spreasheet options

All the google spreasheet options could be used for this subcommand. You shoud [read to the spreadsheet options](../options/gsheet.md) for the full list of parameters and environments variables for theses options.

## Examples

### Return the namespaces info informations

```bash
sxcollector ns-info 
```

#### Output


```bash
Namespace                                         Created                         Age             Status                  podSecEnforce               ocpMonitor                 Description                                                                                                                                   LongName             Requester      NodeSelector            Component                 PartOf               
Namespace name                                    Date of creation                Age (in hours)  State of the namespace  PSP at the namespace level  Openshift monitor enabled  Openshift description                                                                                                                         Long namespace name  the requester  Openshift NodeSelector  Component of the cluster  bellong to this app  
------------------------------------------------- ------------------------------- --------------- ----------------------- --------------------------- -------------------------- --------------------------------------------------------------------------------------------------------------------------------------------- -------------------- -------------- ----------------------- ------------------------- --------------------                                                                                         
aaaaaaa                                           2024-10-02 02:16:30 +0200 CEST  88              Active                                                                                                                                                                                                                                            kube:admin                                                                            
bbbbb                                             2024-10-02 02:17:03 +0200 CEST  88              Active                                                                                                                                                                                                                                            kube:admin                                                                            
default                                           2024-10-01 11:23:25 +0200 CEST  103             Active                  privileged                                                                                                                                                                                                                                                                                                      
kube-node-lease                                   2024-10-01 11:23:25 +0200 CEST  103             Active                                                                                                                                                                                                                                                                                                                                  
kube-public                                       2024-10-01 11:23:25 +0200 CEST  103             Active                  privileged                                                                                                                                                                                                                                                                                                      
kube-system                                       2024-10-01 11:23:25 +0200 CEST  103             Active                  privileged                                                                                                                                                                                                                                                                                                      
openshift                                         2024-10-01 11:47:23 +0200 CEST  102             Active                                                                                                                                                                                                                                                                                                                                  
openshift-apiserver                               2024-10-01 11:37:39 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-apiserver-operator                      2024-10-01 11:24:18 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-authentication                          2024-10-01 11:37:44 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-authentication-operator                 2024-10-01 11:24:17 +0200 CEST  103             Active                  baseline                    true                                                                                                                                                                                                                                                                                
openshift-cloud-controller-manager                2024-10-01 11:24:21 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-cloud-controller-manager-operator       2024-10-01 11:24:19 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-cloud-credential-operator               2024-10-01 11:24:04 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-cloud-network-config-controller         2024-10-01 11:24:17 +0200 CEST  103             Active                                              true                       OpenShift cloud network config controller namespace - a controller used to manage cloud-level network configuration                                                                                                                                      
openshift-cloud-platform-infra                    2024-10-01 11:24:30 +0200 CEST  103             Active                  privileged                                                                                                                                                                                                                                                                                                      
openshift-cluster-csi-drivers                     2024-10-01 11:24:18 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-cluster-machine-approver                2024-10-01 11:24:17 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-cluster-node-tuning-operator            2024-10-01 11:24:18 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-cluster-samples-operator                2024-10-01 11:24:21 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-cluster-storage-operator                2024-10-01 11:24:16 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-cluster-version                         2024-10-01 11:23:25 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-config                                  2024-10-01 11:24:41 +0200 CEST  103             Active                  privileged                                                                                                                                                                                                                                                                                                      
openshift-config-managed                          2024-10-01 11:24:41 +0200 CEST  103             Active                                                                                                                                                                                                                                                                                                                                  
openshift-config-operator                         2024-10-01 11:24:16 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-console                                 2024-10-01 11:49:24 +0200 CEST  102             Active                                              true                                                                                                                                                                                                                                                                                
openshift-console-operator                        2024-10-01 11:49:25 +0200 CEST  102             Active                                              true                                                                                                                                                                                                                                                                                
openshift-console-user-settings                   2024-10-01 11:49:27 +0200 CEST  102             Active                                                                                                                                                                                                                                                                                                                                  
openshift-controller-manager                      2024-10-01 11:37:36 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-controller-manager-operator             2024-10-01 11:24:20 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-dns                                     2024-10-01 11:38:23 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-dns-operator                            2024-10-01 11:24:21 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-etcd                                    2024-10-01 11:23:54 +0200 CEST  103             Active                  privileged                                                                                                                                                                                                                                                                                                      
openshift-etcd-operator                           2024-10-01 11:24:19 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-host-network                            2024-10-01 11:35:01 +0200 CEST  103             Active                                                                         Namespace for enabling network policy specification for host network traffic. Can be used to allow access to or from host network components                                                                                                             
openshift-image-registry                          2024-10-01 11:24:21 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-infra                                   2024-10-01 11:23:35 +0200 CEST  103             Active                  privileged                                                                                                                                                                                                                                                                                                      
openshift-ingress                                 2024-10-01 11:53:00 +0200 CEST  102             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-ingress-canary                          2024-10-01 11:52:55 +0200 CEST  102             Active                                                                                                                                                                                                                                                                                                                                  
openshift-ingress-operator                        2024-10-01 11:24:07 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-insights                                2024-10-01 11:24:20 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-kni-infra                               2024-10-01 11:24:24 +0200 CEST  103             Active                  privileged                                                                                                                                                                                                                                                                                                      
openshift-kube-apiserver                          2024-10-01 11:23:54 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-kube-apiserver-operator                 2024-10-01 11:23:55 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-kube-controller-manager                 2024-10-01 11:23:55 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-kube-controller-manager-operator        2024-10-01 11:23:55 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-kube-scheduler                          2024-10-01 11:23:56 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-kube-scheduler-operator                 2024-10-01 11:24:18 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-kube-storage-version-migrator           2024-10-01 11:37:36 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-kube-storage-version-migrator-operator  2024-10-01 11:24:22 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-machine-api                             2024-10-01 11:24:41 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-machine-config-operator                 2024-10-01 11:24:21 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-marketplace                             2024-10-01 11:24:20 +0200 CEST  103             Active                  baseline                    true                                                                                                                                                                                                                                                                                
openshift-monitoring                              2024-10-01 11:24:35 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-multus                                  2024-10-01 11:34:31 +0200 CEST  103             Active                  privileged                  true                       Multus network plugin components                                                                                                                                                                                                                         
openshift-network-diagnostics                     2024-10-01 11:35:03 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-network-node-identity                   2024-10-01 11:35:09 +0200 CEST  103             Active                  privileged                  true                       OpenShift network node identity namespace - a controller used to manage node identity components                                                                                                                                                         
openshift-network-operator                        2024-10-01 11:24:19 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-node                                    2024-10-01 11:47:25 +0200 CEST  102             Active                                                                                                                                                                                                                                                                                                                                  
openshift-nutanix-infra                           2024-10-01 11:24:28 +0200 CEST  103             Active                  privileged                                                                                                                                                                                                                                                                                                      
openshift-oauth-apiserver                         2024-10-01 11:37:42 +0200 CEST  103             Active                  privileged                  true                                                                                                                                                                                                                                                                                
openshift-openstack-infra                         2024-10-01 11:24:22 +0200 CEST  103             Active                  privileged                                                                                                                                                                                                                                                                                                      
openshift-operator-lifecycle-manager              2024-10-01 11:24:26 +0200 CEST  103             Active                  restricted                  true                                                                                                                                                                                                                                                                                
openshift-operators                               2024-10-01 11:24:27 +0200 CEST  103             Active                  privileged                                                                                                                                                                                                                                                                                                      
openshift-ovirt-infra                             2024-10-01 11:24:26 +0200 CEST  103             Active                  privileged                                                                                                                                                                                                                                                                                                      
openshift-ovn-kubernetes                          2024-10-01 11:34:46 +0200 CEST  103             Active                  privileged                  true                       OVN Kubernetes components                                                                                                                                                                                                                                
openshift-route-controller-manager                2024-10-01 11:37:36 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-service-ca                              2024-10-01 11:37:35 +0200 CEST  103             Active                                                                                                                                                                                                                                                                                                                                  
openshift-service-ca-operator                     2024-10-01 11:24:21 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-user-workload-monitoring                2024-10-01 11:24:36 +0200 CEST  103             Active                                              true                                                                                                                                                                                                                                                                                
openshift-vsphere-infra                           2024-10-01 11:24:27 +0200 CEST  103             Active                  privileged                                                                                                                                                                                                                                                                                                      
```

### Return the namespaces info informations in CSV

```bash
sxcollector ns-info --output-format csv
```

#### Output


```csv
Namespace,Created,Age,Status,podSecEnforce,ocpMonitor,Description,LongName,Requester,NodeSelector,Component,PartOf
Namespace name,Date of creation,Age (in hours),State of the namespace,PSP at the namespace level,Openshift monitor enabled,Openshift description,Long namespace name,the requester,Openshift NodeSelector,Component of the cluster,bellong to this app
aaaaaaa,2024-10-02 02:16:30 +0200 CEST,55,Active,,,,,kube:admin,,,
bbbbb,2024-10-02 02:17:03 +0200 CEST,55,Active,,,,,kube:admin,,,
default,2024-10-01 11:23:25 +0200 CEST,70,Active,privileged,,,,,,,
kube-node-lease,2024-10-01 11:23:25 +0200 CEST,70,Active,,,,,,,,
kube-public,2024-10-01 11:23:25 +0200 CEST,70,Active,privileged,,,,,,,
kube-system,2024-10-01 11:23:25 +0200 CEST,70,Active,privileged,,,,,,,
openshift,2024-10-01 11:47:23 +0200 CEST,69,Active,,,,,,,,
openshift-apiserver,2024-10-01 11:37:39 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-apiserver-operator,2024-10-01 11:24:18 +0200 CEST,70,Active,,true,,,,,,
openshift-authentication,2024-10-01 11:37:44 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-authentication-operator,2024-10-01 11:24:17 +0200 CEST,70,Active,baseline,true,,,,,,
openshift-cloud-controller-manager,2024-10-01 11:24:21 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-cloud-controller-manager-operator,2024-10-01 11:24:19 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-cloud-credential-operator,2024-10-01 11:24:04 +0200 CEST,70,Active,,true,,,,,,
openshift-cloud-network-config-controller,2024-10-01 11:24:17 +0200 CEST,70,Active,,true,OpenShift cloud network config controller namespace - a controller used to manage cloud-level network configuration,,,,,
openshift-cloud-platform-infra,2024-10-01 11:24:30 +0200 CEST,70,Active,privileged,,,,,,,
openshift-cluster-csi-drivers,2024-10-01 11:24:18 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-cluster-machine-approver,2024-10-01 11:24:17 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-cluster-node-tuning-operator,2024-10-01 11:24:18 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-cluster-samples-operator,2024-10-01 11:24:21 +0200 CEST,70,Active,,true,,,,,,
openshift-cluster-storage-operator,2024-10-01 11:24:16 +0200 CEST,70,Active,,true,,,,,,
openshift-cluster-version,2024-10-01 11:23:25 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-config,2024-10-01 11:24:41 +0200 CEST,70,Active,privileged,,,,,,,
openshift-config-managed,2024-10-01 11:24:41 +0200 CEST,70,Active,,,,,,,,
openshift-config-operator,2024-10-01 11:24:16 +0200 CEST,70,Active,,true,,,,,,
openshift-console,2024-10-01 11:49:24 +0200 CEST,69,Active,,true,,,,,,
openshift-console-operator,2024-10-01 11:49:25 +0200 CEST,69,Active,,true,,,,,,
openshift-console-user-settings,2024-10-01 11:49:27 +0200 CEST,69,Active,,,,,,,,
openshift-controller-manager,2024-10-01 11:37:36 +0200 CEST,70,Active,,true,,,,,,
openshift-controller-manager-operator,2024-10-01 11:24:20 +0200 CEST,70,Active,,true,,,,,,
openshift-dns,2024-10-01 11:38:23 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-dns-operator,2024-10-01 11:24:21 +0200 CEST,70,Active,,true,,,,,,
openshift-etcd,2024-10-01 11:23:54 +0200 CEST,70,Active,privileged,,,,,,,
openshift-etcd-operator,2024-10-01 11:24:19 +0200 CEST,70,Active,,true,,,,,,
openshift-host-network,2024-10-01 11:35:01 +0200 CEST,70,Active,,,Namespace for enabling network policy specification for host network traffic. Can be used to allow access to or from host network components,,,,,
openshift-image-registry,2024-10-01 11:24:21 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-infra,2024-10-01 11:23:35 +0200 CEST,70,Active,privileged,,,,,,,
openshift-ingress,2024-10-01 11:53:00 +0200 CEST,69,Active,privileged,true,,,,,,
openshift-ingress-canary,2024-10-01 11:52:55 +0200 CEST,69,Active,,,,,,,,
openshift-ingress-operator,2024-10-01 11:24:07 +0200 CEST,70,Active,,true,,,,,,
openshift-insights,2024-10-01 11:24:20 +0200 CEST,70,Active,,true,,,,,,
openshift-kni-infra,2024-10-01 11:24:24 +0200 CEST,70,Active,privileged,,,,,,,
openshift-kube-apiserver,2024-10-01 11:23:54 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-kube-apiserver-operator,2024-10-01 11:23:55 +0200 CEST,70,Active,,true,,,,,,
openshift-kube-controller-manager,2024-10-01 11:23:55 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-kube-controller-manager-operator,2024-10-01 11:23:55 +0200 CEST,70,Active,,true,,,,,,
openshift-kube-scheduler,2024-10-01 11:23:56 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-kube-scheduler-operator,2024-10-01 11:24:18 +0200 CEST,70,Active,,true,,,,,,
openshift-kube-storage-version-migrator,2024-10-01 11:37:36 +0200 CEST,70,Active,,true,,,,,,
openshift-kube-storage-version-migrator-operator,2024-10-01 11:24:22 +0200 CEST,70,Active,,true,,,,,,
openshift-machine-api,2024-10-01 11:24:41 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-machine-config-operator,2024-10-01 11:24:21 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-marketplace,2024-10-01 11:24:20 +0200 CEST,70,Active,baseline,true,,,,,,
openshift-monitoring,2024-10-01 11:24:35 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-multus,2024-10-01 11:34:31 +0200 CEST,70,Active,privileged,true,Multus network plugin components,,,,,
openshift-network-diagnostics,2024-10-01 11:35:03 +0200 CEST,70,Active,,true,,,,,,
openshift-network-node-identity,2024-10-01 11:35:09 +0200 CEST,70,Active,privileged,true,OpenShift network node identity namespace - a controller used to manage node identity components,,,,,
openshift-network-operator,2024-10-01 11:24:19 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-node,2024-10-01 11:47:25 +0200 CEST,69,Active,,,,,,,,
openshift-nutanix-infra,2024-10-01 11:24:28 +0200 CEST,70,Active,privileged,,,,,,,
openshift-oauth-apiserver,2024-10-01 11:37:42 +0200 CEST,70,Active,privileged,true,,,,,,
openshift-openstack-infra,2024-10-01 11:24:22 +0200 CEST,70,Active,privileged,,,,,,,
openshift-operator-lifecycle-manager,2024-10-01 11:24:26 +0200 CEST,70,Active,restricted,true,,,,,,
openshift-operators,2024-10-01 11:24:27 +0200 CEST,70,Active,privileged,,,,,,,
openshift-ovirt-infra,2024-10-01 11:24:26 +0200 CEST,70,Active,privileged,,,,,,,
openshift-ovn-kubernetes,2024-10-01 11:34:46 +0200 CEST,70,Active,privileged,true,OVN Kubernetes components,,,,,
openshift-route-controller-manager,2024-10-01 11:37:36 +0200 CEST,70,Active,,true,,,,,,
openshift-service-ca,2024-10-01 11:37:35 +0200 CEST,70,Active,,,,,,,,
openshift-service-ca-operator,2024-10-01 11:24:21 +0200 CEST,70,Active,,true,,,,,,
openshift-user-workload-monitoring,2024-10-01 11:24:36 +0200 CEST,70,Active,,true,,,,,,
openshift-vsphere-infra,2024-10-01 11:24:27 +0200 CEST,70,Active,privileged,,,,,,,
```
