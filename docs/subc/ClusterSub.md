# sxcollector cluster-sub

This subcommand allow you to get a sub range of information about your clusters within an Openshift cluster. 
You will get a result with the following content :

- **Namespace** : Name of the namespace
- **Subscription** : Subscription name
- **Channel** : Channel name
- **Source** : Source name
- **PlanApproval** : Kind of install plan
- **startingCSV** : starting CSV version
- **installedCSV** : installed CSV version
- **currentCSV** : current CSV version

All result will be sort by namespace and multiple execution of this subcommand will return the same ordered list.

## Usage

```bash
sxcollector cluster-sub [OPTIONS]...
```

### Generic options

All the generics options could be used for this subcommand. You shoud [read to the generics options](../options/generic.md) for the full list of parameters and environments variables for theses options.

###  Output options

All the formating options could be used for this subcommand. You shoud [read to the formating options](../options/output.md) for the full list of parameters and environments variables for theses options.

###  Mail options

All the mail options could be used for this subcommand. You shoud [read to the mails options](../options/mail.md) for the full list of parameters and environments variables for theses options.


### Confluence options

All the confluences options could be used for this subcommand. You shoud [read to the confluences options](../options/confluence.md) for the full list of parameters and environments variables for theses options.

### Google Spreasheet options

All the google spreasheet options could be used for this subcommand. You shoud [read to the spreadsheet options](../options/gsheet.md) for the full list of parameters and environments variables for theses options.

## Examples

### Return the clusters sub informations

```bash
sxcollector cluster-sub 
```

#### Output


```bash
Subscription                              Namespace              Channel      Source              PlanApproval          startingCSV           installedCSV           currentCSV           
Subscription name                         Name of the namespace  Channel name Source name         Kind of install plan  starting CSV version  installed CSV version  current CSV version  
----------------------------------------- ---------------------- ------------ ------------------- --------------------- --------------------- ---------------------- --------------------                                                                                             
crunchy-postgres-operator-rhmp            aaaaaaa                v5           redhat-marketplace  Automatic             v5.6.1                v5.6.1                 v5.6.1               
crunchy-postgres-operator                 bbbbb                  v5           certified-operators Manual                v5.4.3                v5.4.3                 v5.4.4               
postgresql                                default                v5           community-operators Automatic             v5.6.1                v5.6.1                 v5.6.1               
container-security-operator               openshift-operators    stable-3.12  redhat-operators    Automatic             v3.12.2               v3.12.2                v3.12.3              
rabbitmq-single-active-consumer-operator  openshift-operators    stable       community-operators Manual                v0.2.2                v0.2.2                 v0.2.2               
```

### Return the clusters sub informations in CSV

```bash
sxcollector cluster-sub --output-format csv
```

#### Output


```csv
Subscription,Namespace,Channel,Source,PlanApproval,startingCSV,installedCSV,currentCSV
Subscription name,Name of the namespace,Channel name,Source name,Kind of install plan,starting CSV version,installed CSV version,current CSV version
crunchy-postgres-operator-rhmp,aaaaaaa,v5,redhat-marketplace,Automatic,v5.6.1,v5.6.1,v5.6.1
crunchy-postgres-operator,bbbbb,v5,certified-operators,Manual,v5.4.3,v5.4.3,v5.4.4
postgresql,default,v5,community-operators,Automatic,v5.6.1,v5.6.1,v5.6.1
container-security-operator,openshift-operators,stable-3.12,redhat-operators,Automatic,v3.12.2,v3.12.2,v3.12.3
rabbitmq-single-active-consumer-operator,openshift-operators,stable,community-operators,Manual,v0.2.2,v0.2.2,v0.2.2
```
