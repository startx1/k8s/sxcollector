# Using sxcollector

## Sub-command list

- [**cluster-sub** : Get the full list of subscriptions (Openshift)](ClusterSub.md)
- [**node-state** : Get the state of the cluster nodes (Kubernetes)](NodeState.md)
- [**node-info** : Get the detailled info of the cluster nodes (Kubernetes)](NodeInfo.md)
- [**node-conso** : Get the detailled consumptions of the cluster nodes (Openshift)](NodeConso.md)
- [**node-ns-conso** : Get the detailled consumptions of the cluster nodes and namespaces (Openshift)](NodeNsConso.md)
- [**node-billing** : Get the detailled informations for the billing of the cluster (Openshift)](NodeBilling.md)
- [**ns-state** : Get the state of the cluster namespaces (Kubernetes)](NsState.md)
- [**ns-info** : Get the detailled info of the cluster namespaces (Kubernetes)](NsInfo.md)
- [**ns-conso** : Get the detailled consumptions of the cluster namespaces (Openshift)](NsConso.md)
- [**ns-deep** : Get the detailled consumptions of kinds for each cluster namespaces (Kubernetes)](NsDeep.md)

## Arguments

- [**generic** : Common arguments](../options/generic.md)
- [**output** : Formatting arguments](../options/output.md)
- [**period** : Period arguments (if metrics)](../options/period.md)
- [**mail** : Mail arguments](../options/mail.md)
- [**confluence** : Confluence arguments](../options/confluence.md)
