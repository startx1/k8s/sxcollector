# sxcollector node-state

This subcommand allow you to get the state of all you nodes within a Kubernetes cluster. 
You will get a result with the following content :

- **Name** : Node name
- **Role** : Role of the node
- **Status** : State of the node
- **Age** : since creation (in hours)
- **Region** : Failure domain region
- **Zone** : Failure domain zone
- **InstanceType** : Type of hardware instance
- **AllocatablePods** : Nbr of pods allocatable
- **DeclaredPods** : Nbr of pods declared
- **RunningPods** : Nbr of pods running
- **DeclaredContainers** : Nbr of containers declared
- **RunningContainers** : Nbr of containers running
- **AllocatableMemoryGB** : Memory allocable (in GB)
- **RequestMemoryGB** : Memory requested (in GB)
- **LimitsMemoryGB** : Memory limits (in GB)
- **UsedMemoryGB** : Memory used (in GB)
- **AllocatableCpu** : CPU allocatable (in milicore)
- **RequestCpu** : CPU requested (in milicore)
- **LimitsCpu** : CPU limits (in milicore)
- **usedCPU** : CPU used (in milicore)
- **AllocatableEphemeralStorage** : ephemeral storage allocatable (in GB)
- **RequestEphemeralStorage** : ephemeral storage requested (in GB)
- **LimitsEphemeralStorage** : ephemeral storage limits (in GB)
- **usedEphemeralStorage** : ephemeral storage used (in GB)

All result will be sort by node name and multiple execution of this subcommand will return the same ordered list.

## Usage

```bash
sxcollector node-state [OPTIONS]...
```

### Generic options

All the generics options could be used for this subcommand. You shoud [read to the generics options](../options/generic.md) for the full list of parameters and environments variables for theses options.

###  Output options

All the formating options could be used for this subcommand. You shoud [read to the formating options](../options/output.md) for the full list of parameters and environments variables for theses options.

###  Mail options

All the mail options could be used for this subcommand. You shoud [read to the mails options](../options/mail.md) for the full list of parameters and environments variables for theses options.


### Confluence options

All the confluences options could be used for this subcommand. You shoud [read to the confluences options](../options/confluence.md) for the full list of parameters and environments variables for theses options.

### Google Spreasheet options

All the google spreasheet options could be used for this subcommand. You shoud [read to the spreadsheet options](../options/gsheet.md) for the full list of parameters and environments variables for theses options.
### Period options

All the periods options could be used for this subcommand. You shoud [read to the period options](../options/period.md) for the full list of parameters and environments variables for theses options.

## Examples

### Return the nodes states

```bash
sxcollector node-state \
---duration 2d \
--from "2024-10-02 00:00:00" 
```

#### Output


```bash
Name                                       Role              Status             Age                        Region                 Zone                 InstanceType               AllocatablePods          DeclaredPods          RunningPods          DeclaredContainers          RunningContainers          AllocatableMemoryGB       RequestMemoryGB           LimitsMemoryGB         UsedMemoryGB         AllocatableCpu                 RequestCpu                   LimitsCpu                 usedCPU                 AllocatableEphemeralStorage            RequestEphemeralStorage              LimitsEphemeralStorage            usedEphemeralStorage            
Node name                                  Role of the node  State of the node  since creation (in hours)  Failure domain region  Failure domain zone  Type of hardware instance  Nbr of pods allocatable  Nbr of pods declared  Nbr of pods running  Nbr of containers declared  Nbr of containers running  Memory allocable (in GB)  Memory requested (in GB)  Memory limits (in GB)  Memory used (in GB)  CPU allocatable (in milicore)  CPU requested (in milicore)  CPU limits (in milicore)  CPU used (in milicore)  ephemeral storage allocatable (in GB)  ephemeral storage requested (in GB)  ephemeral storage limits (in GB)  ephemeral storage used (in GB)  
------------------------------------------ ----------------- ------------------ -------------------------- ---------------------- -------------------- -------------------------- ------------------------ --------------------- -------------------- --------------------------- -------------------------- ------------------------- ------------------------- ---------------------- -------------------- ------------------------------ ---------------------------- ------------------------- ----------------------- -------------------------------------- ------------------------------------ --------------------------------- -------------------------------                                                                             
ip-1-1-1-1.eu-west-3.compute.internal      worker            Ready              103                        eu-west-3              eu-west-3a           t3a.2xlarge                250                      100                   0                    147                         100                        29.99                     14.11                     5.22                   4.82                 7.50                           6.00                         4.20                      323095.67               43.49                                  0.00                                 0.00                              0.00                            
ip-1-1-1-2.eu-west-3.compute.internal      worker            Ready              103                        eu-west-3              eu-west-3b           t3a.2xlarge                250                      88                    0                    139                         85                         29.99                     15.15                     5.22                   5.53                 7.50                           6.00                         4.20                      575098.80               43.49                                  0.00                                 0.00                              0.00                            
ip-1-1-1-3.eu-west-3.compute.internal      master            Ready              103                        eu-west-3              eu-west-3c           t3a.2xlarge                250                      88                    0                    152                         90                         29.99                     15.48                     6.08                   6.30                 7.50                           7.00                         4.80                      385735.55               43.49                                  0.00                                 0.00                              0.00                            
```


### Return the nodes states in CSV

```bash
sxcollector node-state \
---duration 2d \
--from "2024-10-02 00:00:00" \
--output-format csv  
```

#### Output


```csv
Name,Role,Status,Age,Region,Zone,InstanceType,AllocatablePods,DeclaredPods,RunningPods,DeclaredContainers,RunningContainers,AllocatableMemoryGB,RequestMemoryGB,LimitsMemoryGB,UsedMemoryGB,AllocatableCpu,RequestCpu,LimitsCpu,usedCPU,AllocatableEphemeralStorage,RequestEphemeralStorage,LimitsEphemeralStorage,usedEphemeralStorage
Node name,Role of the node,State of the node,since creation (in hours),Failure domain region,Failure domain zone,Type of hardware instance,Nbr of pods allocatable,Nbr of pods declared,Nbr of pods running,Nbr of containers declared,Nbr of containers running,Memory allocable (in GB),Memory requested (in GB),Memory limits (in GB),Memory used (in GB),CPU allocatable (in milicore),CPU requested (in milicore),CPU limits (in milicore),CPU used (in milicore),ephemeral storage allocatable (in GB),ephemeral storage requested (in GB),ephemeral storage limits (in GB),ephemeral storage used (in GB)
ip-1-1-1-1.eu-west-3.compute.internal,control-plane,Ready,70,eu-west-3,eu-west-3a,t3a.2xlarge,250,100,0,147,100,29.99,14.11,5.22,4.82,7.50,6.00,4.20,325849.15,43.49,0.00,0.00,0.00
ip-1-1-1-2.eu-west-3.compute.internal,worker,Ready,70,eu-west-3,eu-west-3b,t3a.2xlarge,250,87,0,138,87,29.99,15.11,5.22,5.77,7.50,6.00,4.20,611901.28,43.49,0.00,0.00,0.00
ip-1-1-1-3.eu-west-3.compute.internal,control-plane,Ready,70,eu-west-3,eu-west-3c,t3a.2xlarge,250,89,0,153,89,29.99,15.52,6.08,6.30,7.50,7.00,4.80,365217.33,43.49,0.00,0.00,0.00
```
