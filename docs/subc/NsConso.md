# sxcollector ns-conso

This subcommand allow you to get the conso of all your namespaces within an Openshift cluster. 
You will get a result with the following content :

- **Namespace** : Name of the namespace
- **CPU** : CPU/second used
- **MemoryUsage** : MB of avg memory used
- **EphemeralUsage** : MB of ephemeral storage used
- **PersistentUsage** : MB of persistent storage used
- **PodsMax** : Max pods 
- **ContainersMax** : Max containers 

All result will be sort by namespace and multiple execution of this subcommand will return the same ordered list.

## Usage

```bash
sxcollector ns-conso [OPTIONS]...
```

### Generic options

All the generics options could be used for this subcommand. You shoud [read to the generics options](../options/generic.md) for the full list of parameters and environments variables for theses options.

###  Output options

All the formating options could be used for this subcommand. You shoud [read to the formating options](../options/output.md) for the full list of parameters and environments variables for theses options.

###  Mail options

All the mail options could be used for this subcommand. You shoud [read to the mails options](../options/mail.md) for the full list of parameters and environments variables for theses options.


### Confluence options

All the confluences options could be used for this subcommand. You shoud [read to the confluences options](../options/confluence.md) for the full list of parameters and environments variables for theses options.

### Google Spreasheet options

All the google spreasheet options could be used for this subcommand. You shoud [read to the spreadsheet options](../options/gsheet.md) for the full list of parameters and environments variables for theses options.
### Period options

All the periods options could be used for this subcommand. You shoud [read to the period options](../options/period.md) for the full list of parameters and environments variables for theses options.

## Examples

### Return the namespaces conso informations

```bash
sxcollector ns-conso \
---duration 2d \
--from "2024-10-02 00:00:00" 
```

#### Output


```bash
Namespace                                         CPU              MemoryUsage            EphemeralUsage                PersistentUsage                PodsMax        ContainersMax        
Namespace name                                    CPU/second used  MB of avg memory used  MB of ephemeral storage used  MB of persistent storage used  Max pods used  Max containers used  
------------------------------------------------- ---------------- ---------------------- ----------------------------- ------------------------------ -------------- -------------------- --                                                                                             
openshift-cluster-storage-operator                2842             88                     0                             0                              6              12                   0  
openshift-machine-config-operator                 10037            281                    1                             0                              11             27                   0  
openshift-cluster-csi-drivers                     4339             326                    0                             0                              6              38                   0  
openshift-ovn-kubernetes                          22311            252                    75                            0                              5              33                   0  
openshift-service-ca-operator                     1049             68                     0                             0                              1              2                    0  
openshift-cloud-network-config-controller         90               32                     0                             0                              1              2                    0  
openshift-controller-manager                      3824             191                    0                             0                              3              6                    0  
openshift-network-operator                        2763             196                    30                            0                              1              2                    0  
openshift-kube-storage-version-migrator           53               20                     0                             0                              1              2                    0  
openshift-monitoring                              257880           6333                   44                            0                              20             80                   0  
openshift-cloud-credential-operator               863              93                     1                             0                              3              7                    0  
openshift-multus                                  1747             322                    8                             0                              11             27                   0  
openshift-console                                 1927             1776                   3453                          0                              4              8                    0  
openshift-authentication-operator                 7534             171                    2                             0                              1              2                    0  
openshift-operator-lifecycle-manager              52096            463                    35                            0                              8              11                   0  
openshift-route-controller-manager                3122             122                    0                             0                              3              6                    0  
openshift-kube-apiserver-operator                 5285             208                    4                             0                              1              2                    0  
aaaaaaa                                           230              25                     0                             0                              1              2                    0  
openshift-network-diagnostics                     1070             101                    0                             0                              4              8                    0  
openshift-cluster-samples-operator                1277             88                     9                             0                              1              4                    0  
openshift-kube-controller-manager-operator        2975             105                    2                             0                              1              2                    0  
openshift-apiserver-operator                      2858             160                    1                             0                              1              2                    0  
openshift-authentication                          3748             74                     1                             0                              3              6                    0  
openshift-controller-manager-operator             2244             160                    1                             0                              1              2                    0  
openshift-kube-apiserver                          367126           4938                   91                            0                              44             24                   0  
openshift-config-operator                         2365             58                     0                             0                              1              2                    0  
openshift-dns-operator                            334              64                     0                             0                              1              3                    0  
openshift-machine-api                             2979             312                    20                            0                              5              19                   0  
openshift-cluster-version                         5165             211                    58                            0                              1              2                    0  
openshift-kube-storage-version-migrator-operator  743              44                     0                             0                              1              2                    0  
openshift-cluster-node-tuning-operator            1134             63                     0                             0                              4              8                    0  
openshift-kube-controller-manager                 20816            550                    6                             0                              22             21                   0  
openshift-image-registry                          2928             96                     174                           0                              8              12                   0  
default                                           227              28                     0                             0                              1              2                    0  
openshift-apiserver                               28832            469                    5                             0                              3              9                    0  
openshift-service-ca                              1003             200                    0                             0                              1              2                    0  
openshift-ingress                                 3261             106                    0                             0                              2              4                    0  
openshift-kube-scheduler                          7109             169                    2                             0                              22             18                   0  
openshift-etcd-operator                           5692             140                    2                             0                              1              2                    0  
openshift-console-operator                        3110             184                    0                             0                              1              3                    0  
openshift-cluster-machine-approver                346              117                    0                             0                              1              3                    0  
openshift-etcd                                    183729           1811                   65                            0                              22             21                   0  
openshift-ingress-operator                        1091             118                    0                             0                              1              3                    0  
openshift-oauth-apiserver                         16549            138                    3                             0                              3              6                    0  
bbbbb                                             194              29                     0                             0                              1              2                    0  
openshift-cloud-controller-manager                830              45                     0                             0                              2              4                    0  
openshift-network-node-identity                   440              67                     5                             0                              3              9                    0  
openshift-dns                                     3316             73                     0                             0                              6              15                   0  
openshift-insights                                839              89                     10                            0                              1              2                    0  
openshift-ingress-canary                          104              47                     0                             0                              3              6                    0  
openshift-marketplace                             224285           1338                   0                             0                              12             10                   0  
openshift-kube-scheduler-operator                 1963             80                     1                             0                              1              2                    0  
openshift-cloud-controller-manager-operator       574              142                    0                             0                              1              4                    0  
openshift-operators                               4683             164                    48                            0                              2              3                    0  
```


### Return the namespaces conso informations in CSV

```bash
sxcollector ns-conso \
---duration 2d \
--from "2024-10-02 00:00:00" \
--output-format csv 
```

#### Output


```csv
Namespace,CPU,MemoryUsage,EphemeralUsage,PersistentUsage,PodsMax,ContainersMax
Namespace name,CPU/second used,MB of avg memory used,MB of ephemeral storage used,MB of persistent storage used,Max pods used,Max containers used
openshift-insights,842,89,11,0,1,2,0
openshift-operators,4714,182,77,0,2,3,0
openshift-machine-config-operator,9993,280,1,0,11,27,0
openshift-controller-manager-operator,2134,160,0,0,1,2,0
openshift-cloud-controller-manager-operator,586,142,0,0,1,4,0
openshift-authentication-operator,7390,171,2,0,1,2,0
openshift-apiserver,28954,480,5,0,3,9,0
openshift-cluster-node-tuning-operator,1136,62,0,0,4,8,0
openshift-etcd,184772,1811,40,0,22,21,0
openshift-service-ca,972,178,0,0,1,2,0
openshift-cluster-machine-approver,345,106,0,0,1,3,0
openshift-network-diagnostics,1071,95,0,0,4,8,0
openshift-service-ca-operator,1053,68,0,0,1,2,0
openshift-ingress-operator,1094,118,0,0,1,3,0
openshift-ingress,3071,106,0,0,2,4,0
openshift-network-operator,2718,204,20,0,1,2,0
openshift-apiserver-operator,2881,159,0,0,1,2,0
openshift-cloud-network-config-controller,90,32,0,0,1,2,0
openshift-image-registry,2929,103,7,0,9,12,0
openshift-marketplace,227441,1354,0,0,12,10,0
openshift-kube-scheduler-operator,1951,80,0,0,1,2,0
openshift-ingress-canary,105,51,0,0,3,6,0
openshift-cluster-storage-operator,2850,88,0,0,6,12,0
openshift-kube-apiserver,372383,4938,141,0,44,24,0
openshift-route-controller-manager,3142,89,0,0,3,6,0
bbbbb,195,29,0,0,1,2,0
openshift-kube-controller-manager,20936,557,8,0,22,21,0
openshift-monitoring,269273,6333,52,0,20,80,0
openshift-dns,3317,74,0,0,6,15,0
openshift-controller-manager,3800,191,0,0,3,6,0
openshift-cloud-controller-manager,824,45,0,0,2,4,0
openshift-etcd-operator,5696,140,2,0,1,2,0
openshift-multus,1740,322,9,0,11,27,0
openshift-dns-operator,335,64,0,0,1,3,0
openshift-kube-controller-manager-operator,2989,105,2,0,1,2,0
openshift-ovn-kubernetes,22366,256,77,0,5,33,0
openshift-authentication,3754,74,1,0,3,6,0
openshift-network-node-identity,439,68,5,0,3,9,0
aaaaaaa,238,25,0,0,1,2,0
openshift-config-operator,2369,58,0,0,1,2,0
openshift-oauth-apiserver,16589,141,5,0,3,6,0
openshift-operator-lifecycle-manager,50806,436,78,0,8,11,0
openshift-machine-api,2962,317,14,0,5,19,0
openshift-kube-storage-version-migrator,56,20,0,0,1,2,0
openshift-cluster-samples-operator,1277,88,10,0,1,4,0
openshift-kube-scheduler,7140,174,2,0,22,18,0
openshift-cluster-version,5112,217,59,0,1,2,0
openshift-console-operator,3058,184,0,0,1,3,0
openshift-kube-apiserver-operator,5270,208,4,0,1,2,0
openshift-cloud-credential-operator,863,93,2,0,3,7,0
openshift-kube-storage-version-migrator-operator,753,44,0,0,1,2,0
openshift-console,1723,1011,3454,0,4,8,0
default,232,28,0,0,1,2,0
openshift-cluster-csi-drivers,4378,327,0,0,6,38,0
```
