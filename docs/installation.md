# Installation

You can install `sxcollector` as an executable or container on your desktop, 
or as a pod running into a kubernetes cluster.

## Local install

### Requirements

- Having a RedHat like (Fedora, CentOS, RHEL, ...) operating system
- Having curl installed `sudo yum install curl`
- Install kubectl with `sudo yum install kubectl`

### Get a copy of sxcollector

```bash
sudo su - && \
curl https://gitlab.com/startx1/k8s/sxcollector/-/raw/stable/bin/sxcollector -o sxcollector && \
cp sxcollector /usr/local/bin/ && \
chmod ugo+x /usr/local/bin/sxcollector && \
exit
```

### Check sxcollector

```bash
sxcollector --help
```

## Container install

### Requirements

- Having a RedHat like (Fedora, CentOS, RHEL, ...) operating system
- Having podman installed `sudo yum install podman`
- Install kubectl with `sudo yum install kubectl`

### Get a copy of sxcollector

```bash
podman pull docker.io/startx/sxcollector:latest
```

### Check sxcollector

```bash
podman run docker.io/startx/sxcollector:latest --help
```

## Kubernetes install

### Requirements

- Having a `kubectl` command line installed
- Having a kubernetes or Openshift cluster installed

#### Connect to a kubernetes cluster

```bash
kubectl \
   config set-credentials \
   mycluster/myuser \
   --username=kubeuser \
   --password=kubepassword
kubectl \
   config set-cluster \
   mycluster \
   --insecure-skip-tls-verify=true \
   --server=https://mycluster.kubernetes.com
kubectl \
   config set-context \
   mycluster/myuser \
   --user=mycluster/myuser \
   --namespace=default \
   --cluster=mycluster
kubectl \
   config use-context \
   mycluster/myuser
```

#### Connect to a Openshift cluster

```bash
oc login  \
   --username=kubeuser \
   --password=kubepassword \
   https://mycluster.openshift.com
```

#### Default kubernetes config

You can change the default kubeconfig using environment variable 

```bash
export KUBECONFIG=/home/user/mykubeconfig
```

By default the /home/user/.kube/config file will be used. ensure that the default context is
connected and can interact with a kubernetes cluster.

### Full default

Deploy all the stack in the **sxcollector** namespace.

```bash
git clone https://gitlab.com/startx1/k8s/sxcollector.git
cd sxcollector/examples/kustomize
kubectl apply -k .
```
