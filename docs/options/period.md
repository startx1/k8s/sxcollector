# Period options

`sxcollector` use prometheus for several pluging (conso, state) and a period for the collecteddata could be set.

## Options flags

| **Flag**                | **Description**                                                                                     |
| ----------------------- | --------------------------------------------------------------------------------------------------- |
| `--from DATE`           | Define the starting date. ex: 2024-01-01 00:00:00 (default is minus 1h)                             |
| `--to DATE`             | Define the ending date. ex: 2024-01-03. Have precedence over duration if both are set ( no default) |
| `--duration DURATION`   | Define the period duration. ex: 1y. Could be overwrite by --to (default is 1h)                      |
| `--precision PRECISION` | A factor that divide the duration. Used for slicing prometheus data (default is 1 = all)            |



## Environment variables

| **Environement variable** | **Description**                                                            |
| ------------------------- | -------------------------------------------------------------------------- |
| SXCOLLECTOR_DURATION      | Define the prometheus duration in replacement of the --duration argument   |
| SXCOLLECTOR_PRECISION     | Define the prometheus precision in replacement of the --precision argument |

## examples commands

## Default output (tabulated)

```bash
sxcollector ns-state
```

Default output type is using the last hour form the current date.

## Output last week

```bash
sxcollector ns-state --duration -7d
```

## Output last first quarter of the year

```bash
sxcollector ns-state --duration 90d --from "2024-01-01 00:00:00"
```

## Output a specific period

```bash
sxcollector ns-state --from "2024-01-01 00:00:00" --to "2024-01-02 00:00:00"
```

## Output a specific period

```bash
sxcollector ns-state --from "2024-01-01 00:00:00" --to "2024-01-02 00:00:00"
```

## Use environment variable for duration

```bash
export SXCOLLECTOR_DURATION=-2d 
sxcollector ns-state 
```
