# Sending mail options

`sxcollector` can send the result of the report as an attachment of an e-mail. 
SMTP credentials must be set using either params or environments variables.

## Options flags

| **Flag**                   | **Description**                                                                                                                  |
| -------------------------- | -------------------------------------------------------------------------------------------------------------------------------- |
| `--mail-to EMAIL`          | Define the destination mail. If set, output will be of type mail. SMTP params must be set                                        |
| `--mail-from EMAIL`        | Define the source mail. If set, output will be of type mail. If not use MAIL_FROM env                                            |
| `--mail-subject SUBJECT`   | Customize the mail subject. Default is "Report SXCollector %s at %s" where first arg is name of the plugin and second the date of execution |
| `--smtp-host HOST`         | Define the SMTP host. If output type is mail and port is not set, use the SMTP_PORT env                                          |
| `--smtp-port PORT`         | Define the SMTP port. If output type is mail and port is not set, use the SMTP_PORT env var                                      |
| `--smtp-username USERNAME` | Define the SMTP username. If output type is mail and username is not set, use the SMTP_USERNAME env var                          |
| `--smtp-password PASSWORD` | Define the SMTP password. If output type is mail and password is not set, use the SMTP_PASSWORD env var                          |
| `--smtp-insecure`          | Define if SMTP connection is insecure. If output type is mail and --smtp-insecure is not set, use the SMTP_INSECURE=true env var |


## Environment variables

| **Environement variable** | **Description**                                                                       |
| ------------------------- | ------------------------------------------------------------------------------------- |
| SMTP_HOST                 | Define the SMTP host in replacement of the --smtp-host argument                       |
| SMTP_PORT                 | Define the SMTP port in replacement of the --smtp-port argument                       |
| SMTP_USERNAME             | Define the SMTP username in replacement of the --smtp-username argument               |
| SMTP_PASSWORD             | Define the SMTP password in replacement of the --smtp-password argument               |
| SMTP_INSECURE=true        | Define if SMTP must be insecure (password is not encrypted). --smtp-insecure argument |
| MAIL_FROM                 | Define the source mail in replacement of the --mail-from argument                     |
| MAIL_SUBJECT              | Define the mail subject in replacement of the --mail-subject argument                 |
| SXCOLLECTOR_TO            | Define the destination mail in replacement of the --mail-to argument                  |


## examples commands

## enabling mail output type

```bash
sxcollector ns-state --mail-to dev@example.com
```

If you set the `--mail-to` or `--mail-from` flag, you will enable the mail output type and all smtp configuration (via params 
or environments variables), should be set properly (see next examples).
If none of `--mail-to` and `--mail-from` params are set, no mail will be send, even if you set the MAIL_TO and MAIL_FROM environment variable.

## Sending mail with full params

### Authenticated and secure SMTP relay

```bash
sxcollector ns-state \
--mail-from dev@example.com \
--mail-to dev@example.com \
--smtp-host smtp.gmail.com \
--smtp-port "587" \
--smtp-username dev@example.com \
--smtp-password "mypassword" 
```
### Unauthenticated and insecure SMTP relay

Not sure you will find this kind of extremly bad configured server, but 
it's just for examples purposes.

```bash
sxcollector ns-state \
--mail-from dev@example.com \
--mail-to dev@example.com \
--smtp-host smtp.gmail.com \
--smtp-port "25" \
--smtp-username "" \
--smtp-password "" \
--smtp-insecure
```

In this example, all the configuration is set using params. No env variables are involved.

## Sending mail with full environements variables

```bash
export \
SMTP_HOST=smtp.gmail.com \
SMTP_PORT=587 \
SMTP_USERNAME=example@gmail.com \
SMTP_PASSWORD=mypassword \
MAIL_FROM=example@gmail.com \
SXCOLLECTOR_TO=example@gmail.com
# execute the sxcollector subcommand 
sxcollector ns-state --mail-to dev@example.com
```

In this example, all the configuration is set using environement variables except the 
insecure option that doesn't have it evironment variable counterpart. 