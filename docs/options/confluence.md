# Managing confluence page options

`sxcollector` can send the result of the report as confluence page. 
Confluence credentials must be set using either params or environments variables.

## Options flags

| **Flag**                         | **Description**                                                                                                                                                      |
| -------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `--confluence-url URL`           | The confluence base URL ex: https://example.atlassian.net/wiki. If --confluence-url is not set, use the CONFLUENCE_BASE_URL env var                                  |
| `--confluence-username USERNAME` | The confluence username ex: user@example.com. If --confluence-username is not set, use the CONFLUENCE_USERNAME env var                                               |
| `--confluence-apitoken APITOKEN` | The confluence API Token, see https://id.atlassian.com/manage-profile/security/api-tokens. If --confluence-apitoken is not set, use the CONFLUENCE_API_TOKEN env var |
| `--confluence-spacekey SPACEKEY` | The confluence Space key. If --confluence-spacekey is not set, use the CONFLUENCE_SPACE_KEY env var                                                                  |
| `--confluence-title TITLE`       | The confluence page title (or section if --confluence-mode=append). If --confluence-title is not set, use the CONFLUENCE_PAGE_TITLE env var                          |
| `--confluence-mode`              | Define the default confluence mode (could be create, update, append). Default is create                                                                              |
| `--confluence-content CONTENT`   | The confluence page content (prepended to the generated content). If --confluence-content is not set, use the CONFLUENCE_PAGE_HTML_CONTENT env var                   |
| `--confluence-pageid PAGEID`     | The confluence pageID (required except for --confluence-mode=create). If --confluence-pageid is not set, use the CONFLUENCE_PAGE_ID env var                          |
| `--confluence-parentid PARENTID` | The confluence parentID (useful with --confluence-mode=create). If --confluence-parentid is not set, use the CONFLUENCE_PARENT_ID env var                            |

## Environment variables

| **Environement variable**    | **Description**                                                                      |
| ---------------------------- | ------------------------------------------------------------------------------------ |
| CONFLUENCE_BASE_URL          | Define the confluence base URL in replacement of the --confluence-url argument       |
| CONFLUENCE_USERNAME          | Define the confluence username in replacement of the --confluence-username argument  |
| CONFLUENCE_API_TOKEN         | Define the confluence API Token in replacement of the --confluence-apitoken argument |
| CONFLUENCE_SPACE_KEY         | Define the confluence Space key in replacement of the --confluence-spacekey argument |
| CONFLUENCE_PAGE_TITLE        | Define the page title in replacement of the --confluence-title argument              |
| CONFLUENCE_PAGE_HTML_CONTENT | Define the page content in replacement of the --confluence-content argument          |
| CONFLUENCE_PAGE_ID           | Define the confluence pageID in replacement of the --confluence-pageid argument      |
| CONFLUENCE_PARENT_ID         | Define the confluence parentID in replacement of the --confluence-parentid argument  |


## examples commands

## enabling confluence output type

```bash
sxcollector ns-state --confluence-mode create
```

If you set the `--confluence-mode` flag, you will enable the confluence output type 
and all confluence configuration (via params or environments variables), 
should be set properly (see next examples).

## Creating confluence with full params

```bash
sxcollector ns-state \
--confluence-mode create \
--confluence-url https://example.atlassian.net/wiki \
--confluence-username user@example.com \
--confluence-apitoken mytoken \
--confluence-spacekey MYSPACE \
--confluence-title "My new page" \
--confluence-content "<h1>this is a page<h1/>" \
--confluence-parentid "123456" \
--confluence-pageid "12345678"
```

In this example, all the configuration is set using params. No env variables are involved.

## Sending confluence with full environements variables

```bash
export CONFLUENCE_BASE_URL="https://example.atlassian.net/wiki"
export CONFLUENCE_USERNAME="user@example.com"
export CONFLUENCE_API_TOKEN=mytoken
export CONFLUENCE_SPACE_KEY=MYSPACE
export CONFLUENCE_PAGE_TITLE="My new page"
export CONFLUENCE_PAGE_HTML_CONTENT="<h1>this is a page<h1/>"
export CONFLUENCE_PAGE_ID="12345678"
export CONFLUENCE_PARENT_ID="123456"
sxcollector ns-state --confluence-mode create
```

In this example, all the configuration is set using environement variables except the 
insecure option that doesn't have a nevironment counterpart. 