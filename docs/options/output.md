# Formating options

`sxcollector` can generate report in multiple format (tab, csv or json). Default
output format is tabulated to suit a bash rendering, but csv or json output could be 
produced.
User can also control the headers generation, and store the produced content into a file.

## Options flags

| **Flag**                      | **Description**                                                                        |
| ----------------------------- | -------------------------------------------------------------------------------------- |
| `--output-format FORMAT`      | Set the formating of the generated content. Could be tab, json or csv (default is tab) |
| `--output-csv-separator SEP ` | Define the separator sign to use for the csv format (default is ,)                     |
| `--output-noheaders`          | Disable headers in the result (default is false)                                       |
| `--output-nounits`            | Disable unit header in the result (default is false)                                   |
| `--output-file FILENAME`      | Define the output file. If not set, stdout will be used (default is stdout)            |

## Environment variables

| **Environement variable** | **Description**                                                                      |
| ------------------------- | ------------------------------------------------------------------------------------ |
| SXCOLLECTOR_NOHEADERS     | Disable the headers in replacement of the --output-noheaders argument                |
| SXCOLLECTOR_NOUNITS       | Disable the units in replacement of the --output-nounits argument                    |
| SXCOLLECTOR_SEP           | Define the separator character in replacement of the --output-csv-separator argument |

## examples commands

## Default output (tabulated)

```bash
sxcollector ns-state
```

Default output type is using the standard output with a tabulated format.

## CSV output

```bash
sxcollector ns-state --output-format csv --output-csv-separator ";"
```

## CSV output without headers

```bash
sxcollector ns-state --output-format csv --output-noheaders --output-nounits
```

## CSV output recorded as file

```bash
sxcollector ns-state --output-format csv --output-file ns-state.csv
```
