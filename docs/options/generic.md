# Sending mail options

`sxcollector` have several parameter that could be used for every command.

## Options flags

| **Flag**                | **Description**                                                                                                                               |
| ----------------------- | --------------------------------------------------------------------------------------------------------------------------------------------- |
| `--insecure`            | Skip TLS verification for prometheus communication (default false)                                                                            |
| `--debug`               | Activates debug mode for detailed troubleshooting information.                                                                                |
| `--help`                | Displays this help message and exits.                                                                                                         |
| `--kubeconfig FILEPATH` | Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided) |

The `--debug` and `--help` options are applicable to all commands for enhanced 
functionality or information.

## examples commands

## Get help for a sub-command

```bash
sxcollector ns-state --help
```

## Display all the debug detail

```bash
sxcollector ns-state --debug
```

## Use insecure connection

```bash
sxcollector ns-state --insecure
```

## Use a specific kubeconfig

```bash
sxcollector --kubeconfig /my/kubeconfig
```
