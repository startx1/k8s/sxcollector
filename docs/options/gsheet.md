# Managing google spreadsheet options

`sxcollector` can send the result of the report as google spreadsheet. 
GoogleAPI credentials must be set using either params or environments variables.

## Options flags

| **Flag**                                       | **Description**                                                                                                                                                                                                   |
| ---------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `--gsheet-spreadsheet-id SPREADSHEET_ID`       | The spreadsheetID to use. If set pass to an update mode. If --gsheet-spreadsheet-id is not set, use the GSPREADSHEET_ID env var.                                                                                  |
| `--gsheet-spreadsheet-title SPREADSHEET_TITLE` | The spreadsheet name to use. If --gsheet-spreadsheet-title is not set, use the GSPREADSHEET_TITLE env var. If both unset, use default name: _Report SXCollector_.                                                 |
| `--gsheet-sheet-title SHEET_TITLE`             | The sheet name to use within the spreadsheet. If --gsheet-sheet-title is not set, use the GSPREADSHEET_SHEET_TITLE env var. If both unset, use default name: _Subcommand-Date_.                                   |
| `--gdrive-creds-filename FILENAME`             | The name of the google credentials file. If --gdrive-creds-filename is not set, use the GDRIVE_CREDENTIAL_FILENAME env var. If both unset, use default name: `credentials.json`.                                  |
| `--gdrive-creds-path PATH`                     | The name of the path to google credentials. If --gdrive-creds-path is not set, use the GDRIVE_CREDENTIAL_PATH env var. If both unset, use default path: `/tmp`                                                    |
| `--gdrive-creds-domain DOMAIN`                 | The company domain to use. If --gdrive-creds-domain is not set, use the GDRIVE_CREDENTIAL_DOMAIN env var. If both unset, use default path: `google.com`. Only used if permision are required at the domain level |
| `--gsheet-mode`                                | Define the default gsheet mode (could be create, update, append). Default is create                                                                                                                               |

## Environment variables

| **Environement variable**  | **Description**                                                                              |
| -------------------------- | -------------------------------------------------------------------------------------------- |
| GSPREADSHEET_ID            | Define the ID of the spreadsheet in replacement of the --gsheet-spreadsheet-id argument      |
| GSPREADSHEET_TITLE         | Define the name of the spreadsheet in replacement of the --gsheet-spreadsheet-title argument |
| GSPREADSHEET_SHEET_TITLE   | Define the name of the sheet in replacement of the --gsheet-sheet-title argument             |
| GDRIVE_CREDENTIAL_FILENAME | Define the name of the credential file in replacement of the gdrive-creds-filename argument  |
| GDRIVE_CREDENTIAL_PATH     | Define the path to the credential file in replacement of the --gdrive-creds-path argument    |
| GDRIVE_CREDENTIAL_DOMAIN   | Define the company domain in replacement of the --gdrive-creds-domain argument               |


## examples commands

### enabling gsheet output type

```bash
sxcollector ns-state --gsheet-mode create
```

If you set the `--gsheet-mode` flag, you will enable the gsheet output type 
and all gsheet configuration (via params or environments variables), 
should be set properly (see next examples).

### Creating google spreadsheet with full params

```bash
sxcollector ns-state \
--gsheet-mode create \
--gdrive-creds-domain "example.com" \
--gdrive-creds-filename credentials.json \
--gdrive-creds-path /tmp \
--gsheet-spreadsheet-title "MySXCollectorSpreadsheet" \
--gsheet-sheet-title "MySheet"
```

In this example, all the configuration is set using params. No env variables are involved.

### Creating google spreadsheet with full environements variables

```bash
export GSPREADSHEET_TITLE="MySXCollectorSpreadsheet"
export GSPREADSHEET_SHEET_TITLE="MySheet"
export GDRIVE_CREDENTIAL_FILENAME="credentials.json"
export GDRIVE_CREDENTIAL_PATH="/tmp"
export GDRIVE_CREDENTIAL_DOMAIN="example.com"
sxcollector ns-state --gsheet-mode create
```

In this example, all the configuration is set using environement variables. 

### Updating google spreadsheet with full params

```bash
sxcollector ns-state \
--gsheet-mode update \
--gdrive-creds-domain "example.com" \
--gdrive-creds-filename credentials.json \
--gdrive-creds-path /tmp \
--gsheet-spreadsheet-id "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" \
--gsheet-sheet-title "MySheet"
```